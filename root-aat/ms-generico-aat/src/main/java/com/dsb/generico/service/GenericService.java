package com.dsb.generico.service;

import javax.validation.Valid;

import com.dsb.core.common.http.Response;
import com.dsb.core.domain.wrapper.GenericoWrapper;

import reactor.core.publisher.Mono;

public interface GenericService {

	Mono<Response<GenericoWrapper>> obtenerDatosCliente(@Valid Long idCliente);
}
