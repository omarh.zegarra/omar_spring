package com.dsb.generico.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dsb.core.common.http.Response;
import com.dsb.core.domain.wrapper.GenericoWrapper;
import com.dsb.generico.service.GenericService;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/web/generico")
public class GenericController {
	
	@Autowired
	GenericService genericService;
	
	@GetMapping(value = "/")
	public String ping(){
		return "mantenimiento web is running..";
	}
	
	@PostMapping(value = "/obtener/datos/cliente/{idCliente}",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Mono<Response<GenericoWrapper>> crearProducto(@Valid @PathVariable Long idCliente){
		return genericService.obtenerDatosCliente(idCliente);
	}
}
