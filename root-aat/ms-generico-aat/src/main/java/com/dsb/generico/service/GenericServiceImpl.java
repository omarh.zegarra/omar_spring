package com.dsb.generico.service;

import java.util.HashSet;
import java.util.Set;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.dsb.core.common.base.BaseService;
import com.dsb.core.common.http.Response;
import com.dsb.core.domain.session.SessionCliente;
import com.dsb.core.domain.session.SessionClienteFinal;
import com.dsb.core.domain.session.SessionCoordinador;
import com.dsb.core.domain.session.SessionLote;
import com.dsb.core.domain.session.SessionProducto;
import com.dsb.core.domain.session.SessionRotulo;
import com.dsb.core.domain.wrapper.GenericoWrapper;
import com.dsb.core.util.Util;
import com.dsb.persistence.model.session.SessionClienteFinalModel;
import com.dsb.persistence.model.session.SessionClienteModel;
import com.dsb.persistence.model.session.SessionCoordinadorModel;
import com.dsb.persistence.model.session.SessionProductoModel;
import com.dsb.persistence.model.session.SessionRotuloModel;
import com.dsb.persistence.repository.session.SessionClienteRepository;

import reactor.core.publisher.Mono;

@Service
public class GenericServiceImpl extends BaseService implements GenericService{
	private static final Logger logger = LogManager.getLogger(GenericServiceImpl.class);

	@Autowired
	SessionClienteRepository clienteRepository;
	
	@Override
	public Mono<Response<GenericoWrapper>> obtenerDatosCliente(@Valid Long idCliente){
		logger.info("init get data client session...");
		Response<GenericoWrapper> response = new Response<>();
		GenericoWrapper gw=new GenericoWrapper();
		
		return Mono.just(clienteRepository.findById(idCliente))
			.flatMap(x -> {
				if(x.isPresent()) {
					SessionClienteModel sessionClienteModel=x.get();					
					SessionCliente sessionCliente=new SessionCliente();
					Set<SessionClienteFinal> sessionClientesFinales= new HashSet<>();
					Set<SessionRotulo> sessionRotulos= new HashSet<>();
					Set<SessionProducto> sessionProductos= new HashSet<>();
					Set<SessionLote> sessionLotes= new HashSet<>();
					
					Set<SessionClienteFinalModel> sessionClientesFinalesModel=sessionClienteModel.getSessionClientesFinales();
					Set<SessionRotuloModel> sessionRotuloModel=sessionClienteModel.getSessionRotulos();
					Set<SessionProductoModel> sessionProductoModel=sessionClienteModel.getSessionProductos();					
					SessionCoordinadorModel sessionCoordinadorModel=sessionClienteModel.getSessionCoordinador();
								
					if(!sessionClientesFinalesModel.isEmpty()) {
						sessionClientesFinalesModel
						.forEach(
							y ->{
								y.setSessionCliente(null);
								SessionClienteFinal sessionClienteFinal=Util.objectToObject(SessionClienteFinal.class,y);
								sessionClientesFinales.add(sessionClienteFinal);
							}
						);
						sessionCliente.setSessionClientesFinales(sessionClientesFinales);
					}
					
					if(!sessionRotuloModel.isEmpty()) {
						sessionRotuloModel
						.forEach(
							y ->{
								y.setSessionCliente(null);
								SessionRotulo sessionRotulo=Util.objectToObject(SessionRotulo.class,y);
								sessionRotulos.add(sessionRotulo);
							}
						);
						sessionCliente.setSessionRotulos(sessionRotulos);
					}					
					if(!sessionProductoModel.isEmpty()) {
						sessionProductoModel
						.forEach(
							y ->{			
								if(!y.getSessionLote().isEmpty()) {
									y.getSessionLote()
									.forEach(
										z ->{
											z.setSessionProducto(null);
											SessionLote sessionLote=Util.objectToObject(SessionLote.class,z);
											sessionLotes.add(sessionLote);						
										}
									);								
								}							
								y.setSessionLote(null);
								y.setSessionCliente(null);
								SessionProducto sessionProducto=Util.objectToObject(SessionProducto.class,y);						
								sessionProducto.setSessionLotes(sessionLotes);
								sessionProductos.add(sessionProducto);
							}
						);
						sessionCliente.setSessionProductos(sessionProductos);	
					}					
					if(!StringUtils.isEmpty(sessionCoordinadorModel) && !StringUtils.isEmpty(sessionCoordinadorModel.getSessionCliente())) {
						sessionCoordinadorModel.setSessionCliente(null);
						sessionCliente.setSessionCoordinador(Util.objectToObject(SessionCoordinador.class,sessionCoordinadorModel));	
					}														
					sessionClienteModel.setSessionClientesFinales(null);
					sessionClienteModel.setSessionCoordinador(null);
					sessionClienteModel.setSessionProductos(null);
					sessionClienteModel.setSessionRotulos(null);
					
					sessionCliente.setIdCliente(sessionClienteModel.getIdCliente());
					sessionCliente.setIdArchivoBpm(sessionClienteModel.getIdArchivoBpm());
					sessionCliente.setRuc(sessionClienteModel.getRuc());
					sessionCliente.setRazonSocial(sessionClienteModel.getRazonSocial());
					sessionCliente.setActivo(sessionClienteModel.getActivo());
					sessionCliente.setAcronimo(sessionClienteModel.getAcronimo());
					sessionCliente.setTelefono(sessionClienteModel.getTelefono());
					sessionCliente.setDistrito(sessionClienteModel.getDistrito());
					sessionCliente.setDepartamento(sessionClienteModel.getDepartamento());
					sessionCliente.setProvincia(sessionClienteModel.getProvincia());
					sessionCliente.setDireccion(sessionClienteModel.getDireccion());
					sessionCliente.setCorreo(sessionClienteModel.getCorreo());
					gw.setSessionCliente(sessionCliente);
					response.setObjetoRespuesta(gw);					
				}			
				return Mono.just(response);
			}).onErrorResume(e -> {
				logger.info(e);
				throw new RuntimeException("error on get client session..");
			});
	}
}
