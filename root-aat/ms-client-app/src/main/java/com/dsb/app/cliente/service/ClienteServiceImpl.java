package com.dsb.app.cliente.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.dsb.core.common.http.Response;
import com.dsb.core.domain.BusquedaProducto;
import com.dsb.core.domain.ClienteFinal;
import com.dsb.core.domain.Coordinador;
import com.dsb.core.domain.DetallePedido;
import com.dsb.core.domain.Lote;
import com.dsb.core.domain.Posicion;
import com.dsb.core.domain.Requerimiento;
import com.dsb.core.domain.SeguimPedido;
import com.dsb.core.domain.empresa.Empresa;
import com.dsb.core.domain.empresa.Oficina;
import com.dsb.core.domain.empresa.Sucursal;
import com.dsb.core.domain.persona.Direccion;
import com.dsb.core.domain.producto.Producto;
import com.dsb.core.domain.seguridad.Usuario;
import com.dsb.core.util.Constantes;
import com.dsb.core.util.Enumeradores;
import com.dsb.core.util.Util;
import com.dsb.persistence.model.empresa.CertificadoModel;
import com.dsb.persistence.model.empresa.ConfiguracionModel;
import com.dsb.persistence.model.empresa.DireccionModel;
import com.dsb.persistence.model.empresa.EmpresaModel;
import com.dsb.persistence.model.empresa.OficinaModel;
import com.dsb.persistence.model.empresa.SucursalModel;
import com.dsb.persistence.model.requerimiento.RequerimientoModel;
import com.dsb.persistence.repository.EmpresaRepository;
import com.dsb.persistence.repository.RequerimientoRepository;

import reactor.core.publisher.Mono;

@Service
public class ClienteServiceImpl extends com.dsb.core.common.base.BaseService implements ClienteService{
	private static final Logger logger = LogManager.getLogger(ClienteServiceImpl.class);
	
	@Autowired
	EmpresaRepository empresaRepository;
	@Autowired
	RequerimientoRepository requerimientoRepository;
	
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	
	@Value("${spring.mail.username}")
    private String username;
	
	@Override
	public Mono<Response<Void>> register(final Empresa empresa) {
		Response<Void> response = new Response<>();
		response.setPath(getPathApi());

		return Mono.<Response<Void>> defer(() -> transactionTemplate.execute(status -> {
			logger.info("init insert empresa..");	
			EmpresaModel empresaModel=new EmpresaModel(); 
			Empresa emp=Optional.of(empresa).orElseThrow(IllegalArgumentException::new);
			logger.info("empresa..."+emp);
			empresaModel.setRazonSocial(empresa.getRazonSocial());
			empresaModel.setRuc(empresa.getRuc());
			empresaModel.setUsuarioSunat(empresa.getUsuarioSunat());
			empresaModel.setIdEstado(empresa.getIdEstado());
			
			ConfiguracionModel configEmpresaModel=Util.objectToObject(ConfiguracionModel.class,empresa.getConfiguracionEmpresa());
			Set<ConfiguracionModel> setConfigEmpresa = new HashSet<>();
			setConfigEmpresa.add(configEmpresaModel);			
			empresaModel.setConfiguracionEmpresa(setConfigEmpresa);
			configEmpresaModel.setEmpresa(empresaModel);	
			
			CertificadoModel certificadoModel=Util.objectToObject(CertificadoModel.class,empresa.getCertificado());
			Set<CertificadoModel> setCertificadoModel = new HashSet<>();
			setCertificadoModel.add(certificadoModel);
			empresaModel.setCertificados(setCertificadoModel);
			certificadoModel.setEmpresa(empresaModel);
			
			getSucursalChilds(empresaModel,empresa.getSucursal());
			
			empresaModel = empresaRepository.saveAndFlush(empresaModel);
			response.setStatusText(Enumeradores.StatusText.OK.name());		
			response.setMessage("se creo la empresa correctamente...");
			return Mono.just(response)
				.switchIfEmpty(Mono.error(new RuntimeException("error on register company 1")));
		})).subscribeOn(jdbcScheduler);
		
	}
	
	private void getSucursalChilds(EmpresaModel empresaModel,Sucursal sucursal) {
		Optional<Sucursal> sucursalOptional = Optional.ofNullable(sucursal);
		if(sucursalOptional.isPresent()) {
			SucursalModel sucursalModel=Util.objectToObject(SucursalModel.class,sucursalOptional.get());
			Set<SucursalModel> setSucursals = new HashSet<>();
			setSucursals.add(sucursalModel);	
			sucursalModel.setEmpresa(empresaModel);
			empresaModel.setSucursals(setSucursals);
			
			Optional<Oficina> oficinaOptional = Optional.ofNullable(sucursal.getOficina());
			if(oficinaOptional.isPresent()) {
				OficinaModel oficinaModel=Util.objectToObject(OficinaModel.class,oficinaOptional.get());
				Set<OficinaModel> setOficinas = new HashSet<>();
				setOficinas.add(oficinaModel);	
				sucursalModel.setOficinas(setOficinas);
				oficinaModel.setSucursal(sucursalModel);	
				
				Optional<Direccion> direccionOptional = Optional.ofNullable(sucursal.getOficina().getDireccion());
				if(direccionOptional.isPresent()) {
					DireccionModel direccionModel=Util.objectToObject(DireccionModel.class,direccionOptional.get());
					Set<DireccionModel> setDirecciones = new HashSet<>();
					setDirecciones.add(direccionModel);	
					oficinaModel.setDirecciones(setDirecciones);
					direccionModel.setOficina(oficinaModel);
				}
			}
		}
	}
	
	@Override
	public Mono<Response<EmpresaModel>> findById(Integer id) {
		Response<EmpresaModel> response = new Response<>();
		response.setPath(getPathApi());
		return Mono.<Response<EmpresaModel>> defer(() -> {
			logger.info("init insert empresa..");
			response.setObjetoRespuesta(empresaRepository.findById(Long.valueOf(id.longValue())).get());
			return Mono.just(response);
		})
		.switchIfEmpty(Mono.error(new RuntimeException("error on register company 2")));
	}	
	
	@Override
	public Mono<Response<List<ClienteFinal>>> listaClientesFinales(final Integer id_cliente){
		Response<List<ClienteFinal>> response = getResponse();
		
		return Mono.<Response<List<ClienteFinal>>> defer(() -> {
			logger.info("init get ListaClientesFinales");
			try {
			List<Map<String, Object>> results = jdbcTemplate.queryForList("select * from cargar_clientes_finales(?)",id_cliente);
			List<ClienteFinal> opciones = results.stream().map(m -> {
				ClienteFinal opcion = new ClienteFinal();
				//opcion.setId_cliente_final(Integer.valueOf(String.valueOf(m.get("id_cli_final")))); 
				//opcion.setNombre_razon_social(String.valueOf(m.get("nombre_cli_final"))); 
				return opcion;
			}).collect(Collectors.toList());
			response.setObjetoRespuesta(opciones);
			
			if (opciones.size()==0) { 
				response.setMessage("NO HAY DATOS!");
				response.setStatusText("OK");
			} else {
				response.setMessage("LISTA USUARIOS FINAL CORRECTO!");
				response.setStatusText("OK");
				}
			response.setStatus(200);		
			return Mono.just(response);
			}
			catch (Exception ex) {
				response.setMessage(ex.getMessage());
				response.setStatus(404);
				response.setStatusText("ERROR");
				return Mono.just(response);
			}
		})
		.switchIfEmpty(Mono.error(new RuntimeException("Error en Servicio --- Listar CLientes Finales---")));
	}

	@Override
	public Mono<Response<List<SeguimPedido>>> listaSeguimientoPedido(Integer id_ord_serv, Integer id_guia_remision,
			Integer id_cliente_final, Integer id_cliente) {
Response<List<SeguimPedido>> response = getResponse();
		
		return Mono.<Response<List<SeguimPedido>>> defer(() -> {
			logger.info("init get Lista Seguimiento Pedido");
			try {
			List<Map<String, Object>> results = jdbcTemplate.queryForList("select * from lista_seguimiento_pedido(?,?,?, ?)",id_ord_serv, id_guia_remision, id_cliente_final, id_cliente );
			List<SeguimPedido> opciones = results.stream().map(m -> {
				SeguimPedido opcion = new SeguimPedido();
				opcion.setCodigo_guia(String.valueOf(m.get("codigo_guia"))); 
				opcion.setCodigo_ord_servicio(String.valueOf(m.get("codigo_ord_servicio"))); 
				opcion.setId_cliente_final(Integer.valueOf(String.valueOf(m.get("id_cliente_final")))); 
				opcion.setId_guia_remisionn(Integer.valueOf(String.valueOf(m.get("id_guia_remisionn")))); 
				opcion.setId_orden_servicio(Integer.valueOf(String.valueOf(m.get("id_orden_servicio")))); 
				opcion.setId_seguimiento(Integer.valueOf(String.valueOf(m.get("id_seguimiento")))); 
				opcion.setId_servicio(Integer.valueOf(String.valueOf(m.get("id_servicio")))); 
				opcion.setRazon_social_cli_final(String.valueOf(m.get("razon_social_cli_final"))); 
				opcion.setId_estado_seguimiento(Integer.valueOf(String.valueOf(m.get("id_estado_seguimiento"))));
				opcion.setNombre_estado_seguimiento(String.valueOf(m.get("nombre_estado_seguimiento")));
				return opcion;
	
			}).collect(Collectors.toList());
			response.setObjetoRespuesta(opciones);
			
			if (opciones.size()==0) { 
				response.setMessage("NO HAY DATOS!");
				response.setStatusText("ERROR");
			} else {
				response.setMessage("LISTA USUARIOS FINAL CORRECTO!");
				response.setStatusText("OK");
				}
			response.setStatus(200);		
			return Mono.just(response);
			}
			catch (Exception ex) {
				response.setMessage(ex.getMessage());
				response.setStatus(404);
				response.setStatusText("ERROR");
				return Mono.just(response);
			}
		})
		.switchIfEmpty(Mono.error(new RuntimeException("Error en Servicio --- Lista Seguimiento Pedido---")));
	}

	@Override
	public Mono<Response<List<Usuario>>> envioCorreoCambioContrasena(String tto) {
		Response<List<Usuario>> response = getResponse();

			List<Map<String, Object>> results = jdbcTemplate.queryForList("select * from validar_correo(?)", tto);
			List<Usuario> opciones =results.stream().map(m -> {
				Usuario opcion = new Usuario();
				opcion.setId(Integer.parseInt(String.valueOf(m.get("id_usuario")))); 
				opcion.setNombres(String.valueOf(m.get("nombre_usuario"))); 
				opcion.setClave(String.valueOf(m.get("clave"))); 
				opcion.setActivo(Boolean.parseBoolean(String.valueOf(m.get("activo"))));
				return opcion;	
			}).collect(Collectors.toList());
	
			response.setObjetoRespuesta(opciones);
			
			if (opciones.size()==0) { 
				response.setMessage("NO HAY DATOS!");
				response.setStatusText("OK");
				response.setStatus(200);		
				return Mono.just(response);
			} else {
					if (opciones.get(0).getActivo()==false) { 
						response.setMessage("USUARIO NO ACTIVO!");
						response.setStatusText("OK");
					} 
					else {
							response.setMessage("USUARIO ENCONTRADO!");
							response.setStatusText("OK");
						//	String username=propertiesUtil.getPropertiesString("spring.mail.username");
							try {
								String texto= "Estimado Sr(a) " + opciones.get(0).getNombres()+", se envia su recordatorio de contraseña. El cual viene a ser -> CLAVE= " +opciones.get(0).getClave();
							//    EnvioCorreo m= new EnvioCorreo(hostt,portt,username,password);
							   // m.sendMail(tto, "RECORDATORIO CONTRASEÑA", texto);
							    envioCorreo(username, tto, "RECORDATORIO CONTRASEÑA", texto);
							  //  logger.info("HOST:  "+ propertiesUtil.getPropertiesString("spring.mail.host"));
							/*
							String hostt=propertiesUtil.getPropertiesString("spring.mail.host");
							String portt=propertiesUtil.getPropertiesString("spring.mail.port");
							String username=propertiesUtil.getPropertiesString("spring.mail.username");
							String password=propertiesUtil.getPropertiesString("spring.mail.password");
							*/
								
									//String texto= "Estimado Sr(a) " + opciones.get(0).getNombres()+", se envia su recordatorio de contraseña. El cual viene a ser -> CLAVE= " +opciones.get(0).getClave();
								    //EnvioCorreo m= new EnvioCorreo(hostt,portt,username,password);
								    //m.sendMail(tto, "RECORDATORIO CONTRASEÑA", texto);						    
								    response.setStatus(200);		
									return Mono.just(response);
							}
							catch (Exception ex){
									response.setMessage(ex.getMessage());
									response.setStatus(404);
									response.setStatusText("ERROR");
									return Mono.just(response);
							}
					    };
						response.setStatus(200);	
						return Mono.just(response);
					}

	}

	@Override
	public Mono<Response<List<DetallePedido>>> obtenerSeguimientoPedidoDetalle(Integer id_clientee, Integer id_guiaa,
			Integer id_ord_servicioo, Integer id_seguimientoo) {
		// TODO Auto-generated method stub
		Response<List<DetallePedido>> response = getResponse();
		
		return Mono.<Response<List<DetallePedido>>> defer(() -> {
			logger.info("init get Lista Seguimiento Pedido Detalle");
			try {
			List<Map<String, Object>> results = jdbcTemplate.queryForList("select * from detalle_seguimiento_pedido(?,?,?, ?)",id_clientee, id_guiaa, id_ord_servicioo, id_seguimientoo );
			List<DetallePedido> opciones = results.stream().map(m -> {
				DetallePedido opcion = new DetallePedido();
				opcion.setCodigo_guia(String.valueOf(m.get("codigo_guia"))); 
				opcion.setCodigo_orden_servicio(String.valueOf(m.get("codigo_orden_servicio")));	
				opcion.setDireccion(String.valueOf(m.get("direccion")));
				opcion.setDirectorio_conformidad(String.valueOf(m.get("directorio_conformidad")));	 
				opcion.setDirectorio_documento(String.valueOf(m.get("directorio_documento")));	 
				opcion.setEstado_seguimiento(String.valueOf(m.get("estado_seguimiento")));	
				opcion.setHora_finalizacion(String.valueOf(m.get("hora_finalizacion")));	
				opcion.setHora_inicio_ruta(String.valueOf(m.get("hora_inicio_ruta")));	
				opcion.setHora_llegada(String.valueOf(m.get("hora_llegada")));	
				opcion.setHora_salida(String.valueOf(m.get("hora_salida")));	
				opcion.setId_archivo_conformidad(Integer.parseInt(String.valueOf(m.get("id_archivo_conformidad"))));
				opcion.setId_archivo_entrega_reco(Integer.parseInt(String.valueOf(m.get("id_archivo_entrega_reco"))));
				opcion.setId_cliente_final(Integer.parseInt(String.valueOf(m.get("id_cliente_final"))));
				opcion.setId_guia(Integer.parseInt(String.valueOf(m.get("id_guia"))));
				opcion.setId_orden_servicio(Integer.parseInt(String.valueOf(m.get("id_orden_servicio"))));
				opcion.setId_seguimiento(Integer.parseInt(String.valueOf(m.get("id_seguimiento"))));
				opcion.setKilometros_recorrido(Integer.parseInt(String.valueOf(m.get("kilometros_recorrido"))));
				opcion.setLat_pu_llegada(String.valueOf(m.get("lat_pu_llegada")));
				opcion.setLat_pu_partida(String.valueOf(m.get("lat_pu_partida")));
				opcion.setLon_pu_llegada(String.valueOf(m.get("lon_pu_llegada")));
				opcion.setLon_pu_partida(String.valueOf(m.get("lon_pu_partida")));
				opcion.setPlaca(String.valueOf(m.get("placa")));
				opcion.setPos_hist_lat(String.valueOf(m.get("pos_hist_lat")));
				opcion.setPos_hist_long(String.valueOf(m.get("pos_hist_long")));
				opcion.setPunto_llegada(Integer.parseInt(String.valueOf(m.get("punto_llegada"))));
				opcion.setPunto_partida(Integer.parseInt(String.valueOf(m.get("punto_partida"))));
				opcion.setRazon_social(String.valueOf(m.get("razon_social")));
				opcion.setSub_tipo(String.valueOf(m.get("sub_tipo")));
				opcion.setTipo(String.valueOf(m.get("tipo")));
				opcion.setId_vehiculo(Integer.parseInt(String.valueOf(m.get("id_vehiculo"))));
				opcion.setTiempo_en_ruta(String.valueOf(m.get("tiempo_en_ruta")));
				opcion.setTipo_rs(String.valueOf(m.get("tipo_rs")));
				opcion.setAct_logistica(String.valueOf(m.get("act_logistica")));
				return opcion;
				  
				
			}).collect(Collectors.toList());
			response.setObjetoRespuesta(opciones);
			
			if (opciones.size()==0) { 
				response.setMessage("NO HAY DATOS!");
				response.setStatusText("ERROR");
			} else {
				response.setMessage("DETALLE SEGUIMIENTO PEDIDO ENCONTRADO!");
				response.setStatusText("OK");
				}
			response.setStatus(200);		
			return Mono.just(response);
			}
			catch (Exception ex) {
				response.setMessage(ex.getMessage());
				response.setStatus(404);
				response.setStatusText("ERROR");
				return Mono.just(response);
			}
		})
		.switchIfEmpty(Mono.error(new RuntimeException("Error en Servicio --- Lista Seguimiento Pedido Detalle---")));
	}

	@Override
	public Mono<Response<BusquedaProducto>> obtenerBusquedaStockProducto(Integer id_clientee, String sku_nombre_producto) {
		Response<BusquedaProducto> response = getResponse();
		BusquedaProducto objwrapper= new BusquedaProducto();
		//Producto objProducto= new Producto(0);
		Producto objProducto= new Producto();
		return Mono.<Response<BusquedaProducto>> defer(() -> {
			logger.info("init get menu..");
			try {
			List<Map<String, Object>> results = jdbcTemplate.queryForList("select * from busqueda_stock_producto(?,?)",id_clientee,sku_nombre_producto);
			List<Lote> opciones = results.stream().map(m -> {
				Lote opcion = new Lote();
				opcion.setIdLote(Long.valueOf(String.valueOf(m.get("id_lote"))));	
				/*
				opcion.setEnvoltura_lote(String.valueOf(m.get("envoltura_lote")));	
				opcion.setFecha_vencimiento_lote(String.valueOf(m.get("fecha_vencimiento_lote")));	
				opcion.setId_rotulo_lote(Integer.valueOf(String.valueOf(m.get("id_rotulo_lote"))));
				opcion.setCant_total_lote(Integer.valueOf(String.valueOf(m.get("cant_total_lote"))));
				opcion.setCant_disponible_lote(Integer.valueOf(String.valueOf(m.get("cant_disponible_lote"))));
				opcion.setCant_consumida_lote(Integer.valueOf(String.valueOf(m.get("cant_consumida_lote"))));
				*/

				
				objProducto.setIdCliente(Integer.valueOf(String.valueOf(m.get("id_cliente"))));   
				objProducto.setIdProducto(Integer.valueOf(String.valueOf(m.get("id_producto"))));
				objProducto.setNombre(String.valueOf(m.get("nombre")));	
				objProducto.setNombreIngles(String.valueOf(m.get("nombre_ingles")));	
				objProducto.setPrecioUnitario(Float.parseFloat(String.valueOf(m.get("precio_unitario"))));
				objProducto.setPresentacion(String.valueOf(m.get("presentacion")));
				objProducto.setSku(String.valueOf(m.get("sku")));
				objProducto.setTemperatura(String.valueOf(m.get("temperatura")));	
				objProducto.setAplicaFechaVencimiento(Boolean.parseBoolean(String.valueOf(m.get("aplica_fecha_vencimiento"))));
				//Integer cant=objProducto.getStockDisponible()+opcion.getCantidadDisponible_lote();
				//objProducto.setStockDisponible(cant);
				return opcion;
			}).collect(Collectors.toList());
			
			
			
			logger.info("result.."+opciones);
			
			objwrapper.setListaLotes(opciones);			
			objwrapper.setObjProducto(objProducto);
			
			if (opciones.size()==0) { 
				response.setMessage("No Existe Stock del Producto!");
				response.setStatusText("ERROR");
			} else {
				response.setMessage("El Producto cuenta con Stock en Lotes!");
				response.setStatusText("OK");}
		
			response.setStatus(200);	
			response.setObjetoRespuesta(objwrapper);
			return Mono.just(response);
			}
			catch (Exception ex) {
				response.setMessage(ex.getMessage());
				response.setStatus(404);
				response.setStatusText("ERROR");
				return Mono.just(response);
			}
			
			
		})
		.switchIfEmpty(Mono.error(new RuntimeException("Error en Servicio Busqueda de Producto.")));
	}

	@Override
	public Mono<Response<List<Producto>>> obtenerListaProductos(Integer id_clientee, String sku_nombre_producto) {
		Response<List<Producto>> response = getResponse();
		
		return Mono.<Response<List<Producto>>> defer(() -> {
			logger.info("init get ListaProductos");
			try {
			List<Map<String, Object>> results = jdbcTemplate.queryForList("select * from listar_productos(?,?)",id_clientee,sku_nombre_producto);
			List<Producto> opciones = results.stream().map(m -> {
				//Producto opcion = new Producto(0);
				Producto opcion= new Producto();
				opcion.setAplicaFechaVencimiento(Boolean.parseBoolean(String.valueOf(m.get("aplica_fecha_vencimiento"))));
				opcion.setIdCliente(Integer.valueOf(String.valueOf(m.get("id_cliente")))); 
				opcion.setIdProducto(Integer.valueOf(String.valueOf(m.get("id_producto")))); 
				opcion.setNombre(String.valueOf(m.get("nombre")));
				opcion.setNombreIngles(String.valueOf(m.get("nombre_ingles")));
				opcion.setPrecioUnitario(Float.parseFloat(String.valueOf(m.get("precio_unitario"))));
				opcion.setPresentacion(String.valueOf(m.get("presentacion")));
				opcion.setSku(String.valueOf(m.get("sku")));
				opcion.setTemperatura(String.valueOf(m.get("temperatura")));
				return opcion;
			}).collect(Collectors.toList());
			response.setObjetoRespuesta(opciones);
			
			if (opciones.size()==0) { 
				response.setMessage("NO HAY PRODUCTOS!");
				response.setStatusText("ERROR");
			} else {
				response.setMessage("LISTA DE PRODUCTOS CORRECTA!");
				response.setStatusText("OK");
				}
			response.setStatus(200);		
			return Mono.just(response);
			}
			catch (Exception ex) {
				response.setMessage(ex.getMessage());
				response.setStatus(404);
				response.setStatusText("ERROR");
				return Mono.just(response);
			}
		})
		.switchIfEmpty(Mono.error(new RuntimeException("Error en Servicio --- Listar Productos---")));
	}

	@Override
	public Mono<Response<Posicion>> posicionTiempoReal(Integer in_id_seguimiento, Integer in_id_vehiculo) {
Response<Posicion> response = getResponse();
		
		return Mono.<Response<Posicion>> defer(() -> {
			logger.info("init get ListaClientesFinales");
			try {
			List<Map<String, Object>> results = jdbcTemplate.queryForList("select * from obtener_posicion_tiempo_real(?,?)",in_id_seguimiento,in_id_vehiculo);
			List<Posicion> opciones = results.stream().map(m -> {
				Posicion opcion = new Posicion();
				opcion.setId_seguimiento(Integer.valueOf(String.valueOf(m.get("id_seguimiento")))); 
				opcion.setId_vehiculo(Integer.valueOf(String.valueOf(m.get("id_vehiculo"))));	
				opcion.setPos_hist_lat(String.valueOf(m.get("pos_hist_lat")));
				opcion.setPos_hist_long(String.valueOf(m.get("pos_hist_long")));
				return opcion;
			}).collect(Collectors.toList());
			response.setObjetoRespuesta(opciones.get(0));
			
			if (opciones.size()==0) { 
				response.setMessage("NO HAY DATOS!");
				response.setStatusText("ERROR");
			} else {
				response.setMessage("SE ENCONTRO NUEVA POSICION ACTUAL!");
				response.setStatusText("OK");
				}
			response.setStatus(200);		
			return Mono.just(response);
			}
			catch (Exception ex) {
				response.setMessage(ex.getMessage());
				response.setStatus(404);
				response.setStatusText("ERROR");
				return Mono.just(response);
			}
		})
		.switchIfEmpty(Mono.error(new RuntimeException("Error en Servicio --- POSICION EN TIEMPO REAL ---")));
	}

	@Override
	public Mono<Response<Coordinador>> contactaTuCoordinador(Integer in_id_cliente) {
		Response<Coordinador> response = getResponse();
		
		return Mono.<Response<Coordinador>> defer(() -> {
			logger.info("init get ListaClientesFinales");
			try {
			List<Map<String, Object>> results = jdbcTemplate.queryForList("select * from obten_tu_coordinador(?)", in_id_cliente);
			List<Coordinador> opciones = results.stream().map(m -> {
				Coordinador opcion = new Coordinador();
				opcion.setNombres(String.valueOf(m.get("nombres"))); 
				opcion.setAp_paterno(String.valueOf(m.get("ap_paterno")));		
				opcion.setCorreo(String.valueOf(m.get("pos_hist_long")));	
				opcion.setHorario_lunes_viernes(String.valueOf(m.get("horario_lunes_viernes")));	
				opcion.setId_cliente(Integer.valueOf(String.valueOf(m.get("id_cliente"))));
				opcion.setHorario_sabado(String.valueOf(m.get("horario_sabado")));
				opcion.setId_coodinador(Integer.valueOf(String.valueOf(m.get("id_coodinador"))));
				opcion.setId_personal(Integer.valueOf(String.valueOf(m.get("id_personal"))));
				opcion.setTelefono(String.valueOf(m.get("telefono")));
				opcion.setTiene_whatsapp(Boolean.parseBoolean(String.valueOf(m.get("tiene_whatsapp"))));
				opcion.setCorreo(String.valueOf(m.get("correo")));
				return opcion;
			}).collect(Collectors.toList());
			
			response.setObjetoRespuesta(opciones.get(0));
			
			if (opciones.size()==0) { 
				response.setMessage("NO EXISTE COORDINADOR ASIGNADO!");
				response.setStatusText("ERROR");
			} else {
				response.setMessage("SE ENCONTRO COORDINADOR!");
				response.setStatusText("OK");
				}
			response.setStatus(200);		
			return Mono.just(response);
			}
			catch (Exception ex) {
				response.setMessage(ex.getMessage());
				response.setStatus(404);
				response.setStatusText("ERROR");
				return Mono.just(response);
			}
		}).switchIfEmpty(Mono.error(new RuntimeException("Error en Servicio --- CONTACTA TU COORDINADOR ---")));
	}
	//LISTA DE REQUERIMIENTO
	@Override
	public Mono<Response<List<Requerimiento>>> listaRequerimiento(Long idCliente) {
		
		Response<List<Requerimiento>> response = new Response<>();
		List<Requerimiento> listaReq = new ArrayList<>();
		
		List<RequerimientoModel> listaModelReq = requerimientoRepository.obtenerRequerimientos(idCliente,Constantes.REQUERIMIENTO_BPA,Constantes.REQUERIMIENTO_BPM);
		
		listaModelReq.forEach(l -> {
			Requerimiento req = new Requerimiento();
			req = Util.objectToObject(Requerimiento.class, l);
			listaReq.add(req);
		});
		response.setObjetoRespuesta(listaReq);
		return Mono.just(response).onErrorResume(e -> {
			logger.info(e);
			throw new RuntimeException("error Servicio Angel xD...");
		});
	}
	

}
