package com.dsb.app.cliente.service;

import java.util.List;

import com.dsb.core.common.http.Response;
import com.dsb.core.domain.BusquedaProducto;
import com.dsb.core.domain.ClienteFinal;
import com.dsb.core.domain.Coordinador;
import com.dsb.core.domain.DetallePedido;
import com.dsb.core.domain.Posicion;
import com.dsb.core.domain.Requerimiento;
import com.dsb.core.domain.SeguimPedido;
import com.dsb.core.domain.empresa.Empresa;
import com.dsb.core.domain.producto.Producto;
import com.dsb.core.domain.seguridad.Usuario;
import com.dsb.persistence.model.empresa.EmpresaModel;

import reactor.core.publisher.Mono;

public interface ClienteService {
	Mono<Response<Void>> register(Empresa empresa);
	Mono<Response<EmpresaModel>> findById(Integer id);
	Mono<Response<List<ClienteFinal>>> listaClientesFinales(Integer id);
	Mono<Response<List<SeguimPedido>>> listaSeguimientoPedido(Integer id_ord_serv, Integer id_guia_remision,
			Integer id_cliente_final, Integer id_cliente);
	Mono<Response<List<Usuario>>> envioCorreoCambioContrasena(String from);
	Mono<Response<List<DetallePedido>>> obtenerSeguimientoPedidoDetalle(Integer id_clientee, Integer id_guiaa,
			Integer id_ord_servicioo, Integer id_seguimientoo);
	Mono<Response<BusquedaProducto>> obtenerBusquedaStockProducto(Integer id_clientee, String sku_nombre_producto);
	Mono<Response<List<Producto>>> obtenerListaProductos(Integer id_clientee, String sku_nombre_producto);
	Mono<Response<Posicion>> posicionTiempoReal(Integer in_id_seguimiento, Integer in_id_vehiculo);
	Mono<Response<Coordinador>> contactaTuCoordinador(Integer in_id_cliente);
	Mono<Response<List<Requerimiento>>> listaRequerimiento(Long idCliente);
}

