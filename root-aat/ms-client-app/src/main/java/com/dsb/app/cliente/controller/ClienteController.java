package com.dsb.app.cliente.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dsb.app.cliente.service.ClienteService;
import com.dsb.core.common.http.Response;
import com.dsb.core.domain.Requerimiento;

import reactor.core.publisher.Mono;


@RestController
@RequestMapping("/app/cliente")
public class ClienteController {
	private static final Logger logger = LogManager.getLogger(ClienteController.class);
	
	@Autowired
	ClienteService ClienteService;
	
	@GetMapping(value = "/")
	public String ping(){
		return "client-aat controller is running..";
	}
	
	@GetMapping(value = "/requerimiento/{idCliente}")
	public Mono<Response<List<Requerimiento>>> listaRequerimiento(@Valid @PathVariable Long idCliente){
		return ClienteService.listaRequerimiento(idCliente);
	}
/*
	@PostMapping(value = "/register", produces=MediaType.APPLICATION_STREAM_JSON_VALUE)
	public Mono<Response<Void>> register(@RequestBody Empresa empresa){
		return companyService.register(empresa);
	}

	@PostMapping(value = "/findById/{id}")
	public Mono<Response<EmpresaModel>> findById(@Valid @PathVariable Integer id){
		return companyService.findById(id);
	}
	
	
	@GetMapping(value = "/clientes-finales", produces=MediaType.APPLICATION_JSON_VALUE,headers="Accept=application/json")
	public Mono<Response<List<ClienteFinal>>> listaClientesFinales(@RequestParam(required=true) Integer id_cliente){
		logger.info("Ingreso a controller --- Clientes Finales ---");
	//	if(!Util.isNumeric(code)) throw new BaseControllerException("error on validate input code, isnt numeric");
		return companyService.listaClientesFinales(id_cliente);
	}
	
	@PostMapping(value = "/lista-seguimiento-pedido", produces=MediaType.APPLICATION_JSON_VALUE,headers="Accept=application/json")
	public Mono<Response<List<SeguimPedido>>> listaSeguimientoPedido(@RequestBody InListaRequerimiento objLista	){
		logger.info("Ingreso a controller --- Lista Seguimiento Pedido ---");
		logger.info("Cliente: "+ objLista.getId_cliente());
	//	if(!Util.isNumeric(code)) throw new BaseControllerException("error on validate input code, isnt numeric");
		return companyService.listaSeguimientoPedido(objLista.getId_ord_serv(),objLista.getId_guia_remision(),objLista.getId_cliente_final(),
				objLista.getId_cliente());
	}

	@PostMapping(value = "/cambio-contrasena", produces=MediaType.APPLICATION_JSON_VALUE,headers="Accept=application/json")
	public Mono<Response<List<Usuario>>> envioCorreoCambioContrasena(@RequestParam(required=true) String to){
		logger.info("Ingreso a controller ---Cambio Contrasena ---");
	//	if(!Util.isNumeric(code)) throw new BaseControllerException("error on validate input code, isnt numeric");
		return companyService.envioCorreoCambioContrasena(to); 
	} 
	
	
	@GetMapping(value = "/lista-seguimiento-pedido-detalle", produces=MediaType.APPLICATION_JSON_VALUE,headers="Accept=application/json")
	public Mono<Response<List<DetallePedido>>> seguimientoPedidoDetalle(@RequestParam(required=true) Integer id_clientee,
			@RequestParam(required=true) Integer id_guiaa, @RequestParam(required=true) Integer id_ord_servicioo,
			@RequestParam(required=true) Integer id_seguimientoo){
		logger.info("Ingreso a controller --- Lista Seguimiento Pedido Detalle ---");
	//	if(!Util.isNumeric(code)) throw new BaseControllerException("error on validate input code, isnt numeric");
		return companyService.obtenerSeguimientoPedidoDetalle(id_clientee,id_guiaa,id_ord_servicioo,id_seguimientoo); 
	} 
	
	@GetMapping(value = "/busqueda-stock-producto", produces=MediaType.APPLICATION_JSON_VALUE,headers="Accept=application/json")
	public Mono<Response<BusquedaProducto>> busquedaStockProducto(@RequestParam(required=true) Integer id_clientee, @RequestParam(required=true) String sku_nombre_producto){
		logger.info("Ingreso a controller --- Busqueda Stock Producto ---");
	//	if(!Util.isNumeric(code)) throw new BaseControllerException("error on validate input code, isnt numeric");
		return companyService.obtenerBusquedaStockProducto(id_clientee,sku_nombre_producto); 
	}
	
	@GetMapping(value = "/listar-productos", produces=MediaType.APPLICATION_JSON_VALUE,headers="Accept=application/json")
	public Mono<Response<List<Producto>>> listarProductos(@RequestParam(required=true) Integer id_clientee, @RequestParam(required=true) String sku_nombre_producto){
		logger.info("Ingreso a controller --- Listar Productos ---");
	//	if(!Util.isNumeric(code)) throw new BaseControllerException("error on validate input code, isnt numeric");
		return companyService.obtenerListaProductos(id_clientee,sku_nombre_producto); 
	}
	
	
	@GetMapping(value = "/posicion_tiempo_real", produces=MediaType.APPLICATION_JSON_VALUE,headers="Accept=application/json")
	public Mono<Response<Posicion>> posicionTiempoReal(@RequestParam(required=true) Integer in_id_seguimiento,
			@RequestParam(required=true) Integer in_id_vehiculo){
		logger.info("Ingreso a controller --- Clientes Finales ---"); 
	//	if(!Util.isNumeric(code)) throw new BaseControllerException("error on validate input code, isnt numeric");
		return companyService.posicionTiempoReal(in_id_seguimiento,in_id_vehiculo);
	}
	
	@GetMapping(value = "/contacta_tu_coordinador", produces=MediaType.APPLICATION_JSON_VALUE,headers="Accept=application/json")
	public Mono<Response<Coordinador>> contactaTuCoordinador(@RequestParam(required=true) Integer in_id_cliente){
		logger.info("Ingreso a controller --- Clientes Finales ---"); 
	//	if(!Util.isNumeric(code)) throw new BaseControllerException("error on validate input code, isnt numeric");
		return companyService.contactaTuCoordinador(in_id_cliente);
	}
	*/
}
