package com.dsb.facturacion.service;

import java.util.List;

import javax.validation.Valid;

import com.dsb.core.common.http.Response;
import com.dsb.core.domain.facturacion.Comprobante;
import com.dsb.core.domain.facturacion.Preliquidacion;

import reactor.core.publisher.Mono;

public interface FacturacionService {

	Mono<Response<List<Comprobante>>> getEstadoCuenta(@Valid String idUsuario);

	Mono<Response<Preliquidacion>> getPreliquidación(@Valid String idUsuario);

}
