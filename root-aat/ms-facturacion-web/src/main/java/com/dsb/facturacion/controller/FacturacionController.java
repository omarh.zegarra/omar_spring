package com.dsb.facturacion.controller;


import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dsb.core.common.http.Response;
import com.dsb.core.domain.facturacion.Comprobante;
import com.dsb.core.domain.facturacion.Preliquidacion;
import com.dsb.facturacion.service.FacturacionService;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/web/facturacion")
public class FacturacionController {
	@Autowired
	FacturacionService facturacionService;
	
	@GetMapping(value = "/")
	public String ping(){
		return "client/web/facturacion is running..";
	}
	
	@GetMapping(value = "/estado/cuenta/{idUsuario}")
	public Mono<Response<List<Comprobante>>> getEstadoCuenta(@Valid @PathVariable String idUsuario){
		return facturacionService.getEstadoCuenta(idUsuario);
	}
	
	@GetMapping(value = "/preliquidación/{idUsuario}")
	public Mono<Response<Preliquidacion>> getPreliquidación(@Valid @PathVariable String idUsuario){
		return facturacionService.getPreliquidación(idUsuario);
	}
}