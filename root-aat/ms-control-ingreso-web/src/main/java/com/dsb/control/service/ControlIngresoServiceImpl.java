package com.dsb.control.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Service;

import com.dsb.core.common.http.Response;
import com.dsb.core.domain.controlIngreso.Cita;
import com.dsb.core.domain.controlIngreso.Visita;

import reactor.core.publisher.Mono;

@Service
public class ControlIngresoServiceImpl implements ControlIngresoService{

	@Override
	public Mono<Response<List<Cita>>> getCitas(@Valid String idUsuario) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Mono<Response<List<Visita>>> getVisitas(@Valid String idUsuario) {
		// TODO Auto-generated method stub
		return null;
	}

}
