package com.dsb.control.service;

import java.util.List;

import javax.validation.Valid;

import com.dsb.core.common.http.Response;
import com.dsb.core.domain.controlIngreso.Cita;
import com.dsb.core.domain.controlIngreso.Visita;

import reactor.core.publisher.Mono;

public interface ControlIngresoService {

	Mono<Response<List<Cita>>> getCitas(@Valid String idUsuario);

	Mono<Response<List<Visita>>> getVisitas(@Valid String idUsuario);

}
