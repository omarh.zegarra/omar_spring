package com.dsb.control.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dsb.control.service.ControlIngresoService;
import com.dsb.core.common.http.Response;
import com.dsb.core.domain.controlIngreso.Cita;
import com.dsb.core.domain.controlIngreso.Visita;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/web/control")
public class ControlIngresoController {
	@Autowired
	ControlIngresoService controlIngresoService;
	
	@GetMapping(value = "/")
	public String ping(){
		return "client/web/control is running..";
	}
	
	@GetMapping(value = "/ingresos/{idUsuario}")
	public Mono<Response<List<Cita>>> getCitas(@Valid @PathVariable String idUsuario){
		return controlIngresoService.getCitas(idUsuario);
	}
	
	@GetMapping(value = "/salidas/{idUsuario}")
	public Mono<Response<List<Visita>>> getVisitas(@Valid @PathVariable String idUsuario){
		return controlIngresoService.getVisitas(idUsuario);
	}
}

