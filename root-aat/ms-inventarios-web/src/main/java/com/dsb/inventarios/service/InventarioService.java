package com.dsb.inventarios.service;

import java.util.List;

import javax.validation.Valid;

import com.dsb.core.common.http.Response;
import com.dsb.core.common.wrapper.InventarioWrapper;
import com.dsb.core.domain.Requerimiento;
import com.dsb.core.domain.cliente.Acta;
import com.dsb.core.domain.cliente.Ingreso;
import com.dsb.core.domain.producto.Producto;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface InventarioService {

	Mono<Response<Ingreso>> getStock(@Valid String idUsuario);

	Mono<Response<List<Producto>>> getBalanceInventario(@Valid String idUsuario);

	Mono<Response<Producto>> getKardex(@Valid String idUsuario);

	Mono<Response<Acta>> getInformes(@Valid String idUsuario);

	Mono<Response<Boolean>> crearRequerimiento(Requerimiento requerimiento);

	Mono<Response<Requerimiento>> obtenerRequerimientoXid(@Valid Long idRequerimiento);

	Flux<Response<InventarioWrapper>> obtenerRequerimientos(@Valid Long idCliente);

	Mono<Response<Boolean>> validacionFechaCorte() throws Exception;
	
	Flux<Response<InventarioWrapper>> filtrarRequerimientos(@Valid Long idcliente,  String estado);
	
	Mono<Response<InventarioWrapper>> obtenerWrapperInventario(@Valid Long idCliente);
}
