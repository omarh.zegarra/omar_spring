package com.dsb.inventarios.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dsb.core.common.base.BaseService;
import com.dsb.core.common.http.Response;
import com.dsb.core.common.wrapper.InventarioWrapper;
import com.dsb.core.domain.ProductoXrequerimiento;
import com.dsb.core.domain.Requerimiento;
import com.dsb.core.domain.cliente.Acta;
import com.dsb.core.domain.cliente.Ingreso;
import com.dsb.core.domain.producto.Producto;
import com.dsb.core.util.Constantes;
import com.dsb.core.util.Util;
import com.dsb.persistence.model.requerimiento.ProductoXrequerimientoINVmodel;
import com.dsb.persistence.model.requerimiento.RequerimientoINVmodel;
import com.dsb.persistence.model.requerimiento.RequerimientoMasterINVmodel;
import com.dsb.persistence.repository.ClienteRepository;
import com.dsb.persistence.repository.ProductoRepository;
import com.dsb.persistence.repository.RequerimientoMasterINVrepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class InventarioServiceImpl extends BaseService implements InventarioService{
	private static final Logger logger = LogManager.getLogger(InventarioServiceImpl.class);
	
	@Autowired
	RequerimientoMasterINVrepository requerimientoRepository;

	@Autowired
	ClienteRepository clienteRepository;
	
	@Autowired
	ProductoRepository productoRepository;
	
	@Override
	public Mono<Response<Ingreso>> getStock(@Valid String idUsuario) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Mono<Response<List<Producto>>> getBalanceInventario(@Valid String idUsuario) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Mono<Response<Producto>> getKardex(@Valid String idUsuario) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Mono<Response<Acta>> getInformes(@Valid String idUsuario) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Mono<Response<Boolean>> crearRequerimiento(Requerimiento requerimiento) {
		logger.info("init save requerimientos...");
		Response<Boolean> res = new Response<>();
		Integer idEstado=Constantes.idEstadoRequerimiento.get(requerimiento.getAccion());
		requerimiento.setIdEstado(idEstado);
		String estado=Constantes.estadoRequerimiento.get(requerimiento.getAccion());
		requerimiento.setEstado(estado);
		
		Integer idActividadLogistica=Constantes.idActividadLogistica.get(requerimiento.getActividadLogistica());
		requerimiento.setIdActividadLogistica(idActividadLogistica);		
		String actividadLogistica=Constantes.actividadLogistica.get(requerimiento.getActividadLogistica());
		requerimiento.setActividadLogistica(actividadLogistica);
		return Mono.<Response<Boolean>> defer(() -> transactionTemplate.execute(status -> {
			requerimiento.setCodigo(obtenerCodigoTabla("REQ"));
			RequerimientoMasterINVmodel reqMasterModel=Util.objectToObject(RequerimientoMasterINVmodel.class, requerimiento);
			
			RequerimientoINVmodel reqModel=new RequerimientoINVmodel();
			reqModel=reqMasterModel.getRequerimientoINV();
			reqModel.getDetallesRequerimientos().clear();
			reqModel.setRequerimiento(null);
			
			Set<ProductoXrequerimientoINVmodel> setDetalleReqINVmodel=new HashSet<>(0);	
			for(ProductoXrequerimiento req:  requerimiento.getRequerimientoINV().getDetallesRequerimientos()) {		
				ProductoXrequerimientoINVmodel det=Util.objectToObject(ProductoXrequerimientoINVmodel.class, req);
				det.setRequerimientoINV(reqModel);
				//det.getRequerimientoINV().getDetallesRequerimientos().clear();
				setDetalleReqINVmodel.add(det);		
			}

			reqModel.setDetallesRequerimientos(setDetalleReqINVmodel);
			reqModel.setRequerimiento(reqMasterModel);
			reqMasterModel.setRequerimientoINV(reqModel);
			//reqMasterModel.getRequerimientoINV().setRequerimiento(reqMasterModel);
			requerimientoRepository.save(reqMasterModel);
			res.setObjetoRespuesta(true);   
			return Mono.just(res);
		}))
		.onErrorResume(e -> {
			logger.info("error on save requerimiento"+e);
			throw new RuntimeException("error on save requerimiento...");
		})	
		.subscribeOn(jdbcScheduler);
	}

	@Override
	public Mono<Response<Requerimiento>> obtenerRequerimientoXid(@Valid Long idRequerimiento) {
		logger.info("init obtener requerimiento...");
		Response<Requerimiento> response = new Response<>();	
		return Mono.just(requerimientoRepository.findById(idRequerimiento))
			.switchIfEmpty(Mono.empty())
			.flatMap(x -> {   
				if(!x.isPresent()) return Mono.just(response);
				RequerimientoMasterINVmodel reqModel=x.get();
				
				if(reqModel.getRequerimientoINV()!=null) {
					reqModel.getRequerimientoINV().setRequerimiento(null);
					reqModel.getRequerimientoINV().getDetallesRequerimientos()
					.forEach(y -> {
							y.setRequerimientoINV(null);
						}					
					);
				}
								
				Requerimiento req=Util.objectToObject(Requerimiento.class,reqModel);
				response.setObjetoRespuesta(req);
				return Mono.just(response);
			})
			.onErrorResume(e -> {
				logger.info("error al obtener requerimiento"+e);
				throw new RuntimeException();
			});
	}

	@Override
	public Flux<Response<InventarioWrapper>> obtenerRequerimientos(@Valid Long idCliente) {
		
		logger.info("init obtenerRequerimientos...");
		Response<InventarioWrapper> response = new Response<>(); 
		
		//Mono<Response<List<Parametro>>> parametros = obtenerParametros(Constantes.PARAMETRO_BPD);
		InventarioWrapper wrapper = new InventarioWrapper();
		
		List<RequerimientoMasterINVmodel> lista=requerimientoRepository.obtenerRequerimientoXidCliente(idCliente, Constantes.REQUERIMIENTO_INV);
		lista.forEach(x -> {
			x.setRequerimientoINV(null);
		});
		
		wrapper.setRequerimientos(Util.listObjectToListObject(Requerimiento.class, lista));
		//wrapper.setParametros(parametros.block().getObjetoRespuesta());
				
		response.setObjetoRespuesta(wrapper);		
		return Flux.just(response)
			.onErrorResume(e -> {
				logger.info("error al obtener requerimientos"+e);
				throw new RuntimeException();
		});		
	}

	@Override
	public Mono<Response<Boolean>> validacionFechaCorte() throws Exception {
		Response<Boolean> response=new Response<>();
		return Mono.just(validaFechaCorteRequerimiento())
			.switchIfEmpty(Mono.error(new RuntimeException("error on save requerimiento...")))
			.flatMap(x -> {
				response.setObjetoRespuesta(x);
				return Mono.just(response);
			}).onErrorResume(e -> {
					logger.info(e);
					throw new RuntimeException("error on save requerimiento...");
			});
	}

	@Override
	public Flux<Response<InventarioWrapper>> filtrarRequerimientos(@Valid Long idcliente, String estado) {
		logger.info("init obtener requerimientos...");
		Response<InventarioWrapper> response = new Response<>();
		
		InventarioWrapper wrapper = new InventarioWrapper();
		List<RequerimientoMasterINVmodel> lista = requerimientoRepository.filtroRequerimiento(idcliente, estado);
		lista.forEach(x -> x.setRequerimientoINV(null));
		
		wrapper.setRequerimientos(Util.listObjectToListObject(Requerimiento.class, lista));
		response.setObjetoRespuesta(wrapper);
					
		return Flux.just(response)
		.onErrorResume(e -> {
			logger.info(e);
			throw new RuntimeException();
		});
	}

	@Override
	public Mono<Response<InventarioWrapper>> obtenerWrapperInventario(@Valid Long idCliente) {
		logger.info("init obtener requerimientos...");
		Response<InventarioWrapper> response = new Response<>();		
		InventarioWrapper wrapper = new InventarioWrapper();
		return Mono.just(requerimientoRepository.obtenerRequerimientoPorIdCliente(idCliente))
				.flatMap(y->{ 
					y.forEach(x -> x.setRequerimientoINV(null));
					wrapper.setRequerimientos(Util.listObjectToListObject(Requerimiento.class, y));
					wrapper.setParametros(obtenerParametros("INV").block());				
					response.setObjetoRespuesta(wrapper);				
					return Mono.just(response)
						.onErrorResume(e -> {
								logger.info(e);
								throw new RuntimeException();
						});
				}).onErrorResume(e -> {
					logger.info(e);
					throw new RuntimeException();
				});
	}
	
	
}
