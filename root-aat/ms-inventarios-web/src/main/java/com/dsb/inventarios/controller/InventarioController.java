package com.dsb.inventarios.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dsb.core.common.http.Response;
import com.dsb.core.common.wrapper.InventarioWrapper;
import com.dsb.core.domain.Requerimiento;
import com.dsb.core.domain.cliente.Acta;
import com.dsb.core.domain.cliente.Ingreso;
import com.dsb.core.domain.producto.Producto;
import com.dsb.inventarios.service.InventarioService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/web/inventario")
public class InventarioController {
	@Autowired
	InventarioService inventarioService;
	
	@GetMapping(value = "/")
	public String ping(){
		return "client/web/inventario is running..";
	}
	
	@GetMapping(value = "/stock/{idUsuario}")
	public Mono<Response<Ingreso>> getStock(@Valid @PathVariable String idUsuario){
		return inventarioService.getStock(idUsuario);
	}
	
	@GetMapping(value = "/balance/inventario/{idUsuario}")
	public Mono<Response<List<Producto>>> getBalanceInventario(@Valid @PathVariable String idUsuario){
		return inventarioService.getBalanceInventario(idUsuario);
	}
	
	@GetMapping(value = "/kardex/{idUsuario}")
	public Mono<Response<Producto>> getKardex(@Valid @PathVariable String idUsuario){
		return inventarioService.getKardex(idUsuario);
	}
	
	@GetMapping(value = "/informes/{idUsuario}")
	public Mono<Response<Acta>> getInformes(@Valid @PathVariable String idUsuario){
		return inventarioService.getInformes(idUsuario);
	}
	
	@PostMapping(value = "/crear/requerimiento/",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Mono<Response<Boolean>> crearRequerimiento(@RequestBody Requerimiento requerimiento){
		return inventarioService.crearRequerimiento(requerimiento);
	}
	
	@PostMapping(value = "/obtener/requerimiento/{idRequerimiento}",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Mono<Response<Requerimiento>> obtenerRequerimientoXid(@Valid @PathVariable Long idRequerimiento){
		return inventarioService.obtenerRequerimientoXid(idRequerimiento);
	}
	
	@PostMapping(value = "/obtener/requerimientos/{idCliente}",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Flux<Response<InventarioWrapper>> obtenerRequerimientos(@Valid @PathVariable Long idCliente){
		return inventarioService.obtenerRequerimientos(idCliente);
	}
	
	@PostMapping(value = "/fecha/corte",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Mono<Response<Boolean>> validaFechaCorteRequerimiento() throws Exception{
		return inventarioService.validacionFechaCorte();
	}
	
	@PostMapping(value = "/filtrar/requerimientos/{idCliente}/{estado}",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Flux<Response<InventarioWrapper>> filtrarRequerimientos(@Valid @PathVariable Long idCliente, @Valid @PathVariable String estado){
		return inventarioService.filtrarRequerimientos(idCliente, estado);
	}
	
	@PostMapping(value = "/obtener/inv/{idCliente}",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Mono<Response<InventarioWrapper>> obtenerWrapperBPInv(@Valid @PathVariable Long idCliente) throws Exception{
		return inventarioService.obtenerWrapperInventario(idCliente);
	}
}
