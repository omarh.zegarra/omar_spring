package com.dsb.bpdyt.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.dsb.core.common.base.BaseService;
import com.dsb.core.common.http.Response;
import com.dsb.core.common.wrapper.BPDTwrapper;
import com.dsb.core.domain.ClienteFinal;
import com.dsb.core.domain.Requerimiento;
import com.dsb.core.util.Constantes;
import com.dsb.core.util.Util;
import com.dsb.persistence.model.ClienteFinalModel;
import com.dsb.persistence.model.ClienteModel;
import com.dsb.persistence.model.requerimiento.RequerimientoBPDyTmodel;
import com.dsb.persistence.model.requerimiento.RequerimientoMasterBPDyTmodel;
import com.dsb.persistence.repository.ClienteRepository;
import com.dsb.persistence.repository.ProductoRepository;
import com.dsb.persistence.repository.RequerimientoMasterBPDyTrepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class BpdytServiceImpl extends BaseService implements BpdytService{
	private static final Logger logger = LogManager.getLogger(BpdytServiceImpl.class);

	@Autowired
	RequerimientoMasterBPDyTrepository requerimientoBPDyTRepository;
	
	@Autowired
	ClienteRepository clienteRepository;
	
	@Autowired
	ProductoRepository productoRepository;
	
	
	@Override
	public Mono<Response<Boolean>> crearRequerimiento(@Valid Requerimiento requerimiento) {
		logger.info("init save requerimientos...");
		Response<Boolean> res = new Response<>();

		Integer idEstado=Constantes.idEstadoRequerimiento.get(requerimiento.getAccion());
		requerimiento.setIdEstado(idEstado);
		String estado=Constantes.estadoRequerimiento.get(requerimiento.getAccion());
		requerimiento.setEstado(estado);
		
		Integer idActividadLogistica=Constantes.idActividadLogistica.get(requerimiento.getActividadLogistica());
		requerimiento.setIdActividadLogistica(idActividadLogistica);		
		String actividadLogistica=Constantes.actividadLogistica.get(requerimiento.getActividadLogistica());
		requerimiento.setActividadLogistica(actividadLogistica);
		
		return Mono.<Response<Boolean>> defer(() -> transactionTemplate.execute(status -> {
			requerimiento.setCodigo(obtenerCodigoTabla("REQ"));
			RequerimientoMasterBPDyTmodel reqMasterModel=Util.objectToObject(RequerimientoMasterBPDyTmodel.class, requerimiento);
			
			RequerimientoBPDyTmodel reqmodel=new RequerimientoBPDyTmodel();
			reqmodel=reqMasterModel.getRequerimientoBPDyT();
			reqMasterModel.setRequerimientoBPDyT(reqmodel);
			reqMasterModel.getRequerimientoBPDyT().setRequerimiento(reqMasterModel);		
			
			return Mono.just(requerimientoBPDyTRepository.save(reqMasterModel))
				.switchIfEmpty(Mono.error(new RuntimeException("error on save requerimiento...")))
				.flatMap(x -> {
					res.setObjetoRespuesta(true);   
					return Mono.just(res);
				})
				.onErrorResume(e -> {
					logger.info("error on save requerimiento"+e);
					throw new RuntimeException("error on save requerimiento...");
				});	
		})).subscribeOn(jdbcScheduler);
	}

	@Override
	public Mono<Response<Requerimiento>> obtenerRequerimientoXid(@Valid Long idRequerimiento) {
		logger.info("init obtener requerimiento xid...");
		Response<Requerimiento> response = new Response<>();	
		return Mono.just(requerimientoBPDyTRepository.findById(idRequerimiento))
			.switchIfEmpty(Mono.empty())
			.flatMap(x -> {   
				if(!x.isPresent()) return Mono.just(response);
				RequerimientoMasterBPDyTmodel reqModel=x.get();				
				Requerimiento req=Util.objectToObject(Requerimiento.class,reqModel);
				response.setObjetoRespuesta(req);
				return Mono.just(response);
			})
			.onErrorResume(e -> {
				logger.info("error al obtener requerimiento"+e);
				throw new RuntimeException();
			});
	}

	@Override
	public Flux<Response<BPDTwrapper>> obtenerRequerimientos(@Valid Long idCliente, String estado) {
		logger.info("init listar obtenerRequerimientos...");
		Response<BPDTwrapper> response = new Response<>(); 
		//Mono<Response<List<Parametro>>> parametros = obtenerParametros(Constantes.PARAMETRO_BPD);
		BPDTwrapper wrapper = new BPDTwrapper();
		
		List<Requerimiento> listaBPDyT=new ArrayList<>();
		List<RequerimientoMasterBPDyTmodel> listaMaster = new ArrayList<>();

		listaMaster = requerimientoBPDyTRepository.obtenerRequerimientoXidCliente(idCliente,Constantes.REQUERIMIENTO_BPDT, estado);
		listaMaster.forEach(x -> {
			if(StringUtils.isEmpty(x.getRequerimientoBPDyT())) return;			
			String clienteFinal=x.getRequerimientoBPDyT().getClienteFinal();
			Date fechaCreacion=x.getFechaCreacion();
			
			x.setRequerimientoBPDyT(null);		
			Requerimiento req = Util.objectToObject(Requerimiento.class,x);
			req.setClienteFinal(clienteFinal);
			req.setFechaCreacion(fechaCreacion);
			listaBPDyT.add(req);
		});

		//wrapper.setParametros(parametros.block().getObjetoRespuesta());
		wrapper.setRequerimientos(Util.listObjectToListObject(Requerimiento.class, listaBPDyT));
	    response.setObjetoRespuesta(wrapper);
		return Flux.just(response)
			.onErrorResume(e -> {
				logger.info("error al obtener requerimientos"+e);
				throw new RuntimeException();
			});
	}

	@Override
	public Flux<Response<BPDTwrapper>> filtrarRequerimientos(@Valid Long idcliente, String estado){
		logger.info("init obtener requerimientos...");
		Response<BPDTwrapper> response = new Response<>();
		
		BPDTwrapper wrapper = new BPDTwrapper();
		List<RequerimientoMasterBPDyTmodel> lista = requerimientoBPDyTRepository.filtroRequerimiento(idcliente, estado);
		lista.forEach(x -> x.setRequerimientoBPDyT(null));
		
		wrapper.setRequerimientos(Util.listObjectToListObject(Requerimiento.class, lista));
		response.setObjetoRespuesta(wrapper);
					
		return Flux.just(response)
		.onErrorResume(e -> {
			logger.info(e);
			throw new RuntimeException();
		});
	}
	
	@Override
	public Mono<Response<Boolean>> validacionFechaCorte() throws Exception {
		Response<Boolean> response=new Response<>();
		return Mono.just(validaFechaCorteRequerimiento())
			.switchIfEmpty(Mono.error(new RuntimeException("error on save requerimiento...")))
			.flatMap(x -> {
				response.setObjetoRespuesta(x);
				return Mono.just(response);
			}).onErrorResume(e -> {
					logger.info(e);
					throw new RuntimeException("error on save requerimiento...");
			});
	}

	@Override
	public Mono<Response<BPDTwrapper>> obtenerWrapperBPDT(@Valid Long idCliente) {
		logger.info("init obtener requerimientos...");
		Response<BPDTwrapper> response = new Response<>();		
		BPDTwrapper wrapper = new BPDTwrapper();
		return Mono.just(requerimientoBPDyTRepository.obtenerRequerimientoPorIdCliente(idCliente))
				.flatMap(y->{
					y.forEach(x -> x.setRequerimientoBPDyT(null));
					wrapper.setRequerimientos(Util.listObjectToListObject(Requerimiento.class, y));
					
					Optional<ClienteModel> optClienteModel=clienteRepository.findById(idCliente);
					if(optClienteModel.isPresent()) {	
						List<ClienteFinal> clientesFinales= new ArrayList<>();
						ClienteModel clienteModel=optClienteModel.get();		
						Set<ClienteFinalModel> clientesFinalesModel=clienteModel.getClientesFinales();
						
						clientesFinalesModel.forEach(x->{
							x.setCliente(null);
							ClienteFinal clienteFinal = Util.objectToObject(ClienteFinal.class,x);
							clientesFinales.add(clienteFinal);
						});
						wrapper.setClientesFinales(clientesFinales);
					}
					wrapper.setParametros(obtenerParametros("BPD").block());				
					response.setObjetoRespuesta(wrapper);				
					return Mono.just(response)
						.onErrorResume(e -> {
								logger.info(e);
								throw new RuntimeException();
						});
				}).onErrorResume(e -> {
					logger.info(e);
					throw new RuntimeException();
				});
	}

}
