package com.dsb.bpdyt.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dsb.bpdyt.service.BpdytService;
import com.dsb.core.common.http.Response;
import com.dsb.core.common.wrapper.BPDTwrapper;
import com.dsb.core.domain.Requerimiento;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/web/transporte")
public class BpydController {
	@Autowired
	BpdytService bpdytService;
	
	@GetMapping(value = "/")
	public String ping(){
		return "web/transporte is running..";
	}
	
	@PostMapping(value = "/crear/requerimiento/",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Mono<Response<Boolean>> crearRequerimiento(@RequestBody Requerimiento requerimiento){
		return bpdytService.crearRequerimiento(requerimiento);
	}
	
	@PostMapping(value = "/obtener/requerimiento/{idRequerimiento}",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Mono<Response<Requerimiento>> obtenerRequerimientoXid(@Valid @PathVariable Long idRequerimiento){
		return bpdytService.obtenerRequerimientoXid(idRequerimiento);
	}
	
	@PostMapping(value = "/obtener/requerimientos/{idCliente}",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Flux<Response<BPDTwrapper>> obtenerRequerimientos(@Valid @PathVariable Long idCliente, @RequestParam(required = false, defaultValue = "")String estado){
		return bpdytService.obtenerRequerimientos(idCliente, estado);
	}
	
	@PostMapping(value = "/fecha/corte",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Mono<Response<Boolean>> validaFechaCorteRequerimiento() throws Exception{
		return bpdytService.validacionFechaCorte();
	}
	
	@PostMapping(value = "/filtrar/requerimientos/{idCliente}/{estado}",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Flux<Response<BPDTwrapper>> filtrarRequerimientos(@Valid @PathVariable Long idCliente, @Valid @PathVariable String estado){
		return bpdytService.filtrarRequerimientos(idCliente, estado);
	}
	
	@PostMapping(value = "/obtener/bpd/{idCliente}",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Mono<Response<BPDTwrapper>> obtenerWrapperBPDT(@Valid @PathVariable Long idCliente) throws Exception{
		return bpdytService.obtenerWrapperBPDT(idCliente);
	}
}
