package com.dsb.bpdyt.service;

import javax.validation.Valid;

import com.dsb.core.common.http.Response;
import com.dsb.core.common.wrapper.BPDTwrapper;
import com.dsb.core.domain.Requerimiento;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface BpdytService {

	Mono<Response<Boolean>> crearRequerimiento(@Valid Requerimiento requerimiento);

	Mono<Response<Requerimiento>> obtenerRequerimientoXid(@Valid Long idRequerimiento);

	Flux<Response<BPDTwrapper>> obtenerRequerimientos(@Valid Long idCliente, String estado);

	Mono<Response<Boolean>> validacionFechaCorte() throws Exception;
	
	Flux<Response<BPDTwrapper>> filtrarRequerimientos(@Valid Long idcliente,  String estado);
	
	Mono<Response<BPDTwrapper>> obtenerWrapperBPDT(@Valid Long idCliente);

}
