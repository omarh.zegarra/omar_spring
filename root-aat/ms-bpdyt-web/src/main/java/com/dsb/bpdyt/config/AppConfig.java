package com.dsb.bpdyt.config;

import java.util.concurrent.Executor;

import javax.sql.DataSource;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.dsb.core.common.base.BaseConfig;

@Configuration
@PropertySource(value = "file:${app.properties.path}db_polinsumos.properties", ignoreResourceNotFound = true)
@ComponentScan({"com.dsb.bpdyt.*","com.dsb.util.*"})
public class AppConfig extends BaseConfig {
	
	@Bean(name = "threadPoolTaskExecutor")
	public Executor getAsyncExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(10);
		executor.setMaxPoolSize(10);
		executor.setQueueCapacity(1000000);
		executor.setThreadNamePrefix("databasePoolExecutor-");
		executor.initialize();
		return executor;
	}
	
	@Primary
	@Bean
	@ConfigurationProperties(prefix = "spring.datasource")
	public DataSource dataSource() {
		return DataSourceBuilder.create().build();
	}
	
	@Bean
	public SimpleJdbcCall  simpleJdbcCall () {
		return new SimpleJdbcCall (jdbcTemplate ()) ;
	}
	
	@Bean
	public JdbcTemplate  jdbcTemplate () {
		return new JdbcTemplate (dataSource());
	}
}