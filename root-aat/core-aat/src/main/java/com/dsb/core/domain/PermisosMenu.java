package com.dsb.core.domain;

import lombok.Data;

@Data
public class PermisosMenu {

	Integer id_rol;
	String nombre_menu;
	Integer id_acceso_menu;	
}