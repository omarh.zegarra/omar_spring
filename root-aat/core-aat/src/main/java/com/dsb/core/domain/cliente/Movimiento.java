package com.dsb.core.domain.cliente;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Movimiento {
	String tipo;
	Double volumen;
	Integer semana;	
}
