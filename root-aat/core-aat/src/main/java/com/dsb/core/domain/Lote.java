package com.dsb.core.domain;

import lombok.Data;

@Data
public class Lote {
	Long idLote;
	String descripcion;
	String envoltura;
	Double volumen;
	Integer total;
	Integer cantidadDisponible;
	Integer cantidadConsumida;
}
