package com.dsb.core.domain;

import java.util.List;

public class Autenticacion {
	Usuario usuario;
	List<PermisosMenu> permisosMenu;
	
	
	public Autenticacion() {}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<PermisosMenu> getPermisosMenu() {
		return permisosMenu;
	}

	public void setPermisosMenu(List<PermisosMenu> permisosMenu) {
		this.permisosMenu = permisosMenu;
	};
	
}
