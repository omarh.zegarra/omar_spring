package com.dsb.core.common.wrapper;

import java.util.List;

import com.dsb.core.domain.ClienteFinal;
import com.dsb.core.domain.Parametro;
import com.dsb.core.domain.Requerimiento;

import lombok.Data;

@Data
public class BPDTwrapper {

	List<Requerimiento> requerimientos;
	List<Parametro> parametros;
	List<ClienteFinal> clientesFinales;	
}
