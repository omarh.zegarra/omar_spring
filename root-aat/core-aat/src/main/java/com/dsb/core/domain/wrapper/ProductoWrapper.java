package com.dsb.core.domain.wrapper;

import java.util.List;

import com.dsb.core.domain.Parametro;
import com.dsb.core.domain.producto.Producto;

import lombok.Data;

@Data
public class ProductoWrapper {
	List<Producto> productos;
	List<Parametro> parametros;
}
