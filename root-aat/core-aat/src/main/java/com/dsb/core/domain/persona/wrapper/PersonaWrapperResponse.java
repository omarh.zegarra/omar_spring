package com.dsb.core.domain.persona.wrapper;

import lombok.Data;

@Data
public class PersonaWrapperResponse {

	private Long idPersona;

}
