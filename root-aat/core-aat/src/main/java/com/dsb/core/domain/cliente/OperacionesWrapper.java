package com.dsb.core.domain.cliente;

import java.util.List;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class OperacionesWrapper {
	List<Movimiento> movimientos;
	List<Excedente> excedentes;
	Cumplimiento cumplimiento;
	EntregaRecojo entregaRecojo;
	CumplimientoCita cumplimientoCita;
}
