package com.dsb.core.domain.cliente;

import lombok.Data;

@Data
public class OpcionesMenu {
	Integer idAccesoMenu;
	Integer idAccesoSubMenu;
	Integer idRol;
	Integer idMenu;
	Integer idItem;
	
	String modulo;
	String rol;
	String menu;
	String item;
	
}
