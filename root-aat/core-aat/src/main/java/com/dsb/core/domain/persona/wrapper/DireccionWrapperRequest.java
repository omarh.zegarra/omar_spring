package com.dsb.core.domain.persona.wrapper;

import java.util.List;

import com.dsb.core.domain.persona.Direccion;

import lombok.Data;

@Data
public class DireccionWrapperRequest {

	private Direccion direccion;

	private List<TelefonoWrapperRequest> listaTelefono;

}