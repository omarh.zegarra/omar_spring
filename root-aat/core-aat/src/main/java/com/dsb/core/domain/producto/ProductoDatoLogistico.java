package com.dsb.core.domain.producto;

import lombok.Data;

@Data
public class ProductoDatoLogistico {
	Integer idDatoLogistico;
	
	Integer idTipoEnvaseInmediato;
	Double idTipoEnvaseInmediatoAncho;
	Double idTipoEnvaseInmediatoAlto;
	Double idTipoEnvaseInmediatoLargo;
	Double idTipoEnvaseInmediatoPeso;
	
	/*
	Integer unidadesEnvaseMediato3;
	Double volumenEnvaseMediato3;
	Double pesoEnvaseMediato3;
	*/
	
	Integer idTipoEnvaseMediato2;
	Integer unidadesEnvaseMediato2;
	Double volumenEnvaseMediato2;
	Double pesoEnvaseMediato2;
	
	Integer idTipoEnvaseMediato3;
	Integer unidadesEnvaseMediato3;
	Double volumenEnvaseMediato3;
	Double pesoEnvaseMediato3;
	
	Integer idTipoEnvaseCajaMaster;
	Integer unidadesEnvaseCajaMaster;
	Double volumenEnvaseCajaMaster;
	Double pesoEnvaseCajaMaster;
	
	Integer idEANenvaseMediato2;
	Integer idEANenvaseMediato3;
	Integer idEANenvaseCajaMaster;
	
	String cajaMasterXcama;
	Integer camaXpallet;
	Integer idTipoPallet;
	Integer totalUnidadesXpallet;
	Integer idTipoCarga;
}
