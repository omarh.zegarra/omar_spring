package com.dsb.core.common.http;

import com.dsb.core.common.wrapper.ResponseWrapper;

public class Response<T>  extends ResponseWrapper{
	private T objetoRespuesta;
	
	public Response(){super.initIt();}
	
	public Response(T objetoRespuesta) {
		super(null,null);
		this.objetoRespuesta = objetoRespuesta;
	}

	public Response(Integer status,String statusText,String message) {
		this.setStatus(status);
		this.setStatusText(statusText);
		this.setMessage(message);
	}
	
	public T getObjetoRespuesta() {
		return objetoRespuesta;
	}

	public void setObjetoRespuesta(T objetoRespuesta) {
		this.objetoRespuesta = objetoRespuesta;
	}
	
}
