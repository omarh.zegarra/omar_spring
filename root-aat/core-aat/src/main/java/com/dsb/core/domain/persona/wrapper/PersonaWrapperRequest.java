package com.dsb.core.domain.persona.wrapper;

import java.util.List;

import com.dsb.core.domain.persona.Correo;
import com.dsb.core.domain.persona.Persona;
import com.dsb.core.domain.persona.PersonaJuridica;
import com.dsb.core.domain.persona.PersonaNatural;

import lombok.Data;

@Data
public class PersonaWrapperRequest {

	private String entidadPersona;
	private Persona persona;
	private PersonaNatural personaNatural;
	private PersonaJuridica personaJuridica;
	private List<Correo> listaCorreo;
	private List<DireccionWrapperRequest> listaDireccion;
	private List<TelefonoWrapperRequest> listaTelefono;

}
