package com.dsb.core.domain.cliente;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Vehiculo {
	String modelo;
	String placa;
	Integer entregas;
}
