package com.dsb.core.domain;

import lombok.Data;

@Data
public class DetallePedido {
	Integer id_orden_servicio;  
	String codigo_orden_servicio;
	Integer id_guia; 
	String codigo_guia; 
	Integer id_seguimiento; 
	String estado_seguimiento;
	Integer kilometros_recorrido;
	String hora_salida;
	String hora_inicio_ruta;
	String hora_llegada;
	String hora_finalizacion;
	String tipo; 
	String sub_tipo; 
	String pos_hist_lat; 
	String pos_hist_long; 
	Integer id_archivo_conformidad; 
	String directorio_conformidad;
	Integer id_archivo_entrega_reco; 
	String directorio_documento; 
	Integer punto_partida; 
	String lon_pu_partida;
	String lat_pu_partida; 
	Integer punto_llegada; 
	String lon_pu_llegada; 
	String lat_pu_llegada; 
	Integer id_cliente_final;
	String razon_social;
	String direccion;
	String placa;
	Integer id_vehiculo;
	String tiempo_en_ruta;
	String tipo_rs;
	String act_logistica;
}
