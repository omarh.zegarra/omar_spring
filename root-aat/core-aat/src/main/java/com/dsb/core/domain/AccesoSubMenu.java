package com.dsb.core.domain;



import lombok.Data;

@Data
public class AccesoSubMenu {

	String nombre_sub_menu;
	Integer id_acceso_sub_menu;	
}
