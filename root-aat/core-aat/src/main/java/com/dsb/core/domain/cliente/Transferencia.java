package com.dsb.core.domain.cliente;

import java.security.Timestamp;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Transferencia {
	String codigo;
	Timestamp fecha;
	String statusOrigen;
	String statusDestino;
	String numeroRequerimiento;
	String ordenServicio;
	Integer unidades;
	Double volumen;
}
