package com.dsb.core.domain;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class Requerimiento implements Serializable{
	private static final long serialVersionUID = 1L;
	
	String codigo;
	Integer idRequerimiento;
	Integer idCliente;
	String cliente;
	String fechaRequerimiento;
	Integer tipoRequerimiento;
	Boolean activo;
	Integer idEstado;
	String estado;
	Integer idActividadLogistica;
	String actividadLogistica;
	String creadoPor;
	Integer totalUnidades;
	Double volumen;
	RequerimientoBPA requerimientoBPA;
	RequerimientoBPM requerimientoBPM;
	RequerimientoBPDyT requerimientoBPDyT;
	RequerimientoINV requerimientoINV;
	String clienteFinal;
	String documento;
	Date fechaCreacion;
	String accion;
	String procesoPrincipal;
	String ordenServicio;
	String fechaFinalizacion;
}

