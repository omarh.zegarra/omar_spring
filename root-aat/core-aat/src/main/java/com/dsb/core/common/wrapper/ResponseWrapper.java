package com.dsb.core.common.wrapper;

import javax.annotation.PostConstruct;

import com.dsb.core.util.Enumeradores;

public class ResponseWrapper{
	Integer status;
	String statusText;
	String message;
	String path;
	
	@PostConstruct
	public void initIt(){
		this.statusText=Enumeradores.StatusText.OK.name();
		this.status=200;
	}
	
	public ResponseWrapper(){}
	
	public ResponseWrapper(String statusText,String message){
		this.statusText=statusText;
		this.message=message;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getStatusText() {
		return statusText;
	}
	public void setStatusText(String statusText) {
		this.statusText = statusText;
	}	
}
