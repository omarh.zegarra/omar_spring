package com.dsb.core.domain.empresa;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

import lombok.Data;
/**
 * @Data is a convenient shortcut annotation that bundles the features of 
 * @ToString, @EqualsAndHashCode, @Getter / @Setter and @RequiredArgsConstructor together.
 */
@Data
public class Sucursal implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer idEmpresa;
	private String idSucursal;
	private Timestamp dateCreate;
	private Timestamp dateModify;
	private String descripcion;
	private String idEstado;
	private String terminalCreate;
	private String terminalModify;
	private String userCreate;
	private String userModify;
	private Oficina oficina;
	private Set<Oficina> oficinas;
}