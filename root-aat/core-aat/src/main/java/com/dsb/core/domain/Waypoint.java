package com.dsb.core.domain;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Waypoint {
	String ubicacionActual;
	String estado;
	String nombre;
	String direccion;
}
