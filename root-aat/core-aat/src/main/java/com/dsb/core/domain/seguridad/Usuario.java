package com.dsb.core.domain.seguridad;

import java.util.List;

import com.dsb.core.domain.cliente.OpcionesMenu;

public class Usuario {
	private Integer id;
	private String codigoRol;
	private String nombres;
	private String apellidoPaterno;	
	private String apellidoMaterno;	
	private String correo;	
	private String clave;
	private Boolean estado;
	private String numeroCelular;
	private String claveCelular;		
	private Token token;
	private String claveGenerada;	
	private List<OpcionesMenu> opcionesMenu;	
	private Boolean activo;
	
	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Usuario(String nombres, String correo,String clave,String codigoRol) {
		this.nombres=nombres;
		this.correo=correo;
		this.clave=clave;
		this.codigoRol=codigoRol;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodigoRol() {
		return codigoRol;
	}

	public void setCodigoRol(String codigoRol) {
		this.codigoRol = codigoRol;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public String getNumeroCelular() {
		return numeroCelular;
	}

	public void setNumeroCelular(String numeroCelular) {
		this.numeroCelular = numeroCelular;
	}

	public String getClaveCelular() {
		return claveCelular;
	}

	public void setClaveCelular(String claveCelular) {
		this.claveCelular = claveCelular;
	}

	public Token getToken() {
		return token;
	}

	public void setToken(Token token) {
		this.token = token;
	}

	public String getClaveGenerada() {
		return claveGenerada;
	}

	public void setClaveGenerada(String claveGenerada) {
		this.claveGenerada = claveGenerada;
	}

	public List<OpcionesMenu> getOpcionesMenu() {
		return opcionesMenu;
	}

	public void setOpcionesMenu(List<OpcionesMenu> opcionesMenu) {
		this.opcionesMenu = opcionesMenu;
	}

	public Usuario() {}	
}