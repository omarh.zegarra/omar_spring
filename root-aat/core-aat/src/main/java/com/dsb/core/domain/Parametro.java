package com.dsb.core.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter

public class Parametro implements Serializable{
	
	public Parametro() {}
	
	private static final long serialVersionUID = 1L;
	
	Integer idParametro;
	Integer idHijo;
	Integer idPadre;
	String valor1;
	String valor2;
	String descripcion;
	Boolean estado;
}	
