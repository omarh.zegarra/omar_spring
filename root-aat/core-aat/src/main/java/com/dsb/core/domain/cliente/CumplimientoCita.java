package com.dsb.core.domain.cliente;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class CumplimientoCita {
	String dentroHoraProgramada;
	String fueraHoraProgramada;
	Double promedioRetraso;
	Double promedioAntesHora;
}
