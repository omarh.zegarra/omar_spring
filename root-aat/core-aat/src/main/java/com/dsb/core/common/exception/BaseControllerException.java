package com.dsb.core.common.exception;

public class BaseControllerException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public BaseControllerException(String msg) {
		super(msg);
	}
}
