package com.dsb.core.util;

import lombok.Data;

@Data
public class Enumeradores {
	
	private String statusText;

	public enum StatusText {
		OK, ERROR
	}

	public String getStatusText() {
		return statusText;
	}

	public void setStatusText(String statusText) {
		this.statusText = statusText;
	}
	
	private String estadoRequerimiento;
	
	public enum EstadoRequerimiento {
		GUARDAR, ENVIAR
	}
}