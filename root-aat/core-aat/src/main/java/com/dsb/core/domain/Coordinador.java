package com.dsb.core.domain;

import lombok.Data;

@Data
public class Coordinador {
	Integer id_cliente;
	Integer id_coodinador;
	String horario_lunes_viernes;
	String horario_sabado; 
	Integer id_personal; 
	String nombres; 
	String ap_paterno;
	String telefono;
	String correo; 
	Boolean tiene_whatsapp;
}
