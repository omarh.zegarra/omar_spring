package com.dsb.core.domain;

import com.dsb.core.domain.producto.Producto;

import lombok.Data;

@Data
public class Cliente {
	Long idCliente;
	Integer idCoordinador;
	Integer idArchivoBpm;
	String ruc;
	String razonSocial;
	Boolean activo;
	String acronimo;
	String telefono;
	String distrito;	
	String departamento;
	String provincia;
	String direccion;
	String correo;	
	Producto producto;
	ClienteFinal clienteFinal;
}
