package com.dsb.core.domain.wrapper;

import java.util.List;

import com.dsb.core.domain.cliente.Acta;
import com.dsb.core.domain.cliente.Ingreso;
import com.dsb.core.domain.cliente.Salida;
import com.dsb.core.domain.cliente.Transferencia;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class AlmacenWrapper {
	List<Ingreso> ingresos;
	List<Salida> salidas;
	List<Transferencia> transferencias;
	List<Acta> actas;
}
