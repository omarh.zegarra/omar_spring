package com.dsb.core.domain;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class LineaPedido {
	Long idLineaPedido;
	Integer unidadesRequeridas;
	//String rotuloProducto;
	Integer stockDisponible;
	private Pedido pedido;   
}
