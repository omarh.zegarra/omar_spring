package com.dsb.core.domain.session;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;


@Data
@Entity(name = "session_rotulo")
@Table(name = "rotulo")
public class SessionRotulo implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_rotulo",nullable = false)
	Long idRotulo;
	
	@Column(name = "nombre",nullable = false)
	String nombre;
	
	@Column(name = "activo",nullable = false)
	Boolean activo;
	
	@Column(name = "descripcion",nullable = false)
	String descripcion;
	
	@ManyToOne(cascade= CascadeType.PERSIST)
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente", nullable = false)
	SessionCliente sessionCliente;
}
