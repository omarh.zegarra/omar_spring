package com.dsb.core.domain.empresa;

import lombok.Data;

@Data
public class Configuracion {

	private Integer idConfiguracionEmpresa;
	private Integer idEmpresa;
	private String idMoneda;
	private String indCombustibleMant;
	private String indDireccionEmisor;
	private String indDireccionPrestacion;
	private String indDscto;
	private String indEmisorItinerante;
	private String indExportacion;
	private String indIsc;
	private String indOperGratis;
	private String indOtrosCargos;
	private String indPagoAnticipado;
	private String tipoPago;
}
