package com.dsb.core.domain.persona;

import lombok.Data;

@Data
public class PersonaTelefono {

	private Long idTelefono;

	private Long idPersona;

	private Long idEmpresa;

}