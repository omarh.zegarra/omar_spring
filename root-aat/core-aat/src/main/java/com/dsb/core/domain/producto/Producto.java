package com.dsb.core.domain.producto;

import lombok.Data;

@Data
public class Producto {
	String codigo;
	Integer idProducto;
	Integer idCliente;
	String sku; 
	String nombre;
	String presentacion;
	String temperatura;
	Float precioUnitario;
	Boolean aplicaFechaVencimiento;
	String nombreIngles;
	Float precioTotal;
	Integer stockDisponible;
		
	Boolean productoPeligroso;
	Boolean poseeCodigoBarra;
	Double stockAlertaMinimo;
	Double stockAlertaMaximo;
	Integer puntoReposicion;
	Double volumen;
	Boolean activo;
	
	ProductoDatoBPA productoDatoBPA;
	ProductoDatoLogistico productoDatoLogistico;
}
