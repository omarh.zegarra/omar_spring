package com.dsb.core.domain;
import lombok.Data;

@Data
public class Usuario {
	
	Integer id_cliente;
	String nombre_usuario;
	String nombre_persona;
	String correo_usuario;	
	String razon_social;
	Integer id_archivo;
}
