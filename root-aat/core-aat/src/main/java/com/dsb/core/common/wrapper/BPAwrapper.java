package com.dsb.core.common.wrapper;

import java.util.List;

import com.dsb.core.domain.ClienteFinal;
import com.dsb.core.domain.Parametro;
import com.dsb.core.domain.Requerimiento;
import com.dsb.core.domain.Rotulo;
import com.dsb.core.domain.producto.Producto;

import lombok.Data;


@Data
public class BPAwrapper {	
	List<Requerimiento> requerimientos;
	List<Parametro> parametros;
	List<ClienteFinal> clientesFinales;	
	List<Producto> productos;
	List<Rotulo> rotulos;	
}
