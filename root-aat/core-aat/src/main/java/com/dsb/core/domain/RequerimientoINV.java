package com.dsb.core.domain;

import java.io.Serializable;
import java.util.Set;

import lombok.Data;


@Data
public class RequerimientoINV implements Serializable{

	private static final long serialVersionUID = 1L;
	Integer idTipoInventario;
	Boolean esCiego;
	String detalleInventario;
	Set<ProductoXrequerimiento> detallesRequerimientos;
}
