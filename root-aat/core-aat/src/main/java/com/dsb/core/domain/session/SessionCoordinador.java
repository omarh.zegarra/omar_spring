package com.dsb.core.domain.session;

import lombok.Data;

@Data
public class SessionCoordinador {
	
	Long idCoordinador;
	Long idCliente;
	String horarioLunesViernes;
	String horarioSabado;
	Boolean activo;
	Integer idPersonal;
	String nombres;
	String correo;
	String telefono;
}
