package com.dsb.core.domain.session;

import java.io.Serializable;

import lombok.Data;

@Data
public class SessionLote implements Serializable{

	private static final long serialVersionUID = 1L;
	Long idLote;
	String descripcion;
	String envoltura;
	Double volumen;
	Integer total;
	Integer cantidadDisponible;
	Integer cantidadConsumida;	
}
