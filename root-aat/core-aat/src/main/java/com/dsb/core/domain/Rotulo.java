package com.dsb.core.domain;

import java.io.Serializable;

import lombok.Data;

@Data
public class Rotulo implements Serializable{

	private static final long serialVersionUID = 1L;

	Long idRotulo;
	String nombre;
	Boolean activo;
	String descripcion;
}
