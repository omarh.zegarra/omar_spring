package com.dsb.core.domain.persona;

import com.dsb.core.common.wrapper.ResponseWrapper;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class PersonaWrapper extends ResponseWrapper {

	Persona persona;

}
