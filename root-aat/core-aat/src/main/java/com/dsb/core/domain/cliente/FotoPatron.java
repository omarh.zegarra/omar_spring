package com.dsb.core.domain.cliente;

import java.util.List;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class FotoPatron {
	String procesoPrincipal;
	List<String> procesosSecundarios;
	String tipoEnvase;
}
