package com.dsb.core.domain.persona;

import lombok.Data;

@Data
public class Ubigeo {

	private String idUbigeo;
	private String idDepartamento;
	private String nombreDepartamento;
	private String idProvincia;
	private String nombreProvincia;
	private String idDistrito;
	private String nombreDistrito;
	private String idPostal;

}
