package com.dsb.core.domain;

import lombok.Data;

@Data
public class SeguimPedido {
	Integer id_guia_remisionn;
	String codigo_guia;	
	Integer id_seguimiento;
	Integer id_servicio;
	Integer id_orden_servicio;
	String codigo_ord_servicio;
	Integer id_cliente_final;
	String razon_social_cli_final;
	String nombre_estado_seguimiento;
	Integer id_estado_seguimiento;
}
