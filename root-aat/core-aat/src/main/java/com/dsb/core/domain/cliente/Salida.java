package com.dsb.core.domain.cliente;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Builder
@Data@EqualsAndHashCode(callSuper=false)
public class Salida extends Orden{
	String clienteFinal;
}
