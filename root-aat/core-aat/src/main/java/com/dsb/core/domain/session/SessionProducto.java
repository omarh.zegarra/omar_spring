package com.dsb.core.domain.session;

import java.io.Serializable;
import java.util.Set;

import lombok.Data;

@Data
public class SessionProducto implements Serializable{
	
	private static final long serialVersionUID = 1L;

	Long idProducto;
	String codigo; 
	String sku; 
	String nombre;
	Double precioUnitario;
	Boolean productoPeligroso;
	Boolean poseeCodigoBarra;
	Boolean aplicaFechaVencimiento;
	Double stockAlertaMinimo;
	Double stockAlertaMaximo;
	Integer puntoReposicion;
	String nombreIngles;
	Double volumen;
	Boolean activo;
	Set<SessionLote> sessionLotes;	
}
