package com.dsb.core.domain.cliente;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Cumplimiento {
	String dentroHora;
	String fueraHora;
}
