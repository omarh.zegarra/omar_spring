package com.dsb.core.domain.producto;

import lombok.Data;

@Data
public class ProductoDatoBPA {
	
	String fabricante;
	String presentacion;
	String concentracion;
	Integer tiempoExcursion;
	String registroSanitario;
	Integer idTipoTemperatura;
	Integer idTipoForma;
	Producto producto;
}
