package com.dsb.core.common.base;

import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
//import org.aspectj.lang.annotation.Before;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import reactor.core.publisher.Mono;

@Aspect
@Component
public abstract class BaseAspect extends UtilService{
	private static final Logger logger = LogManager.getLogger(BaseAspect.class);
 
	//@Before("execution(* com.hra.*..controller.*.*(..)) && execution(public * *(..))")
	public Object verifyToken(final JoinPoint proceedingJoinPoint) throws Throwable {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		
		Object value;
		try {
			value = proceedingJoinPoint.getTarget();		
			String headerBearer = request.getHeader("Authorization");
			logger.info("aspect:Authorization token..."+headerBearer);
			String accessToken = request.getHeader("accessToken");
			logger.info("aspect:accessToken..."+accessToken);			
			Boolean bol=bolVerifyToken(accessToken);
			if(!bol) throw new RuntimeException("error token invalido...");
		} catch (Throwable throwable) {
			throw throwable;
		} finally {
			logger.info("{} {} from {}", request.getMethod(), request.getRequestURI(), request.getRemoteAddr(),
					request.getHeader("user-id"));
		}
		logger.info("value..." + value);
		return value;
	}

	public Boolean bolVerifyToken(String bearerToken) {
		try { 	
			Mono<Boolean> bol=  Mono.fromCallable(() ->callApi(new ParameterizedTypeReference<Boolean>(){},null,HttpMethod.POST,"http://localhost:8084/token/verifytoken/"+bearerToken)); 
		    return bol.block();		    	
		}catch(Exception e) { 
			logger.info("error al validar token..."+e.getMessage());
		    return false;
		}
	}
}
