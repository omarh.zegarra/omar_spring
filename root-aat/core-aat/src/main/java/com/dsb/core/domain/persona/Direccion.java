package com.dsb.core.domain.persona;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class Direccion extends Auditoria implements Serializable{

	private static final long serialVersionUID = -2707209034230030555L;

	private Long idEmpresa;

	private Long idDireccion;

	private String tipoDireccion;

	private String descripcion;

	private String idUbigeo;

	private String tipoVia;

	private String zona;

	private String tipoZona;

	private String numeroDireccion;

	private String numeroKilometro;

	private String numeroInterior;

	private String numeroLote;

	private String numeroDepartamento;

	private String numeroManzana;

	private String idEstado;

	//private List<String> propertiesUpdate;
}