package com.dsb.core.domain.cliente;

import java.sql.Timestamp;
import lombok.Data;

@Data
public class Orden {
	String codigo;
	Timestamp fecha;
	String tipo;
	String documento;
	String numeroRequerimiento;
	String ordenServicio;
	Integer unidades;
}
