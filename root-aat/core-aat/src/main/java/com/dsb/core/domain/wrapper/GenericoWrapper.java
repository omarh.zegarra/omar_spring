package com.dsb.core.domain.wrapper;

import com.dsb.core.domain.session.SessionCliente;

import lombok.Data;

@Data
public class GenericoWrapper {
	SessionCliente sessionCliente;
}
