package com.dsb.core.common.wrapper;

import java.util.List;

import com.dsb.core.domain.seguridad.Usuario;

public class UsuarioWrapper extends ResponseWrapper{
	public UsuarioWrapper(String statusText,String message) {
		super(statusText, message);
	}
	public UsuarioWrapper() {}
	
	Usuario usuario;
	List<Usuario> usuarios;
	
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public List<Usuario> getUsuarios() {
		return usuarios;
	}
	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}
}
