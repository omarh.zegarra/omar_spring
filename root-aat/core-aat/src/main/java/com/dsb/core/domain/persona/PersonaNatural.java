package com.dsb.core.domain.persona;

import java.util.List;

import lombok.Data;

@Data
public class PersonaNatural {

	private Long idPersonaNatural;
	private Long idEmpresa;
	private String tipoDocumentoIdentidad;
	private String numeroDocumentoIdentidad;
	private String primerNombre;
	private String segundoNombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private List<String> propertiesUpdate;

}
