package com.dsb.core.domain;

import java.io.Serializable;

import lombok.Data;

@Data
public class RequerimientoBPDyT implements Serializable{	
	private static final long serialVersionUID = 1L;
	
	Integer idRequerimientoBPDyT;
	String documento;
	Integer idTipoServicio;
	String fechaRequerida;
	String ventanaHoraInicio;
	String ventanaHoraFin;
	String nombresContacto;
	String telefonoContacto;
	Integer idClienteFinal;	
	String clienteFinal;
	String direccionRecojo;
	String caracteristicasRecojo;
	String tiempoAnticipacionRecojo;
}
