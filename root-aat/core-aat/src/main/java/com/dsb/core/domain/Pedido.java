package com.dsb.core.domain;

import java.sql.Timestamp;
import java.util.Set;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Pedido {
	Timestamp fechaHoraRequerida;
	String tipoSalida;
	String tipoDocumento1;
	String numeroDocumento1;
	String tipoDocumento2;
	String numeroDocumento2;
	String condiciones;
	Integer entregaAAT;
	Integer entrega;
	String distritoOprovincia;
	String clienteFinal;	
	Integer estado;
	Set<LineaPedido> lineaPedido;
}
