package com.dsb.core.domain.empresa;

import java.util.Set;

import lombok.Data;

@Data
public class Empresa {
    Integer idEmpresa;
    String ruc;
    String razonSocial;
    String usuarioSunat;
    String idEstado;
    Configuracion configuracionEmpresa;
    Sucursal sucursal;
    Certificado certificado;   
    Set<Configuracion> configutaciones;
    Set<Sucursal> sucursales;
    Set<Certificado> certificados;
}
