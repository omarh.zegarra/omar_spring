package com.dsb.core.domain.wrapper;

import java.util.List;

import com.dsb.core.domain.Waypoint;
import com.dsb.core.domain.cliente.Vehiculo;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class BpdytWrapper {
	List<Vehiculo> vehiculos;
	List<Waypoint> waypoints;
}
