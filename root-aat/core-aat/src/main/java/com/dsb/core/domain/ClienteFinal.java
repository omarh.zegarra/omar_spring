package com.dsb.core.domain;

import lombok.Data;

@Data
public class ClienteFinal {	
	Long idClienteFinal;
	String ruc;
	String razonSocial;
	Boolean activo;
	String direccion;
	String descripcion;	
}
