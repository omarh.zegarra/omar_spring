package com.dsb.core.domain.persona;

import lombok.Data;

@Data
public class DireccionTelefono {

	private Long idTelefono;

	private Long idDireccion;

	private Long idEmpresa;

}