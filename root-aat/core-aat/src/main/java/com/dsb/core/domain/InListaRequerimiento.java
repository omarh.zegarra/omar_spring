package com.dsb.core.domain;

import lombok.Data;

@Data
public class InListaRequerimiento {

Integer id_ord_serv;
Integer id_guia_remision;
Integer id_cliente_final;
Integer id_cliente;
}
