package com.dsb.core.common.base;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import com.dsb.core.common.http.Response;
import com.dsb.core.domain.Parametro;
import com.dsb.core.util.PropertiesUtil;

import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;

public abstract class BaseService extends UtilService{
	private static final Logger logger = LogManager.getLogger(BaseService.class);
	
	protected String urlAccessToken;
	protected String urlRefreshToken;
	protected String urlServiceSecurity;
	protected String urlServiceEmail;
	protected String urlCore;
	protected String urlOauth;
	protected String emailFrom;
	protected String defaultUser;
	protected String defaultEmail;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
		
	@Autowired
	private PropertiesUtil propertiesUtil;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private HttpServletResponse response;
	
	@Autowired(required=true)
	protected PasswordEncoder passwordEncoder;
	
	@Autowired
	protected TransactionTemplate transactionTemplate;

	@Autowired
	@Qualifier("jdbcScheduler")
	public Scheduler jdbcScheduler;
	
	@Autowired
	protected WebClient webClient;
	
	@PostConstruct
	void init(){
		urlServiceSecurity=propertiesUtil.getPropertiesString("security.path.url");
		urlOauth=propertiesUtil.getPropertiesString("oauth2.path.url");
		urlAccessToken=propertiesUtil.getPropertiesString("accesstoken.path.url");
		urlRefreshToken=propertiesUtil.getPropertiesString("refreshtoken.path.url");		
		defaultUser=propertiesUtil.getPropertiesString("user.default.pass");
		defaultEmail=propertiesUtil.getPropertiesString("user.default.email");
		urlCore=propertiesUtil.getPropertiesString("core.path.url");
	}	

	public Integer getStatusOk() {	
		return Integer.valueOf(200);
	}
	
	public String getPathApi() {
		return request.getRequestURI();
	}
	
	public Integer getStatusError() {
		return response.getStatus();
	}
	
	public <T> Response<T> getResponse() {
		Response<T> res=new Response<>();
		res.setPath(request.getRequestURI());
		res.setStatus(getStatusOk());
		return res;
	}
	
	@Autowired(required=true)
	JavaMailSender javaMailSender;
	
	public void envioCorreo(String from, String to, String subject, String body) throws InterruptedException {	
		logger.info("start send email");
		SimpleMailMessage message = new SimpleMailMessage();
		message.setFrom(from);
		message.setTo(to);
		message.setSubject(subject);
		message.setText(body);
		javaMailSender.send(message);
		logger.info("end send email");
	}

	public String obtenerCodigoTabla(String codigoTabla) {		
		return jdbcTemplate.queryForObject("select fn_genera_codigo_tabla(?)", String.class,codigoTabla);
	}
	
	public Boolean validaFechaCorteRequerimiento() throws Exception{	
		return jdbcTemplate.queryForObject("select fn_valida_fecha_corte(?)",Boolean.class, "xx");		
	}

	public Mono<List<Parametro>> obtenerParametros(String tipo){
		logger.info("load parametros..");
		return Mono.just(jdbcTemplate.queryForList("select * from fn_obtener_parametros(?)", tipo)
			.stream().map(m -> {
				Parametro parametro = new Parametro();
				parametro.setIdHijo(Integer.parseInt(String.valueOf(m.get("id_hijo"))));
				parametro.setIdPadre(Integer.parseInt(String.valueOf(m.get("id_padre"))));
				parametro.setValor1((String.valueOf(m.get("valor1"))));
				parametro.setValor2((String.valueOf(m.get("valor2"))));
				parametro.setDescripcion((String.valueOf(m.get("descripcion"))));
				return parametro;	
			}).collect(Collectors.toList())
		).onErrorResume(e -> {
			logger.info(e);
			throw new RuntimeException();
		});
	}
}