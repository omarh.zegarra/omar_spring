package com.dsb.core.domain;

import lombok.Data;

@Data
public class Posicion {
	Integer id_seguimiento;
	Integer id_vehiculo; 
	String pos_hist_lat;
	String pos_hist_long;
}
