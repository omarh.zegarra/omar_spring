package com.dsb.core.domain.cliente;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Excedente {
	Double volumen;
	String dia;
}
