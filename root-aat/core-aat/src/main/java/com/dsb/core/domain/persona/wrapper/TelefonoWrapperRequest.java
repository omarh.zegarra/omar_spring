package com.dsb.core.domain.persona.wrapper;

import java.util.List;

import com.dsb.core.domain.persona.Anexo;
import com.dsb.core.domain.persona.Telefono;

import lombok.Data;

@Data
public class TelefonoWrapperRequest {

	private Telefono telefono;
	private List<Anexo> listaAnexo;

}
