package com.dsb.core.domain.empresa;

import java.sql.Timestamp;

import lombok.Data;

@Data
public class Certificado{

	Integer idCertificado;
    Byte[] certificadoDigital;
	Timestamp  fechaInicioVig;
	Timestamp  fechaFinVig;
    String idEstado;
	String terminalCreate;
	Timestamp  dateCreate;
	String userCreate;
}

