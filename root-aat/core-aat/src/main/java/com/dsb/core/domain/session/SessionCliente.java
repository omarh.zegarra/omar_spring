package com.dsb.core.domain.session;

import java.io.Serializable;
import java.util.Set;

import lombok.Data;


@Data
public class SessionCliente implements Serializable{

	private static final long serialVersionUID = 1L;
	Long idCliente;
	Integer idCoordinador;
	Integer idArchivoBpm;
	String ruc;
	String razonSocial;
	Boolean activo;
	String acronimo;
	String telefono;
	String distrito;	
	String departamento;
	String provincia;
	String direccion;
	String correo;	
	private Set<SessionClienteFinal> sessionClientesFinales;
	private Set<SessionProducto> sessionProductos;
	private Set<SessionRotulo> sessionRotulos;
	private SessionCoordinador sessionCoordinador;
}
