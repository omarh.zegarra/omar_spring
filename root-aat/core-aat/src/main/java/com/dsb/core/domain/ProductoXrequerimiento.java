package com.dsb.core.domain;

import java.io.Serializable;

import lombok.Data;

@Data
public class ProductoXrequerimiento implements Serializable{

	private static final long serialVersionUID = 1L;
	
	Long idProductoXRequerimiento;
	Integer idProducto;
	Integer unidadesRequeridas;
	String rotuloProducto;
	Integer stockDisponible;  

	Integer unidadesTrabajar;
	Integer unidadesIngresar;
	Integer detalleProceso;
	String fechaVencimiento;
}
