package com.dsb.core.domain.cliente;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class EntregaRecojo {
	Double total;
	Double parcial;
	Double rechazo;
}
