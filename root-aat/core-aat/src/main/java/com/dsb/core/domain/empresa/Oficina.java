package com.dsb.core.domain.empresa;

import java.io.Serializable;
import java.util.Set;

import com.dsb.core.domain.persona.Direccion;

import lombok.Data;

@Data
public class Oficina implements Serializable {
	private static final long serialVersionUID = 1L;

	private String idOficina;
	private String idSucursal;
	private Integer idEmpresa;
	private String dateCreate;
	private String dateModify;
	private String descripcion;
	private String idEstado;
	private String idOficinaSunat;
	private String terminalCreate;
	private String terminalModify;
	private String userCreate;
	private String userModify;
	private Direccion direccion;
	private Set<Direccion> direcciones;
}