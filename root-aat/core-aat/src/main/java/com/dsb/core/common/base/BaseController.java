package com.dsb.core.common.base;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//@RestControllerAdvice
@RestController
@RequestMapping("/base")
public class BaseController {
	/*
	@ExceptionHandler(value = { Exception.class })
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public <T> Response<T> unKnownException(Exception ex) {
		return new Response<>(404, "Error 404 Not Found","error....");
	}	
	*/
	@GetMapping(value = "/")
	public String ping(){
		return "base controller is running..";
	}
	
	/*
	@PostMapping(value = "/fecha/corte/{diaCorte}",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Mono<Response<Boolean>> crearRequerimiento(@Valid @PathVariable String diaCorte){
		return almacenService.crearRequerimiento(requerimiento);
	}
	*/
}