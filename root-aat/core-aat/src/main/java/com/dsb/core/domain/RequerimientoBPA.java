package com.dsb.core.domain;

import java.io.Serializable;
import java.util.Set;

import lombok.Data;

@Data
public class RequerimientoBPA implements Serializable{	

	private static final long serialVersionUID = 1L;
	
	Integer idRequerimientoBPA;
	Integer idTipoSalida;
	String tipoSalida;
	Integer idTipoDocumento;
	String tipoDocumento;
	String numeroDocumento;
	Integer idTipoDocumento2;
	String tipoDocumento2;
	String numeroDocumento2;
	String condicionesEmpaque;
	Boolean entregaAAT;
	Boolean esLima;
	String direccion;
	Integer idTipoIngreso;
	String tipoIngreso;
	String detalleRequerimiento;
	Integer idTipoCarga;	
	Boolean esfurgon;	
	Boolean recojoAAT;
	String fechaHoraIngreso;
	Integer idClienteFinal;
	String clienteFinal;
	String tipoEvento;
	
	Integer idStatusOrigen;
	String statusOrigen;
	Integer idStatusDestino;
	String statusDestino;
	String detalleTransferencia;

	Integer totalUnidades;
	Double volumen;
	String codigoRequerimiento;
	String ordenServicio;
	String status;
	
	private Set<ProductoXrequerimiento> detallesRequerimientos;
}
