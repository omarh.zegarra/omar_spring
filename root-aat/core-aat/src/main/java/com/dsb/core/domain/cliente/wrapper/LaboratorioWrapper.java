package com.dsb.core.domain.cliente.wrapper;

import java.util.List;

import com.dsb.core.domain.cliente.Vehiculo;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class LaboratorioWrapper {
	List<Vehiculo> vehiculos;
	
}
