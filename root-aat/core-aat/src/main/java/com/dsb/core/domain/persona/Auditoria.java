package com.dsb.core.domain.persona;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class Auditoria implements Serializable{

	private static final long serialVersionUID = 1L;
	private String userCreate;
	private Date dateCreate;
	private String terminalCreate;
	private String userModify;
	private Date dateModify;
	private String terminalModify;
}
