package com.dsb.core.common.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

@Component
public class TokenInterceptor implements HandlerInterceptor {
	private static final Logger logger = LogManager.getLogger(TokenInterceptor.class);
	//private boolean flag=true;
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object object, Exception arg3) throws Exception {
		logger.info("Request Completed!");
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object object, ModelAndView model)
			throws Exception {
		/*if(response.getStatus()!=200 && flag) {
			//response.getWriter().print(Util.objectToJson("StatusText:ERROR"));	
			response.getOutputStream().write(Util.objectToJson("StatusText:ERROR").getBytes());
			logger.info("postHandle executed");
			flag=false;
		}*/
		//response.getOutputStream().write(Util.objectToJson("StatusText:ERROR").getBytes());
		/*response.setContentType("application/json");
		response.getOutputStream().close();
		response.getOutputStream().write(Util.objectToJson("StatusText:ERROR").getBytes());*/
		//response.getWriter().print(Util.objectToJson("StatusText:ERROR"));
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object object) throws Exception {
		logger.info("init validate bearer token...");
		/*boolean hasToken=true;
		String header = request.getHeader("Authorization");
		hasToken =(StringUtils.isEmpty(header) || !header.contains("Bearer")) ? false : true;
		if(!hasToken) {
			logger.info("invalidate not has token...");
			response.setContentType("application/json");
			BaseWrapper wrapper=new BaseWrapper(Enumeradores.Status.ERROR,"Error validate Bearer Token...");
			response.getWriter().print(Util.objectToJson(wrapper));
		}*/
		String headerBearer = request.getHeader("Authorization");
		logger.info("Authorization token..."+headerBearer);
		String headerStrToken = request.getHeader("strToken");
		logger.info("strToken token..."+headerStrToken);
		return true;
		// response.addHeader("JWT",ok?"OK":"ERROR");
	}
}