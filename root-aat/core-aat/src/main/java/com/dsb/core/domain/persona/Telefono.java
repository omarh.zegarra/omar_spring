package com.dsb.core.domain.persona;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class Telefono extends Auditoria {

	private static final long serialVersionUID = 1L;
	private Long idTelefono;
	private Long idEmpresa;
	private String numeroTelefono;
	private String tipoTelefono;
	private String indicadorMovil;
	private String idEstado;
	private List<String> propertiesUpdate;

}
