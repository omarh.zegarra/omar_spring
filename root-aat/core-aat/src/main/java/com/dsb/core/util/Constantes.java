package com.dsb.core.util;

import java.util.HashMap;
import java.util.Map;

public final class Constantes {

    private Constantes(){}
    
    public static final String PARAMETRO_BPA = "BPA";
    public static final String PARAMETRO_BPM = "BPM";
    public static final String PARAMETRO_BPD = "BPD";
    public static final String PARAMETRO_INV = "INV";
    public static final Integer COMBO_TIPO_DOCUMENTO = 1;
    public static final Integer COMBO_TIPO_SALIDA = 7;
    public static final Integer COMBO_TIPO_INGRESO = 13;
    public static final Integer COMBO_TIPO_CARGA = 19;
    public static final Integer COMBO_STATUS_ORIGEN = 22;
    public static final Integer COMBO_STATUS_DESTINO= 29;
    
    public static final Integer REQUERIMIENTO_BPA = 2;
    public static final Integer REQUERIMIENTO_BPM = 4;
    public static final Integer REQUERIMIENTO_BPDT = 5;
    public static final Integer REQUERIMIENTO_INV = 6;
    
    
    public static final String PARAMETRO_DOMINIO_TIPO_PERSONA = "000001";
    public static final String PARAMETRO_TIPO_PERSONA_NATURAL = "01";
    public static final String PARAMETRO_TIPO_PERSONA_JURIDICA = "02";
    
    public static final String PARAMETRO_DOMINIO_TIPO_ESTADO = "000002";
    public static final String PARAMETRO_TIPO_ESTADO_ACTIVO = "01";
    public static final String PARAMETRO_TIPO_ESTADO_INACTIVO = "02";
    
    public static final String PARAMETRO_DOMINIO_TIPO_TELEFONO = "000003";
    public static final String PARAMETRO_TIPO_TELEFONO_MOVIL = "01";
    public static final String PARAMETRO_TIPO_TELEFONO_FIJO = "02";
    
    public static final String INDICADOR_SI = "S";
    public static final String INDICADOR_NO = "N";
    
    public static final String PARAMETRO_DOMINIO_TIPO_DIRECCION = "000005";
    public static final String PARAMETRO_TIPO_DIRECCION_LEGAL = "01";
    public static final String PARAMETRO_TIPO_DIRECCION_DOMICILIADA = "02";
    
    public static final String PARAMETRO_DOMINIO_TIPO_CORREO = "000007";
    public static final String PARAMETRO_DOMINIO_TIPO_CORREO_CORPORATIVO = "01";
    public static final String PARAMETRO_DOMINIO_TIPO_CORREO_PERSONAL = "02";
    
    public static final String FORMATO_FECHA_LARGA = "dd/MM/YYYY HH:mm:ss";
    
    public static final String ENTIDAD_PERSONA_PROVEEDOR = "PROVEEDOR";
    public static final String ENTIDAD_PERSONA_CLIENTE = "CLIENTE";
    public static final String ENTIDAD_PERSONA_CONTACTO = "CONTACTO";
    public static final String ENTIDAD_PERSONA_VENDEDOR = "VENDEDOR";
    public static final String ENTIDAD_PERSONA_TRANSPORTE = "TRANSPORTE";
    public static final String ENTIDAD_PERSONA_LABORATORIO = "LABORATORIO";
    
    /***************************************/
    
    public static final Integer ESTADO_REQUERIMIENTO_EN_PROCESO = 1;
    public static final Integer ESTADO_REQUERIMIENTO_OBSERVADO = 2;
    
    public static final Integer ACTIVIDAD_LOGISTICA_INGRESO_BPA = 1;
    public static final Integer ACTIVIDAD_LOGISTICA_SALIDA_BPA = 2;
    public static final Integer ACTIVIDAD_LOGISTICA_TRANSFERENCIA_BPA = 3;
    public static final Integer ACTIVIDAD_LOGISTICA_BPM = 4;
    public static final Integer ACTIVIDAD_LOGISTICA_BPD = 5;
    public static final Integer ACTIVIDAD_LOGISTICA_INV = 6;
    
    public static final Map<String, Integer> idEstadoRequerimiento = new HashMap<String, Integer>(){
		private static final long serialVersionUID = 1L;{
            put("ENVIAR",ESTADO_REQUERIMIENTO_EN_PROCESO);
            put("GUARDAR",ESTADO_REQUERIMIENTO_EN_PROCESO);
            put("OBSERVADO",ESTADO_REQUERIMIENTO_OBSERVADO);
        };
    };
    
    public static final Map<String, String> estadoRequerimiento = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L;{
            put("ENVIAR","EN PROCESO");
            put("GUARDAR","BORRADOR");
            put("OBSERVADO","OBSERVADO");
        };
    };
    
    public static final Map<String, Integer> idActividadLogistica = new HashMap<String, Integer>(){
		private static final long serialVersionUID = 1L;{
            put("ING",ACTIVIDAD_LOGISTICA_INGRESO_BPA);
            put("SAL",ACTIVIDAD_LOGISTICA_SALIDA_BPA);
            put("TRA",ACTIVIDAD_LOGISTICA_TRANSFERENCIA_BPA);
            put("BPM",ACTIVIDAD_LOGISTICA_BPM);
            put("BPD",ACTIVIDAD_LOGISTICA_BPD);
            put("INV",ACTIVIDAD_LOGISTICA_INV);
        };
    };
    
    public static final Map<String, String> actividadLogistica = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L;{
            put("ING","INGRESO DE PRODUCTOS");
            put("SAL","SALIDA DE PRODUCTOS");
            put("TRA","TRANSFERENCIA DE PRODUCTOS");
            put("BPM","PROCESOS BPM");
            put("BPD","TRANSPORTE DE PRODUCTOS");
            put("INV","INVENTARIO DE PRODUCTOS");
        };
    };
}
