package com.dsb.core.domain.persona;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class Persona extends Auditoria {

	private static final long serialVersionUID = 1L;
	private Long idPersona;
	private Long idEmpresa;
	private String tipoPersona;
	private String idEstado;
	private List<String> propertiesUpdate;
}
