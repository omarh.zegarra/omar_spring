package com.dsb.core.domain;
import java.util.List;

import com.dsb.core.domain.producto.Producto;

import lombok.Data;

@Data
public class BusquedaProducto {

Producto objProducto;
List<Lote> listaLotes;

}
