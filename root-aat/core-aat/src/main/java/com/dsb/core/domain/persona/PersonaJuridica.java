package com.dsb.core.domain.persona;

import java.util.List;

import lombok.Data;

@Data
public class PersonaJuridica {

	private Long idPersonaJuridica;
	private Long idEmpresa;
	private String ruc;
	private String razonSocial;
	private String nombreComercial;
	private String sitioWebsite;
	private List<String> propertiesUpdate;

}
