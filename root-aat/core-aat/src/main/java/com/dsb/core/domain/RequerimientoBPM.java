package com.dsb.core.domain;

import java.io.Serializable;
import java.util.Set;

import lombok.Data;


@Data
public class RequerimientoBPM implements Serializable{

	private static final long serialVersionUID = 1L;
	Integer idTipoDocumento;
	String numeroDocumento;
	Integer idProcesoBPM;
	Integer idDetalleTipoDatoCarga;
	Integer idTipoDatoCarga;
	
	String procesoPrincipal;
	Boolean consolidacionCarga;
	Boolean pesoCajasMediatas;
	Boolean pesoCajasDesuso;
	Boolean trituracionDesecho;
	String fechaHoraIngreso;
	
	Boolean esFurgon;
	Boolean entregaAAT;
	Boolean recogeAAT;
	
	Integer totalUnidades;
	Double volumen;
	String codigoRequerimiento;
	String ordenServicio;
	String status;
	private Set<ProductoXrequerimiento> detallesRequerimientos;
}
