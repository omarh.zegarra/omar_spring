package com.dsb.core.domain.persona;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class Anexo extends Auditoria {

	private static final long serialVersionUID = 1L;
	private Long idAnexo;
	private Long idTelefono;
	private Long idEmpresa;
	private String numeroAnexo;
	private String idEstado;
	private List<String> propertiesUpdate;

}
