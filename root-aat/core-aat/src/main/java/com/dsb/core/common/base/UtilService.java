package com.dsb.core.common.base;

import java.nio.charset.Charset;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import reactor.core.publisher.Mono;

public class UtilService {
	private static final Logger logger = LogManager.getLogger(UtilService.class);
	
	@Autowired
	private RestTemplate restTemplate;
	
	public <T> T callApiWithBoa(ParameterizedTypeReference<T> type, Object payload, HttpMethod method, String url){
		HttpHeaders headers=getHttpHeaderWithBoa();
		return restTemplate.exchange(
			url, method,getHttpEntity(payload,headers), type
		).getBody();
	}
	
	public <T> T callApi(ParameterizedTypeReference<T> type,Object payload, HttpMethod method, String url) throws Exception{
		HttpHeaders headers=getHttpHeaderJwt();
		return restTemplate.exchange(url, method,getHttpEntity(payload,headers), type).getBody();
	}
	
	public <T> T callRestApi(ParameterizedTypeReference<T> type,Object payload, HttpMethod method, String url) throws Exception{
		HttpHeaders headers=getHttpHeaderJwt();
		return restTemplate.exchange(url, method,getHttpEntity(payload,headers), type).getBody();
	}
	
	public <T> HttpEntity<T> getHttpEntity(T t, HttpHeaders headers) {
		headers.setContentType(MediaType.APPLICATION_JSON);
		return new HttpEntity<>(t, headers);
	}
	
	public HttpHeaders getHttpHeaderJwt() {
		HttpHeaders headers = new HttpHeaders();
		try {
			headers.add("Accept",MediaType.APPLICATION_JSON_VALUE);
			headers.add("Content-Type",MediaType.APPLICATION_JSON_VALUE);
		} catch (Exception e) {
			logger.error("error al obtener headers...",e);
		}
		return headers;
	}
	
	public HttpHeaders getHttpHeaderWithBoa() {
		HttpHeaders headers = new HttpHeaders();
		try {
			headers.add("Accept", "Application/json");
			String auth = "my-trusted-client" + ":" + "secret";
	        byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
	        String authHeader = "Basic " + new String(encodedAuth);
	        headers.set( "Authorization", authHeader );  
		} catch (Exception e) {
			logger.error("error al obtener headers Boa...",e);
		}
		return headers;
	}
	
	public Mono<Boolean> verifyToken(String bearerToken) {
		return Mono.<Boolean> defer(() -> {
		    try { 
				Mono<Boolean> bol=  callApi(new ParameterizedTypeReference<Mono<Boolean>>(){},null,HttpMethod.POST,"http://localhost:8765/oauth/token/verifytoken/"+bearerToken); 
		    	return Mono.just(bol.block());		    	
		    }catch(Exception e) { 
		    	return Mono.just(false);
		    }
		}).doOnError(ex -> logger.error("error access token", ex));
	}
}
