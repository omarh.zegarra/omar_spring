package com.dsb.core.domain.persona;

import lombok.Data;

@Data
public class PersonaDireccion {

	private Long idPersona;
	private Long idDireccion;
	private Long idEmpresa;

}
