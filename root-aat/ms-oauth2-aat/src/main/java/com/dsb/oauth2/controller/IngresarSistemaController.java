package com.dsb.oauth2.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dsb.core.common.http.Response;
import com.dsb.core.domain.AccesoSubMenu;
import com.dsb.core.domain.Autenticacion;
import com.dsb.oauth2.service.IngresarSistemaService;
import com.dsb.oauth2.service.LoginServiceImpl;

import reactor.core.publisher.Mono;


@RestController
@RequestMapping("/iniciarSesion")
public class IngresarSistemaController {
private static final Logger logger = LogManager.getLogger(LoginServiceImpl.class);
	
	@Autowired
	IngresarSistemaService ingresarSistema;
	

	@GetMapping(value = "/ingreso", produces=MediaType.APPLICATION_JSON_VALUE,headers="Accept=application/json")
	public Mono<Response<Autenticacion>> AutenticarUsuario(@RequestParam(required=true) String usuario, @RequestParam(required=true) String password,
			@RequestParam(required=true) Integer perfil){
		logger.info("Ingreso a controller --- AutenticarUsuario ---");
	//	if(!Util.isNumeric(code)) throw new BaseControllerException("error on validate input code, isnt numeric");
		return ingresarSistema.autenticarse(usuario,password,perfil);
	}
	
	@GetMapping(value = "/ingreso/subMenu", produces=MediaType.APPLICATION_JSON_VALUE,headers="Accept=application/json")
	public Mono<Response<List<AccesoSubMenu>>> AccesoSubMenu(@RequestParam(required=true) Integer id_acceso_menuu,
			@RequestParam(required=true) Integer id_roll){
		logger.info("Ingreso a controller --- AccesoSubMenu ---");
	//	if(!Util.isNumeric(code)) throw new BaseControllerException("error on validate input code, isnt numeric");
		return ingresarSistema.accesoSubMenu(id_acceso_menuu,id_roll);
	}
}
