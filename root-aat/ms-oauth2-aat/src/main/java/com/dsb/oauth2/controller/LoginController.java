package com.dsb.oauth2.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dsb.core.common.exception.BaseControllerException;
import com.dsb.core.common.http.Response;
import com.dsb.core.domain.seguridad.Usuario;
import com.dsb.core.util.Util;
import com.dsb.oauth2.service.LoginService;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/login")
public class LoginController{
	
	@Autowired
	LoginService loginService;
	
	@GetMapping(value = "/ping")
	protected String test(){
		return "pong...";
	}
	
	@PostMapping(value = "/temporal",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE,headers="Accept=application/json")
	protected Mono<Response<Void>> insertUserTemporal(@Valid @RequestBody(required=true) Usuario usuario){
		if(StringUtils.isEmpty(usuario.getCorreo())) throw new BaseControllerException("error on validate email,cant be blank or whitespace");
		return loginService.insertUserTemporal(usuario);
	}
	
	@PostMapping(value = "/user/{code}",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE,headers="Accept=application/json")
	protected Mono<Response<Usuario>> validateCode(@PathVariable(required=true) String code){
		if(!Util.isNumeric(code)) throw new BaseControllerException("error on validate input code, isnt numeric");
		return loginService.validateCode(code);
	}
}