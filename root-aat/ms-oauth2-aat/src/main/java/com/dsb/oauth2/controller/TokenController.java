package com.dsb.oauth2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dsb.core.common.http.Response;
import com.dsb.core.domain.seguridad.Token;
import com.dsb.core.domain.seguridad.Usuario;
import com.dsb.oauth2.service.TokenService;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/oauth")
public class TokenController {
	@Autowired
	TokenService tokenService;

	@GetMapping(value = "/")
    protected String test() {
        return "ms-oauth is running ...";
    }
	
	@PostMapping(value = "/verifytoken/{bearerToken}",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE,headers="Accept=application/json")
    public Mono<Boolean> verifyToken(@PathVariable String bearerToken) {
        return tokenService.verifyToken(bearerToken);
    }
	
	@PostMapping(value = "/getuserwithaccesstoken",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE,headers="Accept=application/json")
    public Mono<Response<Usuario>> getAccesToken(@RequestBody Usuario usuario) {
        return tokenService.getUserWithAccessToken(usuario);
    }
	
	@PostMapping(value = "/getuseroauth",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE,headers="Accept=application/json")
    public Mono<Response<Usuario>> getUserOauthWithMenu(@RequestBody Usuario usuario) {
        return tokenService.getUserOauthWithMenu(usuario);
    }
	
	@PostMapping(value = "/getaccesstoken2",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE,headers="Accept=application/json")
    public Mono<Token> getAccesToken2(@RequestBody Usuario usuario) {
        return tokenService.getOauthToken(usuario);
    }
	
	@PostMapping(value = "/getrefreshtoken",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE,headers="Accept=application/json")
    public Mono<Response<Usuario>> getRefreshToken(@RequestBody Usuario usuario) {
        return tokenService.getRefreshToken(usuario);
    }
	
	@PostMapping(value = "/getuserwithdefaulttoken",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE,headers="Accept=application/json")
    public Mono<Response<Usuario>> getUserWithDefaultAccessToken(@RequestBody Usuario usuario) {
        return tokenService.getUserWithDefaultAccessToken(usuario);
    }
	
	@PostMapping(value = "/register",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE,headers="Accept=application/json")
    protected Mono<Response<Usuario>> register(@RequestBody Usuario usuario) {
        return tokenService.getUserWithDefaultAccessToken(usuario);
    }

	@PostMapping(value = "/getuserwithrefreshtoken",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE,headers="Accept=application/json")
    public Mono<Response<Usuario>> getUserWithRefreshToken(@RequestBody Usuario usuario) {
        return tokenService.getUserWithRefreshToken(usuario);
    }
}