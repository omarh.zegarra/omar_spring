package com.dsb.oauth2.service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.dsb.core.common.base.BaseService;
import com.dsb.core.common.http.Response;
import com.dsb.core.domain.Autenticacion;
import com.dsb.core.domain.AccesoSubMenu;
import com.dsb.core.domain.PermisosMenu;
import com.dsb.core.domain.Usuario;
import com.dsb.persistence.repository.UsuarioLoginRepository;

import reactor.core.publisher.Mono;

@Service
public class IngresarSistemaServiceImpl extends BaseService implements IngresarSistemaService {
	private static final Logger logger = LogManager.getLogger(IngresarSistemaServiceImpl.class);
	
	@Autowired
	UsuarioLoginRepository usuarioLoginRepository;
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Override
	public Mono<Response<Autenticacion>> autenticarse(final String usuario,final String password, final Integer perfil){
		Response<Autenticacion> response = getResponse();
		Autenticacion objwrapper= new Autenticacion();
		Usuario user = new Usuario();
		
		return Mono.<Response<Autenticacion>> defer(() -> {
			logger.info("init get menu..");
			try {
			List<Map<String, Object>> results = jdbcTemplate.queryForList("select * from ingresar_sistema(?,?,?)",usuario,password,perfil);
			List<PermisosMenu> opciones = results.stream().map(m -> {
				PermisosMenu opcion = new PermisosMenu();
				opcion.setId_rol(Integer.valueOf(String.valueOf(m.get("id_rol")))); 
				opcion.setNombre_menu(String.valueOf(m.get("nombre_menu")));
				opcion.setId_acceso_menu(Integer.valueOf(String.valueOf(m.get("id_acceso_menu")))); 
				user.setNombre_usuario(String.valueOf(m.get("nombre_usuario")));
				user.setNombre_persona(String.valueOf(m.get("nombre_persona")));
				user.setId_cliente(Integer.valueOf(String.valueOf(m.get("id_cliente"))));
				user.setCorreo_usuario(String.valueOf(m.get("correo")));
				user.setRazon_social(String.valueOf(m.get("razon_social")));
			//	user.setId_archivo(Integer.valueOf(String.valueOf(m.get("id_archivo_bpm"))));
				return opcion;
			}).collect(Collectors.toList());
			
			
			
			logger.info("result.."+opciones);
			
			objwrapper.setPermisosMenu(opciones);
			objwrapper.setUsuario(user);
			
			if (opciones.size()==0) { 
				response.setMessage("Ingreso de usuario o password incorrecto!");
				response.setStatusText("ERROR");
			} else {
				response.setMessage("Usuario autenticado correctamente!");
				response.setStatusText("OK");}
		
			response.setStatus(200);	
			response.setObjetoRespuesta(objwrapper);
			return Mono.just(response);
			}
			catch (Exception ex) {
				response.setMessage(ex.getMessage());
				response.setStatus(404);
				response.setStatusText("ERROR");
				return Mono.just(response);
			}
			
			
		})
		.switchIfEmpty(Mono.error(new RuntimeException("Error en Servicio AutenticarUsuario.")));
	}
	
	@Override
	public Mono<Response<List<AccesoSubMenu>>> accesoSubMenu(final Integer id_acceso_menuu,final Integer id_roll){
		Response<List<AccesoSubMenu>> response = getResponse();
		return Mono.<Response<List<AccesoSubMenu>>> defer(() -> {
			logger.info("init get menu..");
			try {
			List<Map<String, Object>> results = jdbcTemplate.queryForList("select * from ingresar_sub_menu(?,?)",id_acceso_menuu,id_roll);
			List<AccesoSubMenu> opciones = results.stream().map(m -> {
				AccesoSubMenu opcion = new AccesoSubMenu();
				opcion.setNombre_sub_menu(String.valueOf(m.get("nombre_sub_menu"))); 
				opcion.setId_acceso_sub_menu(Integer.valueOf(String.valueOf(m.get("id_acceso_sub_menu")))); 
				return opcion;
			}).collect(Collectors.toList());
			logger.info("result.."+opciones);
			
			response.setObjetoRespuesta(opciones);
			if (opciones.size()==0) { 
				response.setMessage("No contiene permiso a ningún Módulo!");
				response.setStatusText("ERROR");} 
			else {
				response.setMessage("Usuario autenticado correctamente!") ;}
				response.setStatusText("OK");
			response.setStatus(200);		
			return Mono.just(response);
			}
			catch (Exception ex){
				response.setMessage(ex.getMessage());
				response.setStatus(404);
				response.setStatusText("ERROR");
				return Mono.just(response);
			}
			
		})
		.switchIfEmpty(Mono.error(new RuntimeException("Error en Servicio AutenticarUsuario.")));
	}

}
