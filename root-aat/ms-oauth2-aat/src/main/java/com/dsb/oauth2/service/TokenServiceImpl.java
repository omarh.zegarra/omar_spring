package com.dsb.oauth2.service;

import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.dsb.core.common.base.BaseService;
import com.dsb.core.common.http.Response;
import com.dsb.core.domain.cliente.OpcionesMenu;
import com.dsb.core.domain.seguridad.Token;
import com.dsb.core.domain.seguridad.Usuario;
import com.dsb.persistence.repository.TokenRepository;

import reactor.core.publisher.Mono;

@Service
public class TokenServiceImpl extends BaseService implements TokenService {
	private static final Logger logger = LogManager.getLogger(TokenServiceImpl.class);

	@Autowired
	TokenRepository tokenRepository;

	@Autowired
	WebClient webClient;
	
	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public Mono<Token> getOauthToken(Usuario usuario) {
		String url = urlAccessToken;
		url = url.replace("uname", usuario.getCorreo()).replace("pss", usuario.getClave());		
		String auth = "my-trusted-client" + ":" + "secret";
        byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
        String authHeader = "Basic " + new String(encodedAuth);
        
		return webClient.post().uri(url)
			.body(Mono.just(usuario), Usuario.class)
			.header("Accept","Application/json")
			.header("Authorization",authHeader)	
			.retrieve()
			.bodyToMono(new ParameterizedTypeReference<Token>() {})
			.onErrorResume(e -> {
				logger.info("error on get default token user" + e);
				throw new RuntimeException("Error on get accesstoken..."+e.getMessage());
			});
	}
	
	@Override
	public Mono<Response<Usuario>> getUserWithAccessToken(Usuario usuario) {
		Response<Usuario> response = new Response<>();
		response.setPath(getPathApi());
		Mono.fromCallable(() -> getOauthToken(usuario))
		.switchIfEmpty(Mono.error(new RuntimeException("empty token...")))
		.subscribe(x -> {
			  logger.info("x>" + x.blockOptional().get());
			  usuario.setToken(x.block());
			  response.setObjetoRespuesta(usuario);			  
		 });
		return Mono.just(response);
	}

	private Mono<Token> getOauthRefreshToken(Usuario usuario) {
		String auth = "my-trusted-client" + ":" + "secret";
        byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
        String authHeader = "Basic " + new String(encodedAuth);
        String url = urlRefreshToken + usuario.getToken().getRefreshToken();
		return webClient.post().uri(url)
			.header("Accept","Application/json")
			.header("Authorization",authHeader)	
			.body(Mono.just(usuario), Usuario.class).retrieve()
			.bodyToMono(new ParameterizedTypeReference<Token>() {
			}).onErrorResume(e -> {
				logger.info("error on get refresh token..." + e);
				throw new RuntimeException("error on get refresh token..."+e.getMessage());
			});	
	}
	
	@Override
	public Mono<Response<Usuario>> getRefreshToken(Usuario usuario) {
		Response<Usuario> response = new Response<>();
		response.setPath(getPathApi());
		Mono.fromCallable(() -> getOauthRefreshToken(usuario))
		.switchIfEmpty(Mono.error(new RuntimeException("empty token...")))
		.subscribe(x -> {
			  logger.info("x>" + x.blockOptional().get());
			  usuario.setToken(x.block());
			  response.setObjetoRespuesta(usuario);			  
		  });
		return Mono.just(response);
	}

	@Override
	public Mono<Response<Usuario>> getUserWithDefaultAccessToken(Usuario usuario) {
		Response<Usuario> response = new Response<>();
		response.setPath(getPathApi());
		//Usuario defaultUser = new Usuario(getDefaultUser(),getDefaultEmail(),null,null);
		Usuario user = new Usuario();
		user.setClave(defaultUser);
		user.setCorreo(defaultEmail);
		String urlToken = urlServiceSecurity + "user/insert/";

		Mono.fromCallable(() -> getOauthToken(user))
		.onErrorResume(e -> {
			logger.info("Error on get oauth token.." + e);
			throw new RuntimeException("Error on get oauth token...");
		}).switchIfEmpty(Mono.error(new RuntimeException("Error on get oauth token...")))
		  .subscribe(x -> {
			  logger.info("x>" + x.block());
			  usuario.setToken(x.block());
			  response.setObjetoRespuesta(usuario);
		});
		
		return webClient.post().uri(urlToken)
		    .body(Mono.just(usuario), Usuario.class).retrieve()
			.bodyToMono(new ParameterizedTypeReference<Response<Usuario>>() {
			}).onErrorResume(e -> {
				logger.info("error on get default token user" + e);
				throw new RuntimeException("Error on call api security/user/insert..."+e.getMessage());
			});
	}

	@Override
	public Mono<Response<Usuario>> getUserWithRefreshToken(Usuario usuario) {
		Response<Usuario> response = new Response<>();
		response.setPath(getPathApi());
		String urlToken = urlServiceSecurity + "token/save/newtoken";

		Mono.fromCallable(() -> getOauthRefreshToken(usuario)).onErrorResume(e -> {
			logger.info("error on login user" + e);
			throw new RuntimeException("Error on get refresh token...");
		}).switchIfEmpty(Mono.error(new RuntimeException("error empty restult to get refresh token...")))
		.subscribe(x -> {
			logger.info("x>" + x.block());
			usuario.setToken(x.block());
			response.setObjetoRespuesta(usuario);
			webClient.post().uri(urlToken).body(Mono.just(usuario), Usuario.class)
				.retrieve().bodyToMono(new ParameterizedTypeReference<Response<Token>>() {
			}).doOnError(e-> Mono.error(new RuntimeException("Error on save new refresh token...")));
		});

		return webClient.post().uri(urlToken).body(Mono.just(usuario), Usuario.class)
			.retrieve().bodyToMono(new ParameterizedTypeReference<Response<Usuario>>() {
		});
	}
	
	@Override
	public Mono<Response<Usuario>> getUserOauthWithMenu(Usuario usuario) {
		Response<Usuario> response = new Response<>();
		response.setPath(getPathApi());

		Mono<Token> monoToken=getOauthToken(usuario);
		Mono<List<OpcionesMenu>> monoMenu=getMenuOpciones("");	

		return Mono.zip(monoMenu,monoToken).map(
			x->{
				System.out.println("x.getT1():"+x.getT1());
				System.out.println("x.getT2():"+x.getT2());
				usuario.setToken(x.getT2());
				usuario.setOpcionesMenu(x.getT1());			
				response.setObjetoRespuesta(usuario);
				return response;
			}
		);
	}
	 
	private Mono<List<OpcionesMenu>> getMenuOpciones(String rol) {
		return Mono.<List<OpcionesMenu>> defer(() -> {
			logger.info("init get menu..");

			List<Map<String, Object>> results = jdbcTemplate.queryForList("select * from fn_menu_dinamico(?)","henry");
			List<OpcionesMenu> opciones = results.stream().map(m -> {
				OpcionesMenu opcion = new OpcionesMenu();
				opcion.setMenu(String.valueOf(m.get("nombre_menu")));
				opcion.setItem(String.valueOf(m.get("nombre_item")));
				return opcion;
			}).collect(Collectors.toList());
					
			logger.info("result.."+opciones);
			return Mono.just(opciones);
		})
		.switchIfEmpty(Mono.error(new RuntimeException("error on register company 2")));
	}	
	
	// Alternative to Mono.defer(). we can also use Mono.fromCallable().
	/*********************************************************************************/
	@Override
	public Mono<Boolean> verifyToken(String bearerToken) {
		return Mono.<Boolean>defer(() -> {
			Mono.fromCallable(() -> tokenRepository.verifyToken(bearerToken)).onErrorResume(e -> {
				logger.info("error on verify token user..." + e);
				throw new RuntimeException("Error on login user...");
			}).switchIfEmpty(Mono.error(new RuntimeException("error on save new accesstoken...")));
			return Mono.just(Boolean.TRUE);
		}).onErrorResume(e -> {
			logger.info("Error on login user..." + e);
			throw new RuntimeException("Error in change password...");
		});
	}
}
