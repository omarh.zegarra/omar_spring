package com.dsb.oauth2.service;

import com.dsb.core.common.http.Response;
import com.dsb.core.domain.seguridad.Usuario;

import reactor.core.publisher.Mono;

public interface LoginService {
	Mono<Response<Void>> insertUserTemporal(Usuario usuario);
	Mono<Response<Usuario>> validateCode(String code);
}