package com.dsb.oauth2.service;

import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.dsb.core.common.base.BaseService;
import com.dsb.core.common.http.Response;
import com.dsb.core.domain.seguridad.Usuario;
import com.dsb.core.util.Util;
import com.dsb.persistence.model.UsuarioLoginModel;
import com.dsb.persistence.repository.UsuarioLoginRepository;

import reactor.core.publisher.Mono;

@Service
public class LoginServiceImpl extends BaseService implements LoginService {
	private static final Logger logger = LogManager.getLogger(LoginServiceImpl.class);

	@Autowired
	UsuarioLoginRepository usuarioLoginRepository;

	@Autowired
	TokenService tokenService;

	@Value("${user.mail.billsale}")
	private String emailFromBillsale;

	@Override
	public Mono<Response<Void>> insertUserTemporal(final Usuario usuario){
		Response<Void> response = getResponse();
		return Mono.fromCallable(() -> transactionTemplate.execute(status -> {
			final String claveGenerada = Util.generateHashCode(8);			
			try {		
				UsuarioLoginModel user = new UsuarioLoginModel();
				user.setNombres(usuario.getNombres());
				user.setCorreo(usuario.getCorreo());
				user.setClave(passwordEncoder.encode(usuario.getClave()));
				user.setClaveGenerada(claveGenerada);
				
				UsuarioLoginModel userResult=usuarioLoginRepository.save(user);
				Optional<UsuarioLoginModel> userOptional = Optional.ofNullable(userResult);				
				
				if(!userOptional.isPresent()) throw new RuntimeException("error on save user...");					
			//	sendEmail(emailFromBillsale, usuario.getCorreo(), "Validacion codigo", claveGenerada);				
			} catch (Exception e1) {
				throw new RuntimeException("error on sent email..." + e1.getMessage());
			}
			logger.info("end method insert user temporal");
			return response;
		}))
		.onErrorResume(e -> {
			logger.info("error on insert user temporal.." + e);
			throw new RuntimeException("Error on insert user temporally..." + e.getMessage());
		})
		.subscribeOn(jdbcScheduler);
	}

	@Override
	public Mono<Response<Usuario>> validateCode(String code) {
		Response<Usuario> response = getResponse();
		Mono.fromCallable(() -> usuarioLoginRepository.findUserByGenerateCode(code))
		.onErrorResume(e -> {
			logger.info("Error on save user.." + e);
			throw new RuntimeException("generated code dont match with the persist code..." + e.getMessage());
		}).switchIfEmpty(Mono.error(new RuntimeException("Error on validate hash code...")))
		  .subscribe(u -> {
			logger.info("u-->" + u);
			Mono<Response<Usuario>> mono = tokenService
					.getUserWithDefaultAccessToken(new Usuario(u.getNombres(), u.getCorreo(), u.getClave(), null));
			response.setObjetoRespuesta(mono.block().getObjetoRespuesta());
		});
		return Mono.just(response);
	}
}