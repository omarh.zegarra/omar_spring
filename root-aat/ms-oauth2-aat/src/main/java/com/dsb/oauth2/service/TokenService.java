package com.dsb.oauth2.service;

import com.dsb.core.common.http.Response;
import com.dsb.core.domain.seguridad.Token;
import com.dsb.core.domain.seguridad.Usuario;

import reactor.core.publisher.Mono;

public interface TokenService {
	Mono<Response<Usuario>> getUserWithRefreshToken(Usuario usuario);
	Mono<Response<Usuario>> getUserWithDefaultAccessToken(Usuario usuario);
	Mono<Boolean> verifyToken(String bearerToken);
	Mono<Response<Usuario>> getUserWithAccessToken(Usuario usuario);
	Mono<Response<Usuario>> getRefreshToken(Usuario usuario);
	Mono<Token> getOauthToken(Usuario usuario);
	Mono<Response<Usuario>> getUserOauthWithMenu(Usuario usuario);
}
