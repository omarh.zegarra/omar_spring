package com.dsb.oauth2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories("com.dsb.persistence.repository")
@SpringBootApplication
@EnableAutoConfiguration
@EntityScan("com.dsb.persistence.model")
public class App extends SpringBootServletInitializer implements CommandLineRunner{
	private static final Logger log= LogManager.getLogger(App.class);
	
    public static void main( String[] args ){
    	SpringApplication.run(App.class, args);
    }
    
    @Override
	public void run(String... arg0) throws Exception {
    	log.info("start ms-oauth..");
	}
    /*
    @Override public void onStartup( ServletContext servletContext ) throws ServletException {
        super.onStartup( servletContext );
        servletContext.addListener( new RequestContextListener() );
    }
    
    @Override public void onStartup( ServletContext servletContext ) throws ServletException {
        super.onStartup( servletContext );
        servletContext.addFilter("requestContextFilter", new RequestContextFilter() ).addMappingForUrlPatterns(null, false, "/*");
    }
    */
}