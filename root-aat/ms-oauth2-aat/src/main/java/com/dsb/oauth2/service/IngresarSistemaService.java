package com.dsb.oauth2.service;

import java.util.List;

import com.dsb.core.common.http.Response;
import com.dsb.core.domain.AccesoSubMenu;
import com.dsb.core.domain.Autenticacion;

import reactor.core.publisher.Mono;

public interface IngresarSistemaService {
	Mono<Response<Autenticacion>> autenticarse(String usuario,String password,Integer perfil);
	Mono<Response<List<AccesoSubMenu>>> accesoSubMenu(Integer id_acceso_menuu,Integer id_roll);
}
