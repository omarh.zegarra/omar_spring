package com.dsb.laboratorio.service;

import javax.validation.Valid;

import org.springframework.stereotype.Service;

import com.dsb.core.common.http.Response;
import com.dsb.core.common.wrapper.BPMwrapper;
import com.dsb.core.domain.Requerimiento;
import com.dsb.core.domain.cliente.FotoPatron;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public interface LaboratorioService {
	Mono<Response<FotoPatron>> getFotopatrones(@Valid String idUsuario);
	Mono<Response<Boolean>> crearRequerimiento(Requerimiento requerimiento);
	Mono<Response<Requerimiento>> obtenerRequerimientoXid(@Valid Long idRequerimiento);
	Flux<Response<BPMwrapper>> obtenerRequerimientos(@Valid Long idCliente, String estado);
	Mono<Response<Boolean>> validacionFechaCorte() throws Exception;
	Flux<Response<BPMwrapper>> filtrarRequerimiento(@Valid Long idCliente, String estado);
	Mono<Response<BPMwrapper>> obtenerWrapperBPM(@Valid Long idCliente);
}
