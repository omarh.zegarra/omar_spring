package com.dsb.laboratorio.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dsb.core.common.http.Response;
import com.dsb.core.common.wrapper.BPMwrapper;
import com.dsb.core.domain.Requerimiento;
import com.dsb.core.domain.cliente.FotoPatron;
import com.dsb.laboratorio.service.LaboratorioService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/web/laboratorio")
public class LaboratorioController {
	
	@Autowired
	LaboratorioService laboratorioService;
	
	@GetMapping(value = "/")
	public String ping(){
		return "client/web/laboratorio is running..";
	}
	
	@GetMapping(value = "/fotopatrones/{idUsuario}")
	public Mono<Response<FotoPatron>> getFotopatrones(@Valid @PathVariable String idUsuario){
		return laboratorioService.getFotopatrones(idUsuario);
	}
	
	@PostMapping(value = "/crear/requerimiento/",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Mono<Response<Boolean>> crearRequerimiento(@RequestBody Requerimiento requerimiento){
		return laboratorioService.crearRequerimiento(requerimiento);
	}
	
	@PostMapping(value = "/obtener/requerimiento/{idRequerimiento}",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Mono<Response<Requerimiento>> obtenerRequerimientoXid(@Valid @PathVariable Long idRequerimiento){
		return laboratorioService.obtenerRequerimientoXid(idRequerimiento);
	}
	
	@PostMapping(value = "/obtener/requerimientos/{idCliente}",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Flux<Response<BPMwrapper>> obtenerRequerimientos(@Valid @PathVariable Long idCliente, @RequestParam(required = false, defaultValue = "")String estado){
		return laboratorioService.obtenerRequerimientos(idCliente, estado);
	}
	
	@PostMapping(value = "/fecha/corte",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Mono<Response<Boolean>> validaFechaCorteRequerimiento() throws Exception{
		return laboratorioService.validacionFechaCorte();
	}
	
	@PostMapping(value = "/filtrar/requerimientos/{idCliente}/{estado}",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Flux<Response<BPMwrapper>> filtrarRequerimiento(@Valid @PathVariable Long idCliente,@PathVariable String estado){
		return laboratorioService.filtrarRequerimiento(idCliente, estado);
	}
	
	@PostMapping(value = "/obtener/bpm/{idCliente}",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Mono<Response<BPMwrapper>> obtenerWrapperBPM(@Valid @PathVariable Long idCliente) throws Exception{
		return laboratorioService.obtenerWrapperBPM(idCliente);
	}

}
