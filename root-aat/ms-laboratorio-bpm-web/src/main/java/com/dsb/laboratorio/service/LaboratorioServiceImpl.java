package com.dsb.laboratorio.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.dsb.core.common.base.BaseService;
import com.dsb.core.common.http.Response;
import com.dsb.core.common.wrapper.BPMwrapper;
import com.dsb.core.domain.ClienteFinal;
import com.dsb.core.domain.ProductoXrequerimiento;
import com.dsb.core.domain.Requerimiento;
import com.dsb.core.domain.cliente.FotoPatron;
import com.dsb.core.domain.producto.Producto;
import com.dsb.core.util.Constantes;
import com.dsb.core.util.Util;
import com.dsb.persistence.model.ClienteFinalModel;
import com.dsb.persistence.model.ClienteModel;
import com.dsb.persistence.model.requerimiento.ProductoXrequerimientoBPMmodel;
import com.dsb.persistence.model.requerimiento.RequerimientoBPMmodel;
import com.dsb.persistence.model.requerimiento.RequerimientoMasterBPMmodel;
import com.dsb.persistence.repository.ClienteRepository;
import com.dsb.persistence.repository.ProductoRepository;
import com.dsb.persistence.repository.RequerimientoMasterBPMrepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class LaboratorioServiceImpl extends BaseService implements LaboratorioService{
	private static final Logger logger = LogManager.getLogger(LaboratorioServiceImpl.class);
	
	@Autowired
	RequerimientoMasterBPMrepository requerimientoRepository;
	
	
	@Autowired
	ClienteRepository clienteRepository;
	
	@Autowired
	ProductoRepository productoRepository;
	
	
	@Override
	public Mono<Response<FotoPatron>> getFotopatrones(@Valid String idUsuario) {
		FotoPatron f=FotoPatron.builder()
			.procesoPrincipal("P1")
			.procesosSecundarios(new ArrayList<>(Arrays.asList(new String[] { "P1", "P2", "P3", "P4" })))
			.build();
		return Mono.just(new Response<FotoPatron>(f));
	}

	@Override
	public Mono<Response<Boolean>> crearRequerimiento(@Valid Requerimiento requerimiento) {
		logger.info("init save requerimientos...");
		Response<Boolean> res = new Response<>();
		Integer idEstado=Constantes.idEstadoRequerimiento.get(requerimiento.getAccion());
		requerimiento.setIdEstado(idEstado);
		String estado=Constantes.estadoRequerimiento.get(requerimiento.getAccion());
		requerimiento.setEstado(estado);
		
		Integer idActividadLogistica=Constantes.idActividadLogistica.get(requerimiento.getActividadLogistica());
		requerimiento.setIdActividadLogistica(idActividadLogistica);		
		String actividadLogistica=Constantes.actividadLogistica.get(requerimiento.getActividadLogistica());
		requerimiento.setActividadLogistica(actividadLogistica);
		return Mono.<Response<Boolean>> defer(() -> transactionTemplate.execute(status -> {
			requerimiento.setCodigo(obtenerCodigoTabla("REQ"));
			RequerimientoMasterBPMmodel reqMasterModel=Util.objectToObject(RequerimientoMasterBPMmodel.class, requerimiento);
			
			RequerimientoBPMmodel reqBPMmodel=new RequerimientoBPMmodel();
			reqBPMmodel=reqMasterModel.getRequerimientoBPM();
			reqBPMmodel.getDetallesRequerimientos().clear();
			
			Set<ProductoXrequerimientoBPMmodel> setDetalleReqBPMmodel=new HashSet<>(0);	
			for(ProductoXrequerimiento req:  requerimiento.getRequerimientoBPM().getDetallesRequerimientos()) {		
				ProductoXrequerimientoBPMmodel det=Util.objectToObject(ProductoXrequerimientoBPMmodel.class, req);
				det.setRequerimientoBPM(reqBPMmodel);
				setDetalleReqBPMmodel.add(det);		
			}
			
			reqBPMmodel.setDetallesRequerimientos(setDetalleReqBPMmodel);
			reqMasterModel.setRequerimientoBPM(reqBPMmodel);
			reqMasterModel.getRequerimientoBPM().setRequerimiento(reqMasterModel);		
			
			return Mono.just(requerimientoRepository.save(reqMasterModel))
				.switchIfEmpty(Mono.error(new RuntimeException("error on save requerimiento...")))
				.flatMap(x -> {
					res.setObjetoRespuesta(true);   
					return Mono.just(res);
				})
				.onErrorResume(e -> {
					logger.info(e);
					throw new RuntimeException("error on save requerimiento...");
				});	
		})).subscribeOn(jdbcScheduler);
	}

	@Override
	public Mono<Response<Requerimiento>> obtenerRequerimientoXid(@Valid Long idRequerimiento) {
		logger.info("init obtener requerimiento...");
		Response<Requerimiento> response = new Response<>();	
		return Mono.just(requerimientoRepository.findById(idRequerimiento))
			.switchIfEmpty(Mono.empty())
			.flatMap(x -> {   
				if(!x.isPresent()) return Mono.just(response);
				RequerimientoMasterBPMmodel reqModel=x.get();
				
				if(reqModel.getRequerimientoBPM()!=null) {
					reqModel.getRequerimientoBPM().setRequerimiento(null);
					reqModel.getRequerimientoBPM().getDetallesRequerimientos()
					.forEach(y -> {
							y.setRequerimientoBPM(null);
						}					
					);	
				}
							
				Requerimiento req=Util.objectToObject(Requerimiento.class,reqModel);
				response.setObjetoRespuesta(req);
				return Mono.just(response);
			})
			.onErrorResume(e -> {
				logger.info("error al obtener requerimiento"+e);
				throw new RuntimeException();
			});
	}

	@Override
	public Flux<Response<BPMwrapper>> obtenerRequerimientos(@Valid Long idCliente, String estado) {
		logger.info("init list obtenerRequerimientos...");
		Response<BPMwrapper> response = new Response<>(); 
		//Mono<Response<List<Parametro>>> parametros = obtenerParametros(Constantes.PARAMETRO_BPD);
		BPMwrapper wrapper = new BPMwrapper();
		
		List<Requerimiento> listaBPM=new ArrayList<>();
		List<RequerimientoMasterBPMmodel> listaMaster=new ArrayList<>();
		
		listaMaster=requerimientoRepository.obtenerRequerimientosXidCliente(idCliente,Constantes.REQUERIMIENTO_BPM, estado);
			listaMaster.forEach(x -> {
			if(StringUtils.isEmpty(x.getRequerimientoBPM())) return;			
			String procesoPrincipal=x.getRequerimientoBPM().getProcesoPrincipal();
			
			if(!StringUtils.isEmpty(x.getRequerimientoBPM().getDetallesRequerimientos())){
				x.getRequerimientoBPM().getDetallesRequerimientos().clear();
			}
			x.setRequerimientoBPM(null);			
			Requerimiento req = Util.objectToObject(Requerimiento.class,x);
			req.setProcesoPrincipal(procesoPrincipal);
			listaBPM.add(req);
		});
		
		wrapper.setRequerimientos(Util.listObjectToListObject(Requerimiento.class, listaBPM));
	    response.setObjetoRespuesta(wrapper);
		return Flux.just(response)
			.onErrorResume(e -> {
				logger.info(e);
				throw new RuntimeException();
			});
	}

	@Override
	public Mono<Response<Boolean>> validacionFechaCorte() throws Exception {
		Response<Boolean> response=new Response<>();
		return Mono.just(validaFechaCorteRequerimiento())
			.switchIfEmpty(Mono.error(new RuntimeException("error on save requerimiento...")))
			.flatMap(x -> {
				response.setObjetoRespuesta(x);
				return Mono.just(response);
			}).onErrorResume(e -> {
					logger.info(e);
					throw new RuntimeException("error on save requerimiento...");
			});
	}

	@Override
	public Flux<Response<BPMwrapper>> filtrarRequerimiento(@Valid Long idCliente, String estado) {
		logger.info("init obtener requerimientos...");
		Response<BPMwrapper> response = new Response<>();
		
		BPMwrapper wrapper = new BPMwrapper();
		List<RequerimientoMasterBPMmodel> lista = requerimientoRepository.filtroRequerimiento(idCliente, estado);
		lista.forEach(x -> x.setRequerimientoBPM(null));
		
		wrapper.setRequerimientos(Util.listObjectToListObject(Requerimiento.class, lista));
		response.setObjetoRespuesta(wrapper);
				
		return Flux.just(response)
		.onErrorResume(e -> {
			logger.info(e);
			throw new RuntimeException();
		});
	}

	@Override
	public Mono<Response<BPMwrapper>> obtenerWrapperBPM(@Valid Long idCliente) {
		logger.info("init obtener requerimientos...");
		Response<BPMwrapper> response = new Response<>();		
		BPMwrapper wrapper = new BPMwrapper();
		return Mono.just(requerimientoRepository.obtenerRequerimientosPorIdCliente(idCliente))
				.flatMap(y->{
					y.forEach(x -> {
						x.setRequerimientoBPM(null);
					});
					wrapper.setRequerimientos(Util.listObjectToListObject(Requerimiento.class, y));
					wrapper.getRequerimientos().forEach(z -> {
						z.setOrdenServicio("OS000015");
					});
										
					Optional<ClienteModel> optClienteModel=clienteRepository.findById(idCliente);
					if(optClienteModel.isPresent()) {	
						List<ClienteFinal> clientesFinales= new ArrayList<>();
						ClienteModel clienteModel=optClienteModel.get();		
						Set<ClienteFinalModel> clientesFinalesModel=clienteModel.getClientesFinales();
						
						clientesFinalesModel.forEach(x->{
							x.setCliente(null);
							ClienteFinal clienteFinal = Util.objectToObject(ClienteFinal.class,x);
							clientesFinales.add(clienteFinal);
						});
						wrapper.setClientesFinales(clientesFinales);
					}
					
					List<Producto> productos= new ArrayList<>();
					productoRepository.obtenerProductosXidCliente(idCliente.intValue())
						.forEach(x->{
							x.setProductoDatoBPA(null);
							x.setProductoDatoLogistico(null);
							Producto producto=Util.objectToObject(Producto.class,x);
							productos.add(producto);
						});
					wrapper.setProductos(productos);
					wrapper.setParametros(obtenerParametros("BPM").block());				
					response.setObjetoRespuesta(wrapper);				
					return Mono.just(response)
						.onErrorResume(e -> {
								logger.info(e);
								throw new RuntimeException();
						});
				}).onErrorResume(e -> {
					logger.info(e);
					throw new RuntimeException();
				});
	}
}
