package com.dsb.mantenimiento.service;

import java.util.List;

import javax.validation.Valid;

import com.dsb.core.common.http.Response;
import com.dsb.core.domain.producto.Producto;
import com.dsb.core.domain.wrapper.ProductoWrapper;

import reactor.core.publisher.Mono;

public interface MantenimientoService {

	Mono<Response<Boolean>> crearProducto(@Valid Producto producto);
	Mono<Response<List<Producto>>> obtenerProductosXidCliente(@Valid Integer idCliente);
	Mono<Response<String>> validarProductos(@Valid String idsProductos,Long idCliente);
	Mono<Response<List<Producto>>> obtenerProductosXCliente(@Valid Integer idCliente);
	Mono<Response<ProductoWrapper>> obtenerWrapperProducto(@Valid Integer idCliente);
}
