package com.dsb.mantenimiento.service;

import java.util.List;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.dsb.core.common.base.BaseService;
import com.dsb.core.common.http.Response;
import com.dsb.core.domain.producto.Producto;
import com.dsb.core.domain.wrapper.ProductoWrapper;
import com.dsb.core.util.Enumeradores;
import com.dsb.core.util.Util;
import com.dsb.persistence.model.producto.ProductoDatoBPAmodel;
import com.dsb.persistence.model.producto.ProductoDatoLogisticoModel;
import com.dsb.persistence.model.producto.ProductoModel;
import com.dsb.persistence.repository.ProductoRepository;

import reactor.core.publisher.Mono;

@Service
public class MantenimientoServiceImpl extends BaseService implements MantenimientoService{
	private static final Logger logger = LogManager.getLogger(MantenimientoServiceImpl.class);

	@Autowired
	ProductoRepository productoRepository;
	
	@Override
	public Mono<Response<Boolean>> crearProducto(@Valid Producto producto) {
		logger.info("init create producto...");
		Response<Boolean> res = new Response<>();
		
		return Mono.<Response<Boolean>>defer(() -> transactionTemplate.execute(status -> {
			producto.setCodigo(obtenerCodigoTabla("PRO"));
			ProductoModel productoModel = Util.objectToObject(ProductoModel.class,producto);
			ProductoDatoBPAmodel productoDatoBPAmodel = productoModel.getProductoDatoBPA();
			productoDatoBPAmodel.setProducto(productoModel);
			ProductoDatoLogisticoModel productoDatoLogisticoModel = productoModel.getProductoDatoLogistico();
			productoDatoLogisticoModel.setProducto(productoModel);		
			productoModel.setProductoDatoBPA(productoDatoBPAmodel);
			productoModel.setProductoDatoLogistico(productoDatoLogisticoModel);
			
			return Mono.just(productoRepository.save(productoModel))
				.switchIfEmpty(Mono.error(new RuntimeException("error on save producto...")))
				.flatMap(x -> {
					res.setObjetoRespuesta(true);
					return Mono.just(res);
				}).onErrorResume(e -> {
					logger.info(e);
					throw new RuntimeException("error on save requerimiento...");
				});
		})).subscribeOn(jdbcScheduler);
	}

	@Override
	public Mono<Response<List<Producto>>> obtenerProductosXCliente(@Valid Integer idCliente) {
		logger.info("init obtenerRequerimientos...");

		Response<List<Producto>> response = new Response<>();		
		return Mono.just(productoRepository.obtenerProductosXidCliente(idCliente))
			.flatMap(x -> {
				x.forEach(y -> {
					if(!StringUtils.isEmpty(y.getProductoDatoBPA())) {
						y.getProductoDatoBPA().setProducto(null);
					}
					if(!StringUtils.isEmpty(y.getProductoDatoLogistico())) {
						y.getProductoDatoLogistico().setProducto(null);
					}
				});		
				response.setObjetoRespuesta(Util.listObjectToListObject(Producto.class, x));
				return Mono.just(response);
			}).onErrorResume(e -> {
				logger.info(e);
			throw new RuntimeException("error on validate products...");
		});
	}

	@Override
	public Mono<Response<ProductoWrapper>> obtenerWrapperProducto(@Valid Integer idCliente) {
		logger.info("init obtenerWrapperProducto...");
		Response<ProductoWrapper> response = new Response<>();	
		ProductoWrapper wrapper=new ProductoWrapper();
		return Mono.just(productoRepository.obtenerProductosXidCliente(idCliente))
			.flatMap(x -> {
				x.forEach(y -> {
					if(!StringUtils.isEmpty(y.getProductoDatoBPA())) {
						y.getProductoDatoBPA().setProducto(null);
					}
					if(!StringUtils.isEmpty(y.getProductoDatoLogistico())) {
						y.getProductoDatoLogistico().setProducto(null);
					}
				});		
				wrapper.setProductos(Util.listObjectToListObject(Producto.class, x));
				wrapper.setParametros(obtenerParametros("PRO").block());
				response.setObjetoRespuesta(wrapper);
				return Mono.just(response);
			}).onErrorResume(e -> {
				logger.info(e);
			throw new RuntimeException("error on get wrapper product...");
		});
	}
	
	@Override
	public Mono<Response<List<Producto>>> obtenerProductosXidCliente(@Valid Integer idCliente) {
		logger.info("init obtenerRequerimientos...");
		Response<List<Producto>> response = new Response<>();

		List<ProductoModel> lista = productoRepository.obtenerProductosXidCliente(idCliente);
		lista.forEach(x -> {
			x.setProductoDatoBPA(null);
			x.setProductoDatoLogistico(null);
		});
		
		response.setObjetoRespuesta(Util.listObjectToListObject(Producto.class, lista));
		return Mono.just(response)
		.onErrorResume(e -> {
			logger.info(e);
			throw new RuntimeException();
		});
	}
	
	@Override
	public Mono<Response<String>> validarProductos(@Valid String idsProductos,Long idCliente) {
		Response<String> response = new Response<>();
		return Mono.just(productoRepository.validarProductosXid(idsProductos,idCliente))
			.flatMap(x -> {
				if(StringUtils.isEmpty(x)) {
					response.setObjetoRespuesta("");
				}else {
					response.setObjetoRespuesta(x);
					response.setStatusText(Enumeradores.StatusText.ERROR.name());
				}			
				return Mono.just(response);
			}).onErrorResume(e -> {
				logger.info(e);
				throw new RuntimeException("error on validate products...");
			});
	}
}
