package com.dsb.mantenimiento.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dsb.core.common.http.Response;
import com.dsb.core.domain.producto.Producto;
import com.dsb.core.domain.wrapper.ProductoWrapper;
import com.dsb.mantenimiento.service.MantenimientoService;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/web/mantenimiento")
public class MantenimientoController {
	@Autowired
	MantenimientoService mantenimientoService;
	
	@GetMapping(value = "/")
	public String ping(){
		return "mantenimiento web is running..";
	}
	
	@PostMapping(value = "/obtener/productos/{idCliente}",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Mono<Response<List<Producto>>> obtenerProductosXidCliente(@Valid @PathVariable Integer idCliente){
		return mantenimientoService.obtenerProductosXidCliente(idCliente);
	}
	
	@PostMapping(value = "/obtener/productos/cliente/{idCliente}",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Mono<Response<List<Producto>>> obtenerProductosXCliente(@Valid @PathVariable Integer idCliente){
		return mantenimientoService.obtenerProductosXCliente(idCliente);
	}
	
	@PostMapping(value = "/obtener/maestro/producto/{idCliente}",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Mono<Response<ProductoWrapper>> obtenerWrapperProducto(@Valid @PathVariable Integer idCliente){
		return mantenimientoService.obtenerWrapperProducto(idCliente);
	}
	
	@PostMapping(value = "/crear/producto",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Mono<Response<Boolean>> crearProducto(@Valid @RequestBody Producto producto){
		return mantenimientoService.crearProducto(producto);
	}
	
	@PostMapping(value = "/validar/productos/{idsProductos}/{idCliente}",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Mono<Response<String>> validarProductos(@Valid @PathVariable String idsProductos,@Valid @PathVariable Long idCliente){
		return mantenimientoService.validarProductos(idsProductos,idCliente);
	}
}
