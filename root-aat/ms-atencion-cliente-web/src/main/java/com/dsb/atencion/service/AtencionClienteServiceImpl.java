package com.dsb.atencion.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Service;

import com.dsb.core.common.http.Response;
import com.dsb.core.domain.cliente.Incidencia;
import com.dsb.core.domain.cliente.Informativo;
import com.dsb.core.domain.cliente.Ingreso;
import com.dsb.core.domain.cliente.Reclamo;

import reactor.core.publisher.Mono;

@Service
public class AtencionClienteServiceImpl implements AtencionClienteService{

	@Override
	public Mono<Response<Ingreso>> getServiciosContratados(@Valid String idUsuario) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Mono<Response<List<Reclamo>>> getReclamos(@Valid String idUsuario) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Mono<Response<List<Incidencia>>> getIncidencias(@Valid String idUsuario) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Mono<Response<Informativo>> getInformativo(@Valid String idUsuario) {
		// TODO Auto-generated method stub
		return null;
	}

}
