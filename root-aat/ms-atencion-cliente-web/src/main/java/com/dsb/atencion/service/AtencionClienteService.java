package com.dsb.atencion.service;

import java.util.List;

import javax.validation.Valid;

import com.dsb.core.common.http.Response;
import com.dsb.core.domain.cliente.Incidencia;
import com.dsb.core.domain.cliente.Informativo;
import com.dsb.core.domain.cliente.Ingreso;
import com.dsb.core.domain.cliente.Reclamo;

import reactor.core.publisher.Mono;

public interface AtencionClienteService {

	Mono<Response<Ingreso>> getServiciosContratados(@Valid String idUsuario);

	Mono<Response<List<Reclamo>>> getReclamos(@Valid String idUsuario);

	Mono<Response<List<Incidencia>>> getIncidencias(@Valid String idUsuario);

	Mono<Response<Informativo>> getInformativo(@Valid String idUsuario);

}
