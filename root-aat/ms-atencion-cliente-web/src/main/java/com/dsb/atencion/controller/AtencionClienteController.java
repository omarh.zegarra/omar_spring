package com.dsb.atencion.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dsb.atencion.service.AtencionClienteService;
import com.dsb.core.common.http.Response;
import com.dsb.core.domain.cliente.Incidencia;
import com.dsb.core.domain.cliente.Informativo;
import com.dsb.core.domain.cliente.Ingreso;
import com.dsb.core.domain.cliente.Reclamo;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/web/cliente")
public class AtencionClienteController {
	@Autowired
	AtencionClienteService atencionClienteService;
	
	@GetMapping(value = "/")
	public String ping(){
		return "/web/atencion/cliente is running..";
	}
	
	@GetMapping(value = "/servicios/contratados/{idUsuario}")
	public Mono<Response<Ingreso>> getServiciosContratados(@Valid @PathVariable String idUsuario){
		return atencionClienteService.getServiciosContratados(idUsuario);
	}
	
	@GetMapping(value = "/reclamos/{idUsuario}")
	public Mono<Response<List<Reclamo>>> getReclamos(@Valid @PathVariable String idUsuario){
		return atencionClienteService.getReclamos(idUsuario);
	}
	
	@GetMapping(value = "/transferencias/{idUsuario}")
	public Mono<Response<List<Incidencia>>> getIncidencias(@Valid @PathVariable String idUsuario){
		return atencionClienteService.getIncidencias(idUsuario);
	}
	
	@GetMapping(value = "/actas/{idUsuario}")
	public Mono<Response<Informativo>> getInformativo(@Valid @PathVariable String idUsuario){
		return atencionClienteService.getInformativo(idUsuario);
	}
}
