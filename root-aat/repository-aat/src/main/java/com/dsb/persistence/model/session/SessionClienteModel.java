package com.dsb.persistence.model.session;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@Entity(name = "session_cliente")
@Table(name = "cliente")
@EqualsAndHashCode(exclude= {"sessionClientesFinales","sessionProductos","sessionRotulos","sessionCoordinador"})
public class SessionClienteModel implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_cliente",nullable = false)
	Long idCliente;
	
	@Column(name = "id_coordinador",nullable = false)
	Integer idCoordinador;
	
	@Column(name = "id_archivo_bpm",nullable = false)
	Integer idArchivoBpm;
	
	@Column(name = "ruc",nullable = false)
	String ruc;
	
	@Column(name = "razon_social",nullable = false)
	String razonSocial;
	
	@Column(name = "activo",nullable = false)
	Boolean activo;
	
	@Column(name = "acronimo",nullable = false)
	String acronimo;
	
	@Column(name = "telefono",nullable = false)
	String telefono;
	
	@Column(name = "distrito",nullable = false)
	String distrito;	
	
	@Column(name = "departamento",nullable = false)
	String departamento;
	
	@Column(name = "provincia",nullable = false)
	String provincia;
	
	@Column(name = "direccion",nullable = false)
	String direccion;
	
	@Column(name = "correo",nullable = false)
	String correo;	

	@OneToMany(fetch = FetchType.LAZY,cascade = { CascadeType.MERGE,CascadeType.PERSIST }, mappedBy="sessionCliente",orphanRemoval = true)
	private Set<SessionClienteFinalModel> sessionClientesFinales= new HashSet<>(0);
	
	@OneToMany(fetch = FetchType.LAZY,cascade = { CascadeType.MERGE,CascadeType.PERSIST }, mappedBy="sessionCliente",orphanRemoval = true)
	private Set<SessionProductoModel> sessionProductos;
	
	@OneToMany(fetch = FetchType.LAZY,cascade = { CascadeType.MERGE,CascadeType.PERSIST }, mappedBy="sessionCliente",orphanRemoval = true)
	private Set<SessionRotuloModel> sessionRotulos;
	
	@OneToOne(fetch = FetchType.LAZY,cascade = { CascadeType.MERGE,CascadeType.PERSIST }, mappedBy="sessionCliente",orphanRemoval = true)
	SessionCoordinadorModel sessionCoordinador;
}
