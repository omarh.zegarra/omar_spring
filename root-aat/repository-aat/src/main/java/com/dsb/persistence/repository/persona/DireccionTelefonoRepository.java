package com.dsb.persistence.repository.persona;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dsb.persistence.model.persona.DireccionTelefonoModel;

@Repository
public interface DireccionTelefonoRepository extends CrudRepository<DireccionTelefonoModel, Long>{

}