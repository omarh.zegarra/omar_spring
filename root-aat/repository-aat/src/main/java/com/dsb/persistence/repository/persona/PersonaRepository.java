package com.dsb.persistence.repository.persona;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dsb.persistence.model.persona.PersonaModel;

@Repository
public interface PersonaRepository extends CrudRepository<PersonaModel, Long> {

}