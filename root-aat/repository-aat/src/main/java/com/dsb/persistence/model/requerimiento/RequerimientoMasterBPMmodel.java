package com.dsb.persistence.model.requerimiento;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity(name = "requerimiento_master_bpm")
@Table(name = "requerimiento")
public class RequerimientoMasterBPMmodel implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_requerimiento",nullable = false)
	private Long idRequerimiento;
	
	@Column(name = "codigo",nullable = false)
	private String codigo;
	
	@Column(name = "id_cliente",nullable = false)
	Long idCliente;
	
	@Column(name = "cliente",nullable = false)
	String cliente;
	
	@Column(name = "fecha_requerimiento",nullable = false)
	String fechaRequerimiento;
	
	@Column(name = "tipo_requerimiento",nullable = false)
	Integer tipoRequerimiento;
	
	@Column(name = "activo",nullable = false)
	Boolean activo;
	
	@Column(name = "id_estado",nullable = false)
	Integer idEstado;
	
	@Column(name = "estado",nullable = false)
	String estado;
	
	@Column(name = "id_actividad_logistica",nullable = false)
	Integer idActividadLogistica;
	
	@Column(name = "actividad_logistica",nullable = false)
	String actividadLogistica;
	
	@Column(name = "creado_por",nullable = false)
	String creadoPor;
	
	@Column(name = "total_unidades",nullable = false)
	Integer totalUnidades;
	
	@Column(name = "volumen",nullable = false)
	Double volumen;
	
	@OneToOne(fetch = FetchType.LAZY,cascade = { CascadeType.MERGE,CascadeType.PERSIST }, mappedBy="requerimiento",orphanRemoval = true)
	RequerimientoBPMmodel requerimientoBPM;
}

