package com.dsb.persistence.model.persona;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;

@Data
@Embeddable
public class TransportadorModelPK implements Serializable {

	private static final long serialVersionUID = 6208682740222160073L;
	@Column(name = "id_empresa ", nullable = false)
	private Long idEmpresa;
	@Column(name = "id_transporte ", nullable = false)
	private Long idTransporte;

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof TransportadorModelPK))
			return false;
		TransportadorModelPK that = (TransportadorModelPK) o;
		return Objects.equals(getIdEmpresa(), that.getIdEmpresa())
				&& Objects.equals(getIdTransporte(), that.getIdTransporte());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getIdEmpresa(), getIdTransporte());
	}

}