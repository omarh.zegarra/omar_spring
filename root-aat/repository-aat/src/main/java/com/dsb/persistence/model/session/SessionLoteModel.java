package com.dsb.persistence.model.session;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity(name = "session_lote")
@Table(name = "lote")
public class SessionLoteModel implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_lote",nullable = false)
	Long idLote;
	
	@Column(name = "descripcion",nullable = false)
	String descripcion;
	
	@Column(name = "envoltura",nullable = false)
	String envoltura;
	
	@Column(name = "volumen",nullable = false)
	Double volumen;
	
	@Column(name = "total",nullable = false)
	Integer total;
	
	@Column(name = "cantidad_disponible",nullable = false)
	Integer cantidadDisponible;
	
	@Column(name = "cantidad_consumida",nullable = false)
	Integer cantidadConsumida;	
	
	@ManyToOne(cascade= CascadeType.PERSIST)
    @JoinColumn(name = "id_producto", referencedColumnName = "id_producto", nullable = false)
	SessionProductoModel sessionProducto;	
}
