package com.dsb.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity(name = "usuario_login")
@Table(name = "usuario_login")
@Data
public class UsuarioLoginModel{

	@Id
	@GeneratedValue( strategy= GenerationType.IDENTITY) 
	@Column(name="id_usuario_login")	
	private Long id;
	
	@Column(name = "nombres",nullable = false)
	private String nombres;
	
	@Column(name = "correo",nullable = false)
	private String correo;	
	
	@Column(name = "clave",nullable = false)
	private String clave;	
	
	@Column(name = "clave_generada",nullable = false)
	private String claveGenerada;	
}
