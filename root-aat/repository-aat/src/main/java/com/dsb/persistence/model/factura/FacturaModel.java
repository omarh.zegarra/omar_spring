/*package com.dsb.persistence.model.factura;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.dsb.persistence.model.requerimiento.RequerimientoModel;

import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@Entity(name = "factura")
@Table(name = "factura")
public class FacturaModel implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_factura",nullable = false)
	private Long idFactura;
	
	@Column(name = "id_cliente",nullable = false)
	private Long idCliente;
	
	@Column(name = "id_tipo_comprobante",nullable = false)
	private Long idTipoComprobante;
	
	@Column(name = "num_comprobante",nullable = false)
	private Long numComprobante;
	
	@Column(name = "importe",nullable = false)
	private Long importe;
	
	@Column(name = "fecha_registro",nullable = false)
	private Date fechaRegistro;
	
	@Column(name = "fecha_emision",nullable = false)
	private Date fechaEmision;
	
	@Column(name = "fecha_vencimiento",nullable = false)
	private Date fechaVencimiento;
	
	@Column(name = "id_estado_factura",nullable = false)
	private Long idEstadoFactura;
	
	@Column(name = "id_detalle_categoria",nullable = false)
	private Long idDetalleCategoria;
	
	@Column(name = "id_categoria",nullable = false)
	private Long idCategoria;
	
	@OneToOne(fetch = FetchType.LAZY,cascade = { CascadeType.MERGE,CascadeType.PERSIST }, mappedBy="factura",orphanRemoval = true)
	EstadoFacturaModel EstadoFactura;
	
}
*/