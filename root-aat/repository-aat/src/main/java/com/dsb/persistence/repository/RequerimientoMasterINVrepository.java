package com.dsb.persistence.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dsb.persistence.model.requerimiento.RequerimientoMasterINVmodel;

@Repository
public interface RequerimientoMasterINVrepository extends CrudRepository<RequerimientoMasterINVmodel, Long>{	
	@Query("select r from requerimiento_master_inv r where r.idRequerimiento = :idRequerimiento")
    List<RequerimientoMasterINVmodel> obtenerRequerimientoXid(@Param("idRequerimiento") Long idRequerimiento);

	@Query("select r from requerimiento_master_inv r where r.idCliente = :idCliente and r.idActividadLogistica= :idActividadLogistica")
    List<RequerimientoMasterINVmodel> obtenerRequerimientoXidCliente(@Param("idCliente") Long idCliente,
    		@Param("idActividadLogistica") Integer idActividadLogistica);
	
	@Query("select r from requerimiento_master_inv r where r.idCliente = :idCliente and	r.idActividadLogistica= 6 and r.estado LIKE CONCAT('%',:estado,'%')")
    List<RequerimientoMasterINVmodel> filtroRequerimiento(@Param("idCliente") Long idCliente, @Param("estado") String estado);
	
	@Query("select r from requerimiento_master_inv r where r.idActividadLogistica= 6 and r.idCliente = :idCliente")
    List<RequerimientoMasterINVmodel> obtenerRequerimientoPorIdCliente(@Param("idCliente") Long idCliente);
}
