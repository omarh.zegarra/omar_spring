package com.dsb.persistence.model.persona;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Data;

@Data
@Entity(name = "laboratorio")
@Table(name = "laboratorio")
@DynamicUpdate
public class LaboratorioModel {

	@EmbeddedId
	private LaboratorioModelPK id;

}
