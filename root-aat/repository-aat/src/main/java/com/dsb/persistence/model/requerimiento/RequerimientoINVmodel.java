package com.dsb.persistence.model.requerimiento;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity(name = "requerimiento_inv")
@Table(name = "requerimiento_inv")
@EqualsAndHashCode(exclude={"requerimiento","detallesRequerimientos"})
public class RequerimientoINVmodel implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_requerimiento_inv",nullable = false)
	Long idRequerimientoINV;
	
	@Column(name = "id_tipo_inventario",nullable = false)
	Integer idTipoInventario;
	
	@Column(name = "es_ciego",nullable = false)
	Boolean esCiego;
	
	@Column(name = "detalle_inventario",nullable = false)
	String detalleInventario;
	
	@OneToOne(cascade= CascadeType.PERSIST)
    @JoinColumn(name = "id_requerimiento", referencedColumnName = "id_requerimiento", nullable = false)
	RequerimientoMasterINVmodel requerimiento;
	
	@OneToMany(fetch = FetchType.LAZY,cascade = { CascadeType.MERGE,CascadeType.PERSIST }, mappedBy="requerimientoINV",orphanRemoval = true)
	Set<ProductoXrequerimientoINVmodel> detallesRequerimientos;	
}
