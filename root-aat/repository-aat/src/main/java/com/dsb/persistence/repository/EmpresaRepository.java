package com.dsb.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dsb.persistence.model.empresa.EmpresaModel;

@Repository
public interface EmpresaRepository extends JpaRepository<EmpresaModel, Long>{
//public interface EmpresaRepository extends CrudRepository<EmpresaModel, Long>{
	//@Query("select e from empresa u where u.correo = :correo and u.clave=:clave")
	//EmpresaModel findCompanyByUser(@Param("correo") String correo,@Param("clave") String clave);
}