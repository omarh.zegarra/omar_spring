package com.dsb.persistence.model.persona;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;

@Data
@Embeddable
public class LaboratorioModelPK implements Serializable {

	private static final long serialVersionUID = 2121261815255033261L;
	@Column(name = "id_empresa ", nullable = false)
	private Long idEmpresa;
	@Column(name = "id_laboratorio ", nullable = false)
	private Long idLaboratorio;

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof LaboratorioModelPK))
			return false;
		LaboratorioModelPK that = (LaboratorioModelPK) o;
		return Objects.equals(getIdEmpresa(), that.getIdEmpresa())
				&& Objects.equals(getIdLaboratorio(), that.getIdLaboratorio());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getIdEmpresa(), getIdLaboratorio());
	}

}