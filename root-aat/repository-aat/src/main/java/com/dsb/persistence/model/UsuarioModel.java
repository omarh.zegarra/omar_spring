package com.dsb.persistence.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity(name = "usuario")
@Table(name = "usuario")
public class UsuarioModel{

	@Id
	@GeneratedValue( strategy= GenerationType.IDENTITY) 
	@Column(name="id_usuario")	
	private Long id;
	
	@Column(name = "id_rol",nullable = false)
	private String codigoRol;
	
	@Column(name = "nombres",nullable = false)
	private String nombres;
	
	@Column(name = "apellido_paterno")
	private String apellidoPaterno;
	
	@Column(name = "apellido_materno")
	private String apellidoMaterno;
	
	@Column(name = "correo",nullable = false)
	private String correo;	
	
	@Column(name = "clave",nullable = false)
	private String clave;	
	
	@Column(name = "estado")
	private Boolean estado;
	
	@Column(name = "numero_celular")
	private String numeroCelular;
	
	@Column(name = "clave_celular")
	private String claveCelular;	

	@JsonIgnore
    @OneToOne(fetch = FetchType.LAZY,cascade =  CascadeType.ALL,mappedBy = "usuario")
	private TokenModel token;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCodigoRol() {
		return codigoRol;
	}
	public void setCodigoRol(String codigoRol) {
		this.codigoRol = codigoRol;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public Boolean getEstado() {
		return estado;
	}
	public void setEstado(Boolean estado) {
		this.estado = estado;
	}
	public String getNumeroCelular() {
		return numeroCelular;
	}
	public void setNumeroCelular(String numeroCelular) {
		this.numeroCelular = numeroCelular;
	}
	public String getClaveCelular() {
		return claveCelular;
	}
	public void setClaveCelular(String claveCelular) {
		this.claveCelular = claveCelular;
	}
	public TokenModel getToken() {
		return token;
	}
	public void setToken(TokenModel token) {
		this.token = token;
	}
}
