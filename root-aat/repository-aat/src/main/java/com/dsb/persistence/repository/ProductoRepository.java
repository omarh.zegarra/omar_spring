package com.dsb.persistence.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dsb.persistence.model.producto.ProductoModel;

@Repository
public interface ProductoRepository extends CrudRepository<ProductoModel, Long>{	
	@Query("select p from producto p where p.idCliente = :idCliente")
    List<ProductoModel> obtenerProductosXidCliente(@Param("idCliente") Integer idCliente);
	
	@Procedure(value = "fn_valida_productos")
    String validarProductosXid(@Param("idsProductos") String idsProductos,@Param("idCliente") Long idCliente);
}
