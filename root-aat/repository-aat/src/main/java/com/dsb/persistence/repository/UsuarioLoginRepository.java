package com.dsb.persistence.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dsb.persistence.model.UsuarioLoginModel;

@Repository
public interface UsuarioLoginRepository extends CrudRepository<UsuarioLoginModel, Long>{
	@Query("select u from usuario_login u where u.claveGenerada =:clave")
    UsuarioLoginModel findUserByGenerateCode(@Param("clave") String clave);
}
