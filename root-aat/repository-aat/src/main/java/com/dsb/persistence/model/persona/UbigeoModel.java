package com.dsb.persistence.model.persona;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity(name = "ubigeo")
@Table(name = "ubigeo")
public class UbigeoModel {

	@Id
	@Column(name = "id_ubigeo", nullable = false)
	private String idUbigeo;
	@Column(name = "id_dpto")
	private String idDepartamento;
	@Column(name = "nombre_dpto")
	private String nombreDepartamento;
	@Column(name = "id_prov")
	private String idProvincia;
	@Column(name = "nombre_prov")
	private String nombreProvincia;
	@Column(name = "id_dist")
	private String idDistrito;
	@Column(name = "nombre_dist")
	private String nombreDistrito;
	@Column(name = "id_postal")
	private String idPostal;

}
