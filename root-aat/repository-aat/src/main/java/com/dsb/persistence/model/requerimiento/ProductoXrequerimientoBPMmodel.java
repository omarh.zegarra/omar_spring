package com.dsb.persistence.model.requerimiento;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity(name = "producto_x_requerimiento_bpm")
@Table(name = "producto_x_requerimiento_bpm")
public class ProductoXrequerimientoBPMmodel implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_producto_x_requerimiento",nullable = false)
	Long idDetalleRequerimientoBPM;
	
	@Column(name = "id_producto",nullable = false)
	Long idProducto;

	@Column(name = "unidades_trabajar",nullable = false)
	Integer unidadesTrabajar;
	
	@Column(name = "unidades_ingresar",nullable = false)
	Integer unidadesIngresar;
	
	@Column(name = "detalle_proceso",nullable = false)
	Integer detalleProceso;
	
	@Column(name = "fecha_vencimiento",nullable = false)
	String fechaVencimiento;
	
	@ManyToOne(cascade= CascadeType.PERSIST)
    @JoinColumn(name = "id_requerimiento_bpm", referencedColumnName = "id_requerimiento_bpm", nullable = false)
	RequerimientoBPMmodel requerimientoBPM;   
}
