package com.dsb.persistence.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.dsb.persistence.model.UsuarioModel;

@Repository
public interface UsuarioRepository extends CrudRepository<UsuarioModel, Long>{
	@Query("select u from usuario u where u.correo = :correo and u.clave=:clave")
    UsuarioModel findUserByEmailAndPassword(@Param("correo") String correo,@Param("clave") String clave);
	
	@Query("select u from usuario u where u.correo = :correo")
    UsuarioModel findUserByEmail(@Param("correo") String correo);
	
	@Query("select u from usuario u where u.nombres = :nombres or u.correo=:correo or u.apellidoPaterno=:paterno")
    List<UsuarioModel> find(@Param("nombres") String nombres,@Param("correo") String correo,@Param("paterno") String paterno);
	
	@Query("select u from usuario u inner join token t on u.token=t.usuario and t.accessToken=:accessToken")
	UsuarioModel findUserByAccessToken(@Param("accessToken") String accessToken);
	
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("update usuario u set u.estado = :estado where u.correo = :correo")
    Integer changeUserStatusByEmail(@Param("correo") String correo,@Param("estado") boolean estado);
	
	@Query("select u from usuario u where u.correo = :correo and u.clave=:clave")
	UsuarioModel findUserByEmailAndPasswordx(@Param("correo") String correo,@Param("clave") String clave);
}
