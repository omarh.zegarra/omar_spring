package com.dsb.persistence.model.persona;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicUpdate;

import com.dsb.persistence.model.AuditoriaModel;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@Entity(name = "direccion")
@Table(name = "direccion")
@DynamicUpdate
public class DireccionModel extends AuditoriaModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_direccion", nullable = false)
	private Long idDireccion;
	@Column(name = "id_empresa ", nullable = false)
	private Long idEmpresa;
	@Column(name = "tipo_direccion")
	private String tipoDireccion;
	@Column(name = "descripcion")
	private String descripcion;
	@Column(name = "id_ubigeo")
	private String idUbigeo;
	@Column(name = "tipo_via")
	private String tipoVia;
	@Column(name = "zona")
	private String zona;
	@Column(name = "tipo_zona")
	private String tipoZona;
	@Column(name = "nro_direccion")
	private String numeroDireccion;
	@Column(name = "nro_kilometro")
	private String numeroKilometro;
	@Column(name = "nro_interior")
	private String numeroInterior;
	@Column(name = "nro_lote")
	private String numeroLote;
	@Column(name = "nro_dpto")
	private String numeroDepartamento;
	@Column(name = "nro_manzana")
	private String numeroManzana;
	@Column(name = "id_estado", nullable = false)
	private String idEstado;
	@Transient
	private List<String> propertiesUpdate;
	@ManyToMany
	@JoinTable(
	  name = "direccion_telefono", 
	  joinColumns = @JoinColumn(name = "id_direccion"), 
	  inverseJoinColumns = @JoinColumn(name = "id_telefono"))
	private List<TelefonoModel> listaTelefonoDireccion;

}
