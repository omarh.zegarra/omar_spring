package com.dsb.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "rol")
@Table(name = "rol")
public class RolModel {
	@Id
	@GeneratedValue( strategy= GenerationType.IDENTITY) 
	@Column(name="id_rol")	
	private Long id;
	
	@Column(name = "descripcion",nullable = false)
	private String descripcion;
	
	@Column(name = "default_rol")
	private String defaultRol;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDefaultRol() {
		return defaultRol;
	}

	public void setDefaultRol(String defaultRol) {
		this.defaultRol = defaultRol;
	}
}
