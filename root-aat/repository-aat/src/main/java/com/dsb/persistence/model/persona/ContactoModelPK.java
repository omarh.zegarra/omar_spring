package com.dsb.persistence.model.persona;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;

@Data
@Embeddable
public class ContactoModelPK implements Serializable {

	private static final long serialVersionUID = -4018785760783879057L;
	@Column(name = "id_empresa ", nullable = false)
	private Long idEmpresa;
	@Column(name = "id_contacto ", nullable = false)
	private Long idContacto;

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof ContactoModelPK))
			return false;
		ContactoModelPK that = (ContactoModelPK) o;
		return Objects.equals(getIdEmpresa(), that.getIdEmpresa())
				&& Objects.equals(getIdContacto(), that.getIdContacto());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getIdEmpresa(), getIdContacto());
	}

}