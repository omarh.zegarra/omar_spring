package com.dsb.persistence.repository.persona;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dsb.persistence.model.persona.CorreoModel;
import com.dsb.persistence.model.persona.CorreoModelPK;

@Repository
public interface CorreoRepository extends CrudRepository<CorreoModel, CorreoModelPK> {

	List<CorreoModel> findByIdIdPersona(Long idPersona);
	
}