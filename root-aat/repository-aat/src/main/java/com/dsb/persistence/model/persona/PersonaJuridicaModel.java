package com.dsb.persistence.model.persona;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Data;

@Data
@Entity(name = "personajuridica")
@Table(name = "persona_juridica")
@DynamicUpdate
public class PersonaJuridicaModel {

	@Id
	@Column(name = "id_persona_juridica", nullable = false)
	private Long idPersonaJuridica;
	@Column(name = "id_empresa ", nullable = false)
	private Long idEmpresa;
	@Column(name = "ruc")
	private String ruc;
	@Column(name = "razon_social")
	private String razonSocial;
	@Column(name = "nombre_comercial")
	private String nombreComercial;
	@Column(name = "sitio_website")
	private String sitioWebsite;

	@Transient
	private List<String> propertiesUpdate;

}
