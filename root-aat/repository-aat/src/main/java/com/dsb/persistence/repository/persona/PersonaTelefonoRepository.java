package com.dsb.persistence.repository.persona;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dsb.persistence.model.persona.PersonaTelefonoModel;

@Repository
public interface PersonaTelefonoRepository extends CrudRepository<PersonaTelefonoModel, Long> {
	
}