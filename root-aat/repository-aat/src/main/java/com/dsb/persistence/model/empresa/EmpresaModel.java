package com.dsb.persistence.model.empresa;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity(name = "empresa")
@Table(name = "empresa")
@EqualsAndHashCode(exclude={"configuracionEmpresa", "sucursals","certificados"})
public class EmpresaModel implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_empresa",nullable = false)
	Long idEmpresa;	
	
	@Column(name = "ruc",nullable = false)
    String ruc;
	
	@Column(name = "razon_social",nullable = false)
    String razonSocial;
	
	@Column(name = "usuario_sunat",nullable = false)
    String usuarioSunat;
	
	@Column(name = "id_estado",nullable = false)
    String idEstado;

    @OneToMany(fetch = FetchType.LAZY,cascade = { CascadeType.MERGE,CascadeType.PERSIST }, mappedBy="empresa",orphanRemoval = true)
	private Set<ConfiguracionModel> configuracionEmpresa = new HashSet<>(0);
		
	@OneToMany(fetch = FetchType.LAZY,cascade = { CascadeType.MERGE,CascadeType.PERSIST }, mappedBy="empresa",orphanRemoval = true)
	private Set<SucursalModel> sucursals= new HashSet<>(0);  
	
	@OneToMany(fetch = FetchType.LAZY,cascade = { CascadeType.MERGE,CascadeType.PERSIST }, mappedBy="empresa",orphanRemoval = true)
	private Set<CertificadoModel> certificados= new HashSet<>(0); 
}