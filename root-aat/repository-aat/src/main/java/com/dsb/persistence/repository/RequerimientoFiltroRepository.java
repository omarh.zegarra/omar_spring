package com.dsb.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.dsb.persistence.model.requerimiento.RequerimientoMasterBPAmodel;

public interface RequerimientoFiltroRepository extends JpaRepository<RequerimientoMasterBPAmodel, Long>, JpaSpecificationExecutor<RequerimientoMasterBPAmodel>{

}
