package com.dsb.persistence.model.persona;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;

@Data
@Embeddable
public class AnexoModelPK implements Serializable {

	private static final long serialVersionUID = -2483459890372592877L;
	@Column(name = "id_anexo", nullable = false)
	private Long idAnexo;
	@Column(name = "id_telefono ", nullable = false)
	private Long idTelefono;
	@Column(name = "id_empresa ", nullable = false)
	private Long idEmpresa;

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof AnexoModelPK))
			return false;
		AnexoModelPK that = (AnexoModelPK) o;
		return Objects.equals(getIdAnexo(), that.getIdAnexo()) && Objects.equals(getIdTelefono(), that.getIdTelefono())
				&& Objects.equals(getIdEmpresa(), that.getIdEmpresa());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getIdAnexo(), getIdTelefono(), getIdEmpresa());
	}

}