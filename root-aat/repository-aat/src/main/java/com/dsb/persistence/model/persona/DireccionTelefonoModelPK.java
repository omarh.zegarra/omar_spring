package com.dsb.persistence.model.persona;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;

@Data
@Embeddable
public class DireccionTelefonoModelPK implements Serializable {

	private static final long serialVersionUID = 997083097297958102L;
	@Column(name = "id_telefono", nullable = false)
	private Long idTelefono;
	@Column(name = "id_direccion", nullable = false)
	private Long idDireccion;
	@Column(name = "id_empresa ", nullable = false)
	private Long idEmpresa;

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof DireccionTelefonoModelPK))
			return false;
		DireccionTelefonoModelPK that = (DireccionTelefonoModelPK) o;
		return Objects.equals(getIdTelefono(), that.getIdTelefono())
				&& Objects.equals(getIdDireccion(), that.getIdDireccion())
				&& Objects.equals(getIdEmpresa(), that.getIdEmpresa());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getIdTelefono(), getIdDireccion(), getIdEmpresa());
	}

}