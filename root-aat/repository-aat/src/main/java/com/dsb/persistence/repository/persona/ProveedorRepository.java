package com.dsb.persistence.repository.persona;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dsb.persistence.model.persona.ProveedorModel;
import com.dsb.persistence.model.persona.ProveedorModelPK;

@Repository
public interface ProveedorRepository extends CrudRepository<ProveedorModel, ProveedorModelPK>{

}