package com.dsb.persistence.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity(name = "cliente_final")
@Table(name = "cliente_final")
public class ClienteFinalModel implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_cliente_final",nullable = false)
	Long idClienteFinal;
	
	@Column(name = "ruc",nullable = false)
	String ruc;
	
	@Column(name = "razon_social",nullable = false)
	String razonSocial;
	
	@Column(name = "activo",nullable = false)
	Boolean activo;
	
	@Column(name = "direccion",nullable = false)
	String direccion;
	
	@Column(name = "descripcion",nullable = false)
	String descripcion;	
	
	@ManyToOne(cascade= CascadeType.PERSIST)
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente", nullable = false)
	ClienteModel cliente;	
}
