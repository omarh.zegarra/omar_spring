package com.dsb.persistence.model.persona;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity(name = "personadireccion")
@Table(name = "persona_direccion")
public class PersonaDireccionModel {

	@EmbeddedId
	private PersonaDireccionModelPK id;

}