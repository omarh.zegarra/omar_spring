package com.dsb.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity(name = "token")
@Table(name = "token")
public class TokenModel {	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_token",nullable = false)
	private Long id;
	
	@JsonProperty("access_token")
	@Column(name = "access_token",nullable = false)
	private String accessToken;
	
	@JsonProperty("token_type")
	@Column(name = "token_type",nullable = false)
	private String tokenType;
	
	@JsonProperty("refresh_token")
	@Column(name = "refresh_token",nullable = false)
	private String refreshToken;
	
	@JsonProperty("expires_in")
	@Column(name = "expires_in",nullable = false)
	private Integer expiresIn;	

	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_usuario", nullable = false)
	private UsuarioModel usuario;
	
	@Column(name = "scope",nullable = false)
	private String scope;	
	
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public String getTokenType() {
		return tokenType;
	}
	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}
	public String getRefreshToken() {
		return refreshToken;
	}
	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}
	public Integer getExpiresIn() {
		return expiresIn;
	}
	public void setExpiresIn(Integer expiresIn) {
		this.expiresIn = expiresIn;
	}
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setUsuario(UsuarioModel usuario) {
		this.usuario = usuario;
	}
	public UsuarioModel getUsuario() {
		return usuario;
	}	
}

