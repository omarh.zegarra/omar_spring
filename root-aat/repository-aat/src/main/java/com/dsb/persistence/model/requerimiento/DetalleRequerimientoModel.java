package com.dsb.persistence.model.requerimiento;
/*
package com.dsb.persistence.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity(name = "detalle_requerimiento")
@Table(name = "detalle_requerimiento")
public class DetalleRequerimientoModel implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_detalle_requerimiento",nullable = false)
	Long idDetalleRequerimiento;

	@Column(name = "unidades_requeridas",nullable = false)
	Integer unidadesRequeridas;
	
	@Column(name = "rotulo_producto",nullable = false)
	String rotuloProducto;
	
	@Column(name = "stock_disponible",nullable = false)
	Integer stockDisponible;
	
	@ManyToOne(cascade= CascadeType.PERSIST)
    @JoinColumn(name = "id_requerimiento_bpa", referencedColumnName = "id_requerimiento_bpa", nullable = false)
	RequerimientoBPAmodel requerimientoBPA;   
}
*/
