package com.dsb.persistence.model.requerimiento;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity(name = "requerimiento")
@Table(name = "requerimiento")
@EqualsAndHashCode(exclude={"requerimientoBPM","requerimientoBPA"})
public class RequerimientoModel implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_requerimiento",nullable = false)
	private Long idRequerimiento;
	
	@Column(name = "codigo",nullable = false)
	private String codigo;
	
	@Column(name = "id_cliente",nullable = false)
	Long idCliente;
	
	@Column(name = "fecha_requerimiento",nullable = false)
	String fechaRequerimiento;
	
	@Column(name = "tipo_requerimiento",nullable = false)
	Integer tipoRequerimiento;
	
	@Column(name = "activo",nullable = false)
	Boolean activo;
	
	@Column(name = "id_estado",nullable = false)
	Integer idEstado;
	
	@Column(name = "id_actividad_logistica",nullable = false)
	Integer idActividadLogistica;
	
	@Column(name = "actividad_logistica",nullable = false)
	String actividadLogistica;
	
	@Column(name = "creado_por",nullable = false)
	String creadoPor;
	
	@Column(name = "total_unidades",nullable = false)
	Integer totalUnidades;
	
	@Column(name = "volumen",nullable = false)
	Double volumen;
	
	@Column(name = "fecha_creacion", insertable = false, updatable = false, columnDefinition = "DATE ​​DEFAULT CURRENT_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy'T'HH:mm:ss.SSSZ", timezone="UTC")
	Date fechaCreacion;
	
	@OneToOne(fetch = FetchType.LAZY,cascade = { CascadeType.MERGE,CascadeType.PERSIST }, mappedBy="requerimiento",orphanRemoval = true)
	RequerimientoBPAmodel requerimientoBPA;
	
	@OneToOne(fetch = FetchType.LAZY,cascade = { CascadeType.MERGE,CascadeType.PERSIST }, mappedBy="requerimiento",orphanRemoval = true)
	RequerimientoBPMmodel requerimientoBPM;
}

