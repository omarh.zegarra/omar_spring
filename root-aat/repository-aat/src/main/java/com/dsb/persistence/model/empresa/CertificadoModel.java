package com.dsb.persistence.model.empresa;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
@Entity(name = "certificado")
@Table(name = "certificado")
public class CertificadoModel implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_certificado",nullable = false)
	Long idCertificado;
	
	@Column(name = "cert_digital",nullable = false)
    Byte[] certificadoDigital;
	
	@JsonFormat(pattern="yyyy-MM-dd")
	@Column(name = "fecha_inicio_vig",nullable = false)
	Timestamp  fechaInicioVig;
	
	@JsonFormat(pattern="yyyy-MM-dd")
	@Column(name = "fecha_fin_vig",nullable = false)
	Timestamp  fechaFinVig;
	
	@Column(name = "id_estado",nullable = false)
    String idEstado;
	
	@Column(name = "terminalCreate",nullable = false)
	String terminalCreate;
	
	@JsonFormat(pattern="yyyy-MM-dd")
	@Column(name = "date_create",nullable = false)
	Timestamp  dateCreate;
	
	@Column(name = "userCreate",nullable = false)
	String userCreate;
	
    @ManyToOne(cascade= CascadeType.PERSIST)
    @JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa", nullable = false)
	EmpresaModel empresa;
}
