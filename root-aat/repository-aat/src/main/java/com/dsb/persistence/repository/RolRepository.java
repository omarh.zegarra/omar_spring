package com.dsb.persistence.repository;

import org.springframework.data.repository.CrudRepository;

import com.dsb.persistence.model.RolModel;

public interface RolRepository extends CrudRepository<RolModel, Long>{

}
