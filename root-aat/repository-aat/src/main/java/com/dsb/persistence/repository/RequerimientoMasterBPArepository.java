package com.dsb.persistence.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dsb.persistence.model.requerimiento.RequerimientoMasterBPAmodel;

@Repository
public interface RequerimientoMasterBPArepository extends JpaRepository<RequerimientoMasterBPAmodel, Long>, JpaSpecificationExecutor<RequerimientoMasterBPAmodel>{	

	@Query("select r from requerimiento_master_bpa r where r.idCliente = :idCliente and r.idActividadLogistica in (1,2,3)")
    List<RequerimientoMasterBPAmodel> obtenerRequerimientoXidCliente(@Param("idCliente") Long idCliente);
	
	@Query("select r from requerimiento_master_bpa r where r.idCliente = :idCliente and r.actividadLogistica LIKE %:actividadLogistica%")
    List<RequerimientoMasterBPAmodel> filtroRequerimiento(@Param("idCliente") Long idCliente,@Param("actividadLogistica") String actividadLogistica);

	@Query("select r from requerimiento_master_bpa r where r.idCliente = :idCliente and r.actividadLogistica LIKE %:actividadLogistica%")
    List<RequerimientoMasterBPAmodel> filtro(@Param("idCliente") Long idCliente,@Param("actividadLogistica") String actividadLogistica);
	
}