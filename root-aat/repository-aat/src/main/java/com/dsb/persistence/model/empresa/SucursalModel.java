package com.dsb.persistence.model.empresa;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity(name = "sucursal")
@Table(name = "sucursal")
@EqualsAndHashCode(exclude={ "oficinas"})
public class SucursalModel  implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_sucursal",nullable = false)
	Long idSucursal;
	
	@Column(name = "descripcion",nullable = false)
    String descripcion;
	
	@Column(name = "id_estado",nullable = false)
    String idEstado;
	
    @ManyToOne(cascade= CascadeType.PERSIST)
	@JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa", nullable = false)
	EmpresaModel empresa;
	
	@JsonFormat(pattern="yyyy-MM-dd")
	@Column(name = "date_create",nullable = false)
	Timestamp  dateCreate;
	
	@JsonFormat(pattern="yyyy-MM-dd")
	@Column(name = "date_modify",nullable = false)
	Timestamp  dateModify;
	
	@Column(name = "terminalCreate",nullable = false)
	String terminalCreate;
	
	@Column(name = "terminalModify",nullable = false)
	String terminalModify;
	
	@Column(name = "userCreate",nullable = false)
	String userCreate;
	
	@Column(name = "userModify",nullable = false)
	String userModify;
	
	@OneToMany(fetch = FetchType.LAZY,cascade = { CascadeType.MERGE,CascadeType.PERSIST }, mappedBy="sucursal",orphanRemoval = true)
	private Set<OficinaModel> oficinas= new HashSet<>(0);
}
