package com.dsb.persistence.model.persona;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;

@Data
@Embeddable
public class ClienteModelPK implements Serializable {

	private static final long serialVersionUID = 6064766438002780216L;
	@Column(name = "id_empresa ", nullable = false)
	private Long idEmpresa;
	@Column(name = "id_cliente ", nullable = false)
	private Long idCliente;

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof ClienteModelPK))
			return false;
		ClienteModelPK that = (ClienteModelPK) o;
		return Objects.equals(getIdEmpresa(), that.getIdEmpresa())
				&& Objects.equals(getIdCliente(), that.getIdCliente());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getIdEmpresa(), getIdCliente());
	}

}