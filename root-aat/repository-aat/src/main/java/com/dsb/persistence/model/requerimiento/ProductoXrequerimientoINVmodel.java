package com.dsb.persistence.model.requerimiento;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity(name = "producto_x_requerimiento_inv")
@Table(name = "producto_x_requerimiento_inv")
public class ProductoXrequerimientoINVmodel implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_producto_x_requerimiento",nullable = false)
	Long idProductoXrequerimientoINV;

	@Column(name = "id_producto",nullable = false)
	Long idProducto;
	
	@Column(name = "rotulo_producto",nullable = false)
	String rotuloProducto;
	
	@Column(name = "stock_disponible",nullable = false)
	Integer stockDisponible;
	
	@ManyToOne(cascade= CascadeType.PERSIST)
    @JoinColumn(name = "id_requerimiento_inv", referencedColumnName = "id_requerimiento_inv", nullable = false)
	RequerimientoINVmodel requerimientoINV;   
}
