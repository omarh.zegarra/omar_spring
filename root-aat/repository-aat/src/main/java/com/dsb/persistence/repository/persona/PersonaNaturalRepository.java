package com.dsb.persistence.repository.persona;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dsb.persistence.model.persona.PersonaNaturalModel;

@Repository
public interface PersonaNaturalRepository extends CrudRepository<PersonaNaturalModel, Long> {

}