package com.dsb.persistence.model.persona;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Data;

@Data
@Entity(name = "personanatural")
@Table(name = "persona_natural")
@DynamicUpdate
public class PersonaNaturalModel {

	@Id
	@Column(name = "id_persona_natural", nullable = false)
	private Long idPersonaNatural;
	@Column(name = "id_empresa ", nullable = false)
	private Long idEmpresa;
	@Column(name = "tipo_doc_ident")
	private String tipoDocumentoIdentidad;
	@Column(name = "numero_doc_ident")
	private String numeroDocumentoIdentidad;
	@Column(name = "primer_nombre")
	private String primerNombre;
	@Column(name = "segundo_nombre")
	private String segundoNombre;
	@Column(name = "apellido_paterno")
	private String apellidoPaterno;
	@Column(name = "apellido_materno")
	private String apellidoMaterno;
	@Transient
	private List<String> propertiesUpdate;

}
