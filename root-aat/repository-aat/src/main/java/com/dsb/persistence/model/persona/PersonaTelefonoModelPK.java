package com.dsb.persistence.model.persona;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;

@Data
@Embeddable
public class PersonaTelefonoModelPK implements Serializable {

	private static final long serialVersionUID = -823032713793917697L;
	@Column(name = "id_telefono", nullable = false)
	private Long idTelefono;
	@Column(name = "id_persona", nullable = false)
	private Long idPersona;
	@Column(name = "id_empresa ", nullable = false)
	private Long idEmpresa;

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof PersonaTelefonoModelPK))
			return false;
		PersonaTelefonoModelPK that = (PersonaTelefonoModelPK) o;
		return Objects.equals(getIdPersona(), that.getIdPersona())
				&& Objects.equals(getIdTelefono(), that.getIdTelefono())
				&& Objects.equals(getIdEmpresa(), that.getIdEmpresa());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getIdTelefono(), getIdPersona(), getIdEmpresa());
	}

}