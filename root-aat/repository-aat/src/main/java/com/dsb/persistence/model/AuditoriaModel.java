package com.dsb.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Data
@MappedSuperclass
public class AuditoriaModel{

	@Column(name = "user_create")
	String userCreate;
	@Column(name = "date_create")
	@Temporal(TemporalType.TIMESTAMP)
	Date dateCreate;
	@Column(name = "terminal_create")
	String terminalCreate;
	@Column(name = "user_modify")
	String userModify;
	@Column(name = "date_modify")
	@Temporal(TemporalType.TIMESTAMP)
	Date dateModify;
	@Column(name = "terminal_modify")
	String terminalModify;

}
