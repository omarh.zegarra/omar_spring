package com.dsb.persistence.repository.session;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dsb.persistence.model.session.SessionClienteModel;

@Repository
public interface SessionClienteRepository extends CrudRepository<SessionClienteModel, Long>{
	/*
	@Query("select c from cliente c where c.idCliente = :idCliente")
	ClienteModel obtenerClienteXid(@Param("idCliente") Long idCliente);
	*/
}
