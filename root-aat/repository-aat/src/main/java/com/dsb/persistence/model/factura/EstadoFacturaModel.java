/*package com.dsb.persistence.model.factura;


import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.dsb.persistence.model.requerimiento.RequerimientoModel;

import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@Entity(name = "estado_factura")
@Table(name = "estado_factura")
public class EstadoFacturaModel implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_estado_factura",nullable = false)
	private Long idEstadoFactura;
	
	@Column(name = "nombre",nullable = false)
	private String nombre;
	
	@Column(name = "activo",nullable = false)
	private Long activo;
	
	@Column(name = "descripcion",nullable = false)
	private String descripcion;
	
}
*/