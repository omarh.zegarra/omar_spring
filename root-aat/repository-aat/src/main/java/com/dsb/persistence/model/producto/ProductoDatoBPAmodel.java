package com.dsb.persistence.model.producto;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity(name = "dato_bpa")
@Table(name = "dato_bpa")
public class ProductoDatoBPAmodel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_dato_bpa",nullable = false)
	Integer idDatoBPA;
	
	@Column(name = "fabricante",nullable = false)
	String fabricante;
	
	@Column(name = "presentacion",nullable = false)
	String presentacion;
	
	@Column(name = "concentracion",nullable = false)
	String concentracion;
	
	@Column(name = "tiempo_excursion",nullable = false)
	Integer tiempoExcursion;
	
	@Column(name = "registro_sanitario",nullable = false)
	String registroSanitario;
	
	@Column(name = "id_tipo_temperatura",nullable = false)
	Integer idTipoTemperatura;
	
	@Column(name = "id_tipo_forma",nullable = false)
	Integer idTipoForma;
	
	@OneToOne(cascade= CascadeType.PERSIST)
    @JoinColumn(name = "id_producto", referencedColumnName = "id_producto", nullable = false)
	ProductoModel producto;
}
