package com.dsb.persistence.model.persona;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicUpdate;

import com.dsb.persistence.model.AuditoriaModel;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@Entity(name = "persona")
@Table(name = "persona")
@DynamicUpdate
public class PersonaModel extends AuditoriaModel{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_persona",nullable = false)
	private Long idPersona;	
	@Column(name = "id_empresa ",nullable = false)
	private Long idEmpresa;
	@Column(name = "tipo_persona ",nullable = false)
	private String tipoPersona;
	@Column(name = "id_estado",nullable = false)
	private String idEstado;
	@Transient
	private List<String> propertiesUpdate;
	@ManyToMany
	@JoinTable(
	  name = "persona_telefono", 
	  joinColumns = @JoinColumn(name = "id_persona"), 
	  inverseJoinColumns = @JoinColumn(name = "id_telefono"))
	private List<TelefonoModel> listaTelefonoPersona;
	@ManyToMany
	@JoinTable(
	  name = "persona_direccion", 
	  joinColumns = @JoinColumn(name = "id_persona"), 
	  inverseJoinColumns = @JoinColumn(name = "id_direccion"))
	private List<DireccionModel> listaDireccionPersona;

	
	
	
	
	
}


