package com.dsb.persistence.model.persona;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;

@Data
@Embeddable
public class ProveedorModelPK implements Serializable {

	private static final long serialVersionUID = -5527443617923169542L;
	@Column(name = "id_empresa ", nullable = false)
	private Long idEmpresa;
	@Column(name = "id_proveedor ", nullable = false)
	private Long idProveedor;

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof ProveedorModelPK))
			return false;
		ProveedorModelPK that = (ProveedorModelPK) o;
		return Objects.equals(getIdEmpresa(), that.getIdEmpresa())
				&& Objects.equals(getIdProveedor(), that.getIdProveedor());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getIdEmpresa(), getIdProveedor());
	}

}