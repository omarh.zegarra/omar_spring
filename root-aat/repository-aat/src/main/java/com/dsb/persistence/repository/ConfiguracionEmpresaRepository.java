package com.dsb.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dsb.persistence.model.empresa.ConfiguracionModel;

@Repository
public interface ConfiguracionEmpresaRepository extends JpaRepository<ConfiguracionModel, Long>{

}