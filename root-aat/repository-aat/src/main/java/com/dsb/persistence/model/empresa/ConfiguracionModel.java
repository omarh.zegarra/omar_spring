package com.dsb.persistence.model.empresa;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity(name = "configuracion_empresa")
@Table(name = "configuracion_empresa")
public class ConfiguracionModel implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_configuracion_empresa",nullable = false)
	Long idConfiguracionEmpresa;
	
	@Column(name = "tipo_pago",nullable = false)
	String tipoPago;
	
	@Column(name = "id_moneda",nullable = false)
	String idMoneda;
	
	@Column(name = "ind_exportacion")
	String indExportacion;
	
	@Column(name = "ind_oper_gratis")
	String indOperGratis;
	
	@Column(name = "ind_pago_anticipado")
	String indPagoAnticipado;
	
	@Column(name = "ind_emisor_itinerante")
	String indEmisorItinerante;
	
	@Column(name = "ind_direccion_emisor")
	String indDireccionEmisor;
	
	@Column(name = "ind_direccion_prestacion")
	String indDireccionPrestacion;
	
	@Column(name = "ind_combustible_mant")
	String indCombustibleMant;
	
	@Column(name = "ind_dscto")
	String indDscto;
	
	@Column(name = "ind_isc")
	String indIsc;
	
	@Column(name = "ind_otros_cargos")
	String indOtrosCargos;
	
	@ManyToOne(cascade= CascadeType.PERSIST)
    @JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa", nullable = false)
	private EmpresaModel empresa;   
}