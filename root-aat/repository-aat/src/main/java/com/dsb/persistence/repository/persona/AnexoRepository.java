package com.dsb.persistence.repository.persona;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dsb.persistence.model.persona.AnexoModel;
import com.dsb.persistence.model.persona.AnexoModelPK;

@Repository
public interface AnexoRepository extends CrudRepository<AnexoModel, AnexoModelPK> {

	List<AnexoModel> findByIdIdTelefonoAndIdIdEmpresa(Long idTelefono, Long idEmpresa);
	
}