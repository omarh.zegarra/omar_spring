package com.dsb.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity(name = "lote")
@Table(name = "lote")
public class LoteModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_lote",nullable = false)
	Long idLote;
	
	@Column(name = "descripcion",nullable = false)
	String descripcion;
	
	@Column(name = "envoltura",nullable = false)
	String envoltura;
	
	@Column(name = "volumen",nullable = false)
	Double volumen;
	
	@Column(name = "total",nullable = false)
	Integer total;
	
	@Column(name = "cantidad_disponible",nullable = false)
	Integer cantidadDisponible;
	
	@Column(name = "cantidad_consumida",nullable = false)
	Integer cantidadConsumida;	
}
