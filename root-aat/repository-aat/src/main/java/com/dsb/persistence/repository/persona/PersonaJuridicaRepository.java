package com.dsb.persistence.repository.persona;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dsb.persistence.model.persona.PersonaJuridicaModel;

@Repository
public interface PersonaJuridicaRepository extends CrudRepository<PersonaJuridicaModel, Long> {

}