package com.dsb.persistence.model.persona;

import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicUpdate;

import com.dsb.persistence.model.AuditoriaModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "telefono")
@Table(name = "telefono")
@DynamicUpdate
public class TelefonoModel extends AuditoriaModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_telefono", nullable = false)
	private Long idTelefono;
	@Column(name = "id_empresa", nullable = false)
	private Long idEmpresa;
	@Column(name = "nro_telefono")
	private String numeroTelefono;
	@Column(name = "tipo_telefono")
	private String tipoTelefono;
	@Column(name = "ind_movil")
	private String indicadorMovil;
	@Column(name = "id_estado", nullable = false)
	private String idEstado;
	@Transient
	private List<String> propertiesUpdate;
	
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof TelefonoModel))
			return false;
		TelefonoModel that = (TelefonoModel) o;
		return Objects.equals(getIdTelefono(), that.getIdTelefono());
	}
	
}
