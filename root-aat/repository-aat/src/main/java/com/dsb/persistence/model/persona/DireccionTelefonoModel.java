package com.dsb.persistence.model.persona;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity(name = "direcciontelefono")
@Table(name = "direccion_telefono")
public class DireccionTelefonoModel {

	@EmbeddedId
	private DireccionTelefonoModelPK id;

}