package com.dsb.persistence.model.session;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity(name = "session_coordinador")
@Table(name = "coordinador")
//@EqualsAndHashCode(exclude= {"sessionClientesFinales","sessionProductos","sessionRotulos"})
public class SessionCoordinadorModel implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_coordinador",nullable = false)
	Long idCoordinador;
	
	@Column(name = "horario_lunes_viernes",nullable = false)
	String horarioLunesViernes;
	
	@Column(name = "horario_sabado",nullable = false)
	String horarioSabado;
	
	@Column(name = "activo",nullable = false)
	Boolean activo;
	
	@Column(name = "id_personal",nullable = false)
	Integer idPersonal;
	
	@Column(name = "nombres",nullable = false)
	String nombres;
	
	@Column(name = "correo",nullable = false)
	String correo;
	
	@Column(name = "telefono",nullable = false)
	String telefono;
	
	@OneToOne(cascade= CascadeType.PERSIST)
    @JoinColumn(name = "id_coordinador", referencedColumnName = "id_coordinador", nullable = false)
	SessionClienteModel sessionCliente;	
}
