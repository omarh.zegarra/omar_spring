package com.dsb.persistence.repository.persona;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dsb.persistence.model.persona.TransportadorModelPK;
import com.dsb.persistence.model.persona.TransporteModel;

@Repository
public interface TransporteRepository extends CrudRepository<TransporteModel, TransportadorModelPK>{

}