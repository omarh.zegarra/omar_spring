package com.dsb.persistence.model.producto;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity(name = "dato_logistico")
@Table(name = "dato_logistico")
public class ProductoDatoLogisticoModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_dato_logistico",nullable = false)
	Integer idDatoLogistico;
	
	@Column(name = "id_tipo_envase_mediato2",nullable = false)
	Integer idTipoEnvaseMediato2;
	
	@Column(name = "unidades_envase_mediato2",nullable = false)
	Integer unidadesEnvaseMediato2;
	
	@Column(name = "volumen_envase_mediato2",nullable = false)
	Double volumenEnvaseMediato2;
	
	@Column(name = "peso_envase_mediato2",nullable = false)
	Double pesoEnvaseMediato2;
	
	@Column(name = "id_tipo_envase_mediato3",nullable = false)
	Integer idTipoEnvaseMediato3;
	
	@Column(name = "unidades_envase_mediato3",nullable = false)
	Integer unidadesEnvaseMediato3;
	
	@Column(name = "volumen_envase_mediato3",nullable = false)
	Double volumenEnvaseMediato3;
	
	@Column(name = "peso_envase_mediato3",nullable = false)
	Double pesoEnvaseMediato3;
		
	@Column(name = "id_tipo_envase_caja_master",nullable = false)
	Integer idTipoEnvaseCajaMaster;
	
	@Column(name = "unidades_envase_caja_master",nullable = false)
	Integer unidadesEnvaseCajaMaster;
	
	@Column(name = "volumen_envase_caja_master",nullable = false)
	Double volumenEnvaseCajaMaster;
	
	@Column(name = "peso_envase_caja_master",nullable = false)
	Double pesoEnvaseCajaMaster;
	
	@Column(name = "id_ean_envase_mediato2",nullable = false)
	Integer idEANenvaseMediato2;
	
	@Column(name = "id_ean_envase_mediato3",nullable = false)
	Integer idEANenvaseMediato3;
	
	@Column(name = "id_ean_envase_caja_master",nullable = false)
	Integer idEANenvaseCajaMaster;
	
	@Column(name = "caja_master_x_cama",nullable = false)
	String cajaMasterXcama;
	
	@Column(name = "cama_x_pallet",nullable = false)
	Integer camaXpallet;
	
	@Column(name = "id_tipo_pallet",nullable = false)
	Integer idTipoPallet;
	
	@Column(name = "total_unidades_x_pallet",nullable = false)
	Integer totalUnidadesXpallet;
	
	@Column(name = "id_tipo_carga",nullable = false)
	Integer idTipoCarga;
	
	@OneToOne(cascade= CascadeType.PERSIST)
    @JoinColumn(name = "id_producto", referencedColumnName = "id_producto", nullable = false)
	ProductoModel producto;
	
}
