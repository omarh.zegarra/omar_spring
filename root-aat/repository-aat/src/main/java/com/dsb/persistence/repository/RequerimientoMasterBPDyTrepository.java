package com.dsb.persistence.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dsb.persistence.model.requerimiento.RequerimientoMasterBPDyTmodel;

@Repository
public interface RequerimientoMasterBPDyTrepository extends CrudRepository<RequerimientoMasterBPDyTmodel, Long>{	
	@Query("select r from requerimiento_master_bpd r where r.idRequerimiento = :idRequerimiento")
    List<RequerimientoMasterBPDyTmodel> obtenerRequerimientoXid(@Param("idRequerimiento") Long idRequerimiento);

	@Query("select r from requerimiento_master_bpd r where r.idCliente = :idCliente and "
			+ "r.idActividadLogistica= :idActividadLogistica and r.estado LIKE CONCAT('%',:estado,'%')")
    List<RequerimientoMasterBPDyTmodel> obtenerRequerimientoXidCliente(@Param("idCliente") Long idCliente, 
    		@Param("idActividadLogistica") Integer idActividadLogistica, @Param("estado") String estado);
	
	@Query("select r from requerimiento_master_bpd r where r.idActividadLogistica= 5 and r.idCliente = :idCliente")
    List<RequerimientoMasterBPDyTmodel> obtenerRequerimientoPorIdCliente(@Param("idCliente") Long idCliente);
	
	@Query("select r from requerimiento_master_bpd r where r.idCliente = :idCliente and	r.idActividadLogistica= 5 and r.estado LIKE CONCAT('%',:estado,'%')")
    List<RequerimientoMasterBPDyTmodel> filtroRequerimiento(@Param("idCliente") Long idCliente, @Param("estado") String estado);
}
