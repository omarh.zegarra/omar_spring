package com.dsb.persistence.model.persona;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity(name = "personatelefono")
@Table(name = "persona_telefono")
public class PersonaTelefonoModel {

	@EmbeddedId
	private PersonaTelefonoModelPK id;
	
}