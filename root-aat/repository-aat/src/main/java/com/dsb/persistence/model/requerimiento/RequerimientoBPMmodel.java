package com.dsb.persistence.model.requerimiento;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity(name = "requerimiento_bpm")
@Table(name = "requerimiento_bpm")
@EqualsAndHashCode(exclude={"requerimiento","detallesRequerimientos"})
public class RequerimientoBPMmodel implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_requerimiento_bpm",nullable = false)
	Long idRequerimientoBPM;
	
	@Column(name = "id_tipo_documento",nullable = false)
	Integer idTipoDocumento;
	
	@Column(name = "numero_documento",nullable = false)
	String numeroDocumento;
	
	@Column(name = "id_proceso_bpm",nullable = false)
	Integer idProcesoBPM;
	
	@Column(name = "proceso_principal",nullable = false)
	String procesoPrincipal;
	
	@Column(name = "consolidacion_carga",nullable = false)
	Boolean consolidacionCarga;
	
	@Column(name = "peso_cajas_mediatas",nullable = false)
	Boolean pesoCajasMediatas;
	
	@Column(name = "peso_cajas_desuso",nullable = false)
	Boolean pesoCajasDesuso;
	
	@Column(name = "trituracion_desecho",nullable = false)
	Boolean trituracionDesecho;
	
	@Column(name = "id_detalle_tipo_dato_carga",nullable = false)
	Integer idDetalleTipoDatoCarga;
	
	@Column(name = "id_tipo_dato_carga",nullable = false)
	Integer idTipoDatoCarga;
	
	@Column(name = "fecha_hora_ingreso",nullable = false)
	String fechaHoraIngreso;
	
	@Column(name = "es_furgon",nullable = false)
	Boolean esFurgon;
	
	@Column(name = "recoge_aat",nullable = false)
	Boolean recogeAAT;
	
	@Column(name = "entrega_aat",nullable = false)
	Boolean entregaAAT;
	
	@OneToOne(cascade= CascadeType.PERSIST)
    @JoinColumn(name = "id_requerimiento", referencedColumnName = "id_requerimiento", nullable = false)
	RequerimientoMasterBPMmodel requerimiento;
			
	@OneToMany(fetch = FetchType.LAZY,cascade = { CascadeType.MERGE,CascadeType.PERSIST }, mappedBy="requerimientoBPM",orphanRemoval = true)
	Set<ProductoXrequerimientoBPMmodel> detallesRequerimientos;		
}
