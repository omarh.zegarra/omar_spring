package com.dsb.persistence;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.dsb.persistence.repository.persona.AnexoRepository;
import com.dsb.persistence.repository.persona.CorreoRepository;
import com.dsb.persistence.repository.persona.DireccionRepository;
import com.dsb.persistence.repository.persona.DireccionTelefonoRepository;
import com.dsb.persistence.repository.persona.PersonaDireccionRepository;
import com.dsb.persistence.repository.persona.PersonaJuridicaRepository;
import com.dsb.persistence.repository.persona.PersonaNaturalRepository;
import com.dsb.persistence.repository.persona.PersonaRepository;
import com.dsb.persistence.repository.persona.PersonaTelefonoRepository;
import com.dsb.persistence.repository.persona.TelefonoRepository;
import com.dsb.persistence.repository.persona.UbigeoRepository;

@Configuration 
@EnableJpaRepositories("com.dsb.persistence.repository")
@SpringBootApplication
@EnableAutoConfiguration
@EntityScan("com.dsb.persistence.model")
public class App implements CommandLineRunner{
	private static final Logger logger = LogManager.getLogger(App.class);
	
	@Autowired
	PersonaRepository personaRepository;
	
	@Autowired
	PersonaNaturalRepository personaNaturalRepository;
	
	@Autowired
	PersonaJuridicaRepository personaJuridicaRepository;
	
	@Autowired
	TelefonoRepository telefonoRepository;
	
	@Autowired
	PersonaTelefonoRepository personaTelefonoRepository;
	
	@Autowired
	DireccionRepository direccionRepository;
	
	@Autowired
	DireccionTelefonoRepository direccionTelefonoRepository;
	
	@Autowired
	PersonaDireccionRepository personaDireccionRepository;
	
	@Autowired
	AnexoRepository anexoRepository;
	
	@Autowired
	CorreoRepository correoRepository;
	
	@Autowired
	UbigeoRepository ubigeoRepository;
	
    public static void main( String[] args ){
    	SpringApplication.run(App.class, args);
    }

	@Override
	public void run(String... arg0) throws Exception {
		logger.info("perona repository..."+personaRepository);
		/*PersonaModel persona = new PersonaModel();
		persona.setIdEmpresa(1L);
		persona.setTipoPersona("2");
		persona.setIdEstado("2");
		personaRepository.save(persona);
		
		PersonaNaturalModel personaNatural = new PersonaNaturalModel();
		personaNatural.setIdPersonaNatural(1L);
		personaNatural.setIdEmpresa(1L);
		personaNaturalRepository.save(personaNatural);
		
		
		PersonaJuridicaModel personaJuridica = new PersonaJuridicaModel();
		personaJuridica.setIdPersonaJuridica(1L);
		personaJuridica.setIdEmpresa(1L);
		personaJuridicaRepository.save(personaJuridica);
		
		
		TelefonoModel telefono = new TelefonoModel();
		telefono.setIdEmpresa(1L);
		telefono.setIdEstado("2");
		telefonoRepository.save(telefono);
		
		
		PersonaTelefonoModel personaTelefono = new PersonaTelefonoModel();
		PersonaTelefonoModelPK personaTelefonoPK = new PersonaTelefonoModelPK();
		personaTelefonoPK.setIdEmpresa(1L);
		personaTelefonoPK.setIdPersona(1L);
		personaTelefonoPK.setIdTelefono(1L);
		personaTelefono.setId(personaTelefonoPK);
		personaTelefonoRepository.save(personaTelefono);
		
		
		
		DireccionModel direccion = new DireccionModel();
		direccion.setIdEmpresa(1L);
		direccion.setIdEstado("2");
		direccionRepository.save(direccion);
		
		DireccionTelefonoModel direccionTelefono = new DireccionTelefonoModel();
		DireccionTelefonoModelPK direccionTelefonoPK = new DireccionTelefonoModelPK();
		direccionTelefonoPK.setIdEmpresa(1L);
		direccionTelefonoPK.setIdDireccion(2L);
		direccionTelefonoPK.setIdTelefono(1L);
		direccionTelefono.setId(direccionTelefonoPK);
		direccionTelefonoRepository.save(direccionTelefono);
		
		PersonaDireccionModel personaDireccion = new PersonaDireccionModel();
		PersonaDireccionModelPK personaDireccionPK = new PersonaDireccionModelPK();
		personaDireccionPK.setIdEmpresa(1L);
		personaDireccionPK.setIdDireccion(2L);
		personaDireccionPK.setIdPersona(1L);
		personaDireccion.setId(personaDireccionPK);
		personaDireccionRepository.save(personaDireccion);
		
		AnexoModel anexo = new AnexoModel();
		anexo.setIdEmpresa(1L);
		anexo.setIdTelefono(1L);
		anexo.setIdEstado("2");
		anexoRepository.save(anexo);
		
		CorreoModel correo = new CorreoModel();
		correo.setIdEmpresa(1L);
		correo.setIdPersona(1L);
		correo.setIdEstado("2");
		correo.setTipo_correo("1");
		correoRepository.save(correo);
		
		UbigeoModel ubigeo = new UbigeoModel();
		ubigeo.setIdUbigeo("1");
		ubigeoRepository.save(ubigeo);
		
		*/
		
		
		
		
	}
}