package com.dsb.persistence.model.persona;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicUpdate;

import com.dsb.persistence.model.AuditoriaModel;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@Entity(name = "correo")
@Table(name = "correo")
@DynamicUpdate
public class CorreoModel extends AuditoriaModel {

	@EmbeddedId
	private CorreoModelPK id;
	@Column(name = "email")
	private String email;
	@Column(name = "tipo_correo")
	private String tipo_correo;
	@Column(name = "id_estado", nullable = false)
	private String idEstado;
	@Transient
	private List<String> propertiesUpdate;

}
