package com.dsb.persistence.model.persona;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicUpdate;

import com.dsb.persistence.model.AuditoriaModel;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity(name = "anexo")
@Table(name = "anexo")
@DynamicUpdate
public class AnexoModel extends AuditoriaModel {

	@EmbeddedId
	private AnexoModelPK id;
	@Column(name = "nro_anexo")
	private String numeroAnexo;
	@Column(name = "id_estado", nullable = false)
	private String idEstado;
	@Transient
	private List<String> propertiesUpdate;

}
