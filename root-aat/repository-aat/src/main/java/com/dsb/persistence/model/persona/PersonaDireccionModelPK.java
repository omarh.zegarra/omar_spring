package com.dsb.persistence.model.persona;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;

@Data
@Embeddable
public class PersonaDireccionModelPK implements Serializable {

	private static final long serialVersionUID = 6528294466007752843L;
	@Column(name = "id_persona", nullable = false)
	private Long idPersona;
	@Column(name = "id_direccion", nullable = false)
	private Long idDireccion;
	@Column(name = "id_empresa ", nullable = false)
	private Long idEmpresa;

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof PersonaDireccionModelPK))
			return false;
		PersonaDireccionModelPK that = (PersonaDireccionModelPK) o;
		return Objects.equals(getIdPersona(), that.getIdPersona())
				&& Objects.equals(getIdDireccion(), that.getIdDireccion())
				&& Objects.equals(getIdEmpresa(), that.getIdEmpresa());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getIdPersona(), getIdEmpresa());
	}

}