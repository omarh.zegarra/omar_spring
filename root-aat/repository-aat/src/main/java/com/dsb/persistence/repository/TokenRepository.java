package com.dsb.persistence.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dsb.persistence.model.TokenModel;

@Repository
public interface TokenRepository extends CrudRepository<TokenModel, Long>{
	@Query("select t from token t where t.accessToken=:accessToken")
    TokenModel findByAccesToken(@Param("accessToken") String accessToken);
	
	@Query("select t from token t inner join usuario u on t.usuario=u.token and u.correo=:correo")
    TokenModel findByEmailUser(@Param("correo") String correo);
	
	@Query("select t from token t inner join usuario u on t.usuario=u.token and t.accessToken=:accessToken")
    TokenModel findUserByAccessToken(@Param("accessToken") String accessToken);
	
	@Query("select t from token t where t.accessToken=:bearerToken")
    TokenModel verifyToken(@Param("bearerToken") String bearerToken);
}
