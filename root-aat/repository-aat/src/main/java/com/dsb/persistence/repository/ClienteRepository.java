package com.dsb.persistence.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dsb.persistence.model.ClienteModel;

@Repository
public interface ClienteRepository extends CrudRepository<ClienteModel, Long>{
	/*
	@Query("select c from cliente c where c.idCliente = :idCliente")
	ClienteModel obtenerClienteXid(@Param("idCliente") Long idCliente);
	*/
}
