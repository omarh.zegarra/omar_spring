package com.dsb.persistence.model.persona;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Data;

@Data
@Entity(name = "clientex")
@Table(name = "clientex")
@DynamicUpdate
public class ClienteModel {

	@EmbeddedId
	private ClienteModelPK id;
	
}
