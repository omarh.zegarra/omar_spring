package com.dsb.persistence.model.empresa;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity(name = "oficina")
@Table(name = "oficina")
@EqualsAndHashCode(exclude={"direcciones"})
public class OficinaModel implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_oficina",nullable = false)
	Integer idOficina;	
	
	@JsonFormat(pattern="yyyy-MM-dd")
	@Column(name = "dateCreate",nullable = false)
	Timestamp dateCreate;
	
	@JsonFormat(pattern="yyyy-MM-dd")
	@Column(name = "dateModify",nullable = false)
	Timestamp dateModify;
	
	@Column(name = "descripcion",nullable = false)
	String descripcion;
	
	@Column(name = "id_estado",nullable = false)
	String idEstado;
	
	@Column(name = "id_oficina_sunat",nullable = false)
	String idOficinaSunat;
	
	@Column(name = "terminal_create",nullable = false)
	String terminalCreate;
	
	@Column(name = "terminal_modify",nullable = false)
	String terminalModify;
	
	@Column(name = "user_create",nullable = false)
	String userCreate;
	
	@Column(name = "user_modify",nullable = false)
	String userModify;
	
    @ManyToOne(cascade= CascadeType.PERSIST)
    @JoinColumn(name = "id_sucursal", referencedColumnName = "id_sucursal", nullable = false)
	private SucursalModel sucursal;
    
	@OneToMany(fetch = FetchType.LAZY,cascade = {CascadeType.PERSIST,CascadeType.MERGE }, mappedBy="oficina",orphanRemoval = true)
	private Set<DireccionModel> direcciones;
	
}