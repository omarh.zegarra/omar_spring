package com.dsb.persistence.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dsb.persistence.model.requerimiento.RequerimientoMasterBPMmodel;

@Repository
public interface RequerimientoMasterBPMrepository extends CrudRepository<RequerimientoMasterBPMmodel, Long>{		
	@Query("select r from requerimiento_master_bpm r where r.idCliente = :idCliente and"
			+ " r.idActividadLogistica= :idActividadLogistica and r.estado LIKE CONCAT('%',:estado,'%')")
    List<RequerimientoMasterBPMmodel> obtenerRequerimientosXidCliente(
    	@Param("idCliente") Long idCliente,@Param("idActividadLogistica") Integer idActividadLogistica, @Param("estado") String estado);
	
	@Query("select r from requerimiento_master_bpm r where r.idCliente = :idCliente")
    List<RequerimientoMasterBPMmodel> listaRequerimientoPorIdCliente(@Param("idCliente") Long idCliente);
	
	@Query("select r from requerimiento_master_bpm r where r.idCliente = :idCliente and	r.idActividadLogistica= 4 and r.estado LIKE CONCAT('%',:estado,'%')")
    List<RequerimientoMasterBPMmodel> filtroRequerimiento(@Param("idCliente") Long idCliente, @Param("estado") String estado);
	
	@Query("select r from requerimiento_master_bpm r where r.idActividadLogistica= 4 and r.idCliente = :idCliente")
    List<RequerimientoMasterBPMmodel> obtenerRequerimientosPorIdCliente(@Param("idCliente") Long idCliente);
}
