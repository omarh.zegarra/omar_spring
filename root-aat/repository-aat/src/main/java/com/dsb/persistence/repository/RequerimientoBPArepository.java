

package com.dsb.persistence.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dsb.persistence.model.requerimiento.RequerimientoBPAmodel;

@Repository
public interface RequerimientoBPArepository extends CrudRepository<RequerimientoBPAmodel, Long>{	

	@Query("select r from requerimiento_bpa r inner join requerimiento_master_bpa q on "
			 + " r.requerimiento.idRequerimiento = q.idRequerimiento where q.idCliente = :idCliente and r.tipoEvento = :tipoEvento")
	List<RequerimientoBPAmodel> obtenerRequerimientosBPAxTipoEventoYcliente(@Param("idCliente") Long idCliente,@Param("tipoEvento") String tipoEvento);
}
