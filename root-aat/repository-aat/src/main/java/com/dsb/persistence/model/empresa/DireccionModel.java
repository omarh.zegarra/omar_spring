package com.dsb.persistence.model.empresa;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity(name = "direccion")
@Table(name = "direccion")
public class DireccionModel implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_direccion",nullable = false)
	private Long idDireccion;

	@JsonFormat(pattern="yyyy-MM-dd")
	@Column(name = "date_create",nullable = false)
	private Timestamp dateCreate;
	
	@JsonFormat(pattern="yyyy-MM-dd")
	@Column(name = "date_modify",nullable = false)
	private Timestamp dateModify;

	@Column(name = "descripcion",nullable = false)
	private String descripcion;

	@Column(name = "id_estado",nullable = false)
	private String idEstado;

	@Column(name = "id_ubigeo",nullable = false)
	private String idUbigeo;

	@Column(name = "terminal_create",nullable = false)
	private String terminalCreate;

	@Column(name = "terminal_modify",nullable = false)
	private String terminalModify;

	@Column(name = "tipo_direccion",nullable = false)
	private String tipoDireccion;

	@Column(name = "tipo_via",nullable = false)
	private String tipoVia;

	@Column(name = "tipo_zona",nullable = false)
	private String tipoZona;

	@Column(name = "user_create",nullable = false)
	private String userCreate;

	@Column(name = "user_modify",nullable = false)
	private String userModify;

	@Column(name = "zona",nullable = false)
	private String zona;
	
	@JsonIgnore
    @ManyToOne(cascade= CascadeType.PERSIST)
	@JoinColumn(name = "id_oficina", referencedColumnName = "id_oficina", nullable = false)
	OficinaModel oficina;
}
