package com.dsb.persistence.repository.persona;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dsb.persistence.model.persona.LaboratorioModel;
import com.dsb.persistence.model.persona.LaboratorioModelPK;

@Repository
public interface LaboratorioRepository extends CrudRepository<LaboratorioModel, LaboratorioModelPK>{

}