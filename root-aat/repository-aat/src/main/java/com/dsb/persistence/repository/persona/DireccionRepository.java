package com.dsb.persistence.repository.persona;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dsb.persistence.model.persona.DireccionModel;

@Repository
public interface DireccionRepository extends CrudRepository<DireccionModel, Long>{

}