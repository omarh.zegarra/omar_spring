package com.dsb.persistence.model.requerimiento;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity(name = "requerimiento_bpdyt")
@Table(name = "requerimiento_bpdyt")
@EqualsAndHashCode(exclude={"requerimiento"})
public class RequerimientoBPDyTmodel implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_requerimiento_bpdyt",nullable = false)
	Long idRequerimientoBPDyt;
	
	@Column(name = "documento",nullable = false)
	String documento;
	
	@Column(name = "id_tipo_servicio",nullable = false)
	Integer idTipoServicio;
	
	@Column(name = "fecha_requerida",nullable = false)
	String fechaRequerida;
	
	@Column(name = "ventana_hora_inicio",nullable = false)
	String ventanaHoraInicio;
	
	@Column(name = "ventana_hora_fin",nullable = false)
	String ventanaHoraFin;
		
	@Column(name = "nombres_contacto",nullable = false)
	String nombresContacto;
	
	@Column(name = "telefono_contacto",nullable = false)
	String telefonoContacto;
	
	@Column(name = "id_cliente_final",nullable = false)
	Integer idClienteFinal;
	
	@Column(name = "cliente_final",nullable = false)
	String clienteFinal;
	
	@Column(name = "direccion_recojo",nullable = false)
	String direccionRecojo;
	
	@Column(name = "caracteristicas_recojo",nullable = false)
	String caracteristicasRecojo;
	
	@Column(name = "tiempo_anticipacion_recojo",nullable = false)
	String tiempoAnticipacionRecojo;
	
	@OneToOne(cascade= CascadeType.PERSIST)
    @JoinColumn(name = "id_requerimiento", referencedColumnName = "id_requerimiento", nullable = false)
	RequerimientoMasterBPDyTmodel requerimiento;
}
