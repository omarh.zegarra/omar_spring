package com.dsb.persistence.model.persona;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;

@Data
@Embeddable
public class CorreoModelPK implements Serializable {

	private static final long serialVersionUID = 5563067266408567954L;
	@Column(name = "id_correo", nullable = false)
	private Long idCorreo;
	@Column(name = "id_persona ", nullable = false)
	private Long idPersona;
	@Column(name = "id_empresa ", nullable = false)
	private Long idEmpresa;

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof CorreoModelPK))
			return false;
		CorreoModelPK that = (CorreoModelPK) o;
		return Objects.equals(getIdCorreo(), that.getIdCorreo()) && Objects.equals(getIdPersona(), that.getIdPersona())
				&& Objects.equals(getIdEmpresa(), that.getIdEmpresa());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getIdCorreo(), getIdPersona(), getIdEmpresa());
	}

}