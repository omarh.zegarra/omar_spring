package com.dsb.persistence.model.producto;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity(name = "producto")
@Table(name = "producto")
@EqualsAndHashCode(exclude={"productoDatoBPA", "productoDatoLogistico"})

public class ProductoModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_producto",nullable = false)
	Integer idProducto;
	
	@Column(name = "codigo",nullable = false)
	String codigo; 
	
	@Column(name = "id_cliente",nullable = false)
	Integer idCliente;
	
	@Column(name = "sku",nullable = false)
	String sku; 
	
	@Column(name = "nombre",nullable = false)
	String nombre;
	
	@Column(name = "precio_unitario",nullable = false)
	Double precioUnitario;
	
	@Column(name = "producto_peligroso",nullable = false)
	Boolean productoPeligroso;
	
	@Column(name = "posee_codigo_barra",nullable = false)
	Boolean poseeCodigoBarra;
	
	@Column(name = "aplica_fecha_vencimiento",nullable = false)
	Boolean aplicaFechaVencimiento;
	
	@Column(name = "stock_alerta_minimo",nullable = false)
	Double stockAlertaMinimo;
	
	@Column(name = "stock_alerta_maximo",nullable = false)
	Double stockAlertaMaximo;
	
	@Column(name = "punto_reposicion",nullable = false)
	Integer puntoReposicion;
	
	@Column(name = "nombre_ingles",nullable = false)
	String nombreIngles;
	
	@Column(name = "volumen",nullable = false)
	Double volumen;
	
	@Column(name = "activo",nullable = false)
	Boolean activo;
	
	@OneToOne(fetch = FetchType.LAZY,cascade = { CascadeType.MERGE,CascadeType.PERSIST }, mappedBy="producto",orphanRemoval = true)
	ProductoDatoBPAmodel productoDatoBPA;
	
	@OneToOne(fetch = FetchType.LAZY,cascade = { CascadeType.MERGE,CascadeType.PERSIST }, mappedBy="producto",orphanRemoval = true)
	ProductoDatoLogisticoModel productoDatoLogistico;
	
	/*
	@Transient
	@OneToOne(fetch = FetchType.LAZY,cascade = { CascadeType.MERGE,CascadeType.PERSIST } ,orphanRemoval = true)
	LoteModel lote;
	*/
}
