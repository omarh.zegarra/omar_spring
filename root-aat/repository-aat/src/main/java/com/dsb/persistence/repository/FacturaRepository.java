/*package com.dsb.persistence.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dsb.persistence.model.factura.FacturaModel;
import com.dsb.persistence.model.requerimiento.RequerimientoModel;

@Repository
public interface FacturaRepository extends CrudRepository<FacturaModel, Long> {
	
	@Query("select f from factura f where f.idCliente = :idCliente")
    List<FacturaModel> obtenerFacturas(@Param("idCliente") Long idCliente);

}
*/