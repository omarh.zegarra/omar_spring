package com.dsb.persistence.model.persona;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;

@Data
@Embeddable
public class VendedorModelPK implements Serializable {

	private static final long serialVersionUID = 3566821093121173877L;
	@Column(name = "id_empresa ", nullable = false)
	private Long idEmpresa;
	@Column(name = "id_vendedor ", nullable = false)
	private Long idVendedor;

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof VendedorModelPK))
			return false;
		VendedorModelPK that = (VendedorModelPK) o;
		return Objects.equals(getIdEmpresa(), that.getIdEmpresa())
				&& Objects.equals(getIdVendedor(), that.getIdVendedor());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getIdEmpresa(), getIdVendedor());
	}

}