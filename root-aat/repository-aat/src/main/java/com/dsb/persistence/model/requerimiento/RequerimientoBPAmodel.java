package com.dsb.persistence.model.requerimiento;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity(name = "requerimiento_bpa")
@Table(name = "requerimiento_bpa")
@EqualsAndHashCode(exclude={"requerimiento","detallesRequerimientos"})
public class RequerimientoBPAmodel implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_requerimiento_bpa",nullable = false)
	Long idRequerimientoBPA;
	
	@Column(name = "id_tipo_carga",nullable = false)
	Integer idTipoCarga;
	
	@Column(name = "id_tipo_salida",nullable = false)
	Integer idTipoSalida;
	
	@Column(name = "tipo_salida",nullable = false)
	String tipoSalida;
	
	@Column(name = "entrega_aat",nullable = false)
	Boolean entregaAAT;
	
	@Column(name = "recoge_aat",nullable = false)
	Boolean recogeAAT;
	
	@Column(name = "id_tipo_documento",nullable = false)
	Integer idTipoDocumento;
	
	@Column(name = "tipo_documento",nullable = false)
	String tipoDocumento;
	
	@Column(name = "numero_documento",nullable = false)
	String numeroDocumento;
	
	@Column(name = "id_tipo_documento2",nullable = false)
	Integer idTipoDocumento2;
	
	@Column(name = "tipo_documento2",nullable = false)
	String tipoDocumento2;
	
	@Column(name = "numero_documento2",nullable = false)
	String numeroDocumento2;
	
	@Column(name = "es_lima",nullable = false)
	Boolean esLima;
	
	@Column(name = "direccion",nullable = false)
	String direccion;
	
	@Column(name = "id_tipo_ingreso",nullable = false)
	Integer idTipoIngreso;
	
	@Column(name = "tipo_ingreso",nullable = false)
	String tipoIngreso;
		
	@Column(name = "detalle_requerimiento",nullable = false)
	String detalleRequerimiento;
		
	@Column(name = "es_furgon",nullable = false)
	Boolean esfurgon;
	
	@Column(name = "fecha_hora_ingreso",nullable = false)
	String fechaHoraIngreso;
	
	@Column(name = "condiciones_empaque",nullable = false)
	String condicionesEmpaque;
	
	@Column(name = "id_cliente_final",nullable = false)
	Integer idClienteFinal;
		
	@Column(name = "cliente_final",nullable = false)
	String clienteFinal;
	
	@Column(name = "tipo_evento",nullable = false)
	String tipoEvento;
	
	@Column(name = "id_status_origen",nullable = false)
	Integer idStatusOrigen;
	
	@Column(name = "status_origen",nullable = false)
	String statusOrigen;
	
	@Column(name = "id_status_destino",nullable = false)
	Integer idStatusDestino;
	
	@Column(name = "status_destino",nullable = false)
	String statusDestino;
	
	@Column(name = "detalle_transferencia",nullable = false)
	String detalleTransferencia;
	
	@OneToOne(cascade= CascadeType.PERSIST)
    @JoinColumn(name = "id_requerimiento", referencedColumnName = "id_requerimiento", nullable = false)
	RequerimientoMasterBPAmodel requerimiento;
			
	@OneToMany(fetch = FetchType.LAZY,cascade = { CascadeType.MERGE,CascadeType.PERSIST }, mappedBy="requerimientoBPA",orphanRemoval = true)
	private Set<ProductoXrequerimientoBPAmodel> detallesRequerimientos;		
}
