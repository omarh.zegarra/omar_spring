package com.dsb.persistence.model;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class FiltroRequerimiento implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Long idCliente;
	private String actividadLogistica;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
	private Date fechaInicio;
	private Date fechaFin;
}
