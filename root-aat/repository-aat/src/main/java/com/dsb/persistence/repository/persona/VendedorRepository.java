package com.dsb.persistence.repository.persona;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dsb.persistence.model.persona.VendedorModel;
import com.dsb.persistence.model.persona.VendedorModelPK;

@Repository
public interface VendedorRepository extends CrudRepository<VendedorModel, VendedorModelPK>{

}