package com.dsb.operaciones.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Service;

import com.dsb.core.common.http.Response;
import com.dsb.core.domain.cliente.Cumplimiento;
import com.dsb.core.domain.cliente.CumplimientoCita;
import com.dsb.core.domain.cliente.EntregaRecojo;
import com.dsb.core.domain.cliente.Excedente;
import com.dsb.core.domain.cliente.Movimiento;
import com.dsb.core.domain.cliente.OperacionesWrapper;

import reactor.core.publisher.Mono;

@Service
public class OperacionesServiceImpl implements OperacionesService{

	@Override
	public Mono<Response<OperacionesWrapper>> getIngresosSalidas(@Valid String idUsuario) {
		return Mono.just(new Response<OperacionesWrapper>(generateMockWrapper()));
	}

	@Override
	public Mono<Response<List<Excedente>>> getExcedente(@Valid String idUsuario) {
		return Mono.just(new Response<List<Excedente>>(generateMockWrapper().getExcedentes()));
	}

	@Override
	public Mono<Response<OperacionesWrapper>> getEntregaRecojo(@Valid String idUsuario) {
		return Mono.just(new Response<OperacionesWrapper>(generateMockWrapper()));
	}

	@Override
	public Mono<Response<CumplimientoCita>> getCumpliminetoCita(@Valid String idUsuario) {
		return Mono.just(new Response<CumplimientoCita>(generateMockWrapper().getCumplimientoCita()));
	}
	
	private OperacionesWrapper generateMockWrapper() {
		OperacionesWrapper op=OperacionesWrapper.builder()
			.cumplimiento(Cumplimiento.builder().dentroHora("10").fueraHora("20").build())
			.entregaRecojo(EntregaRecojo.builder().parcial(10.0).rechazo(20.0).total(30.0).build())
			.cumplimientoCita(CumplimientoCita.builder().dentroHoraProgramada("20").fueraHoraProgramada("30")
				.promedioAntesHora(30.0).promedioRetraso(40.0).build())
			.excedentes(new ArrayList<>(Arrays.asList(Excedente.builder().dia("lunes").volumen(50.0).build())))
			.movimientos(new ArrayList<>(Arrays.asList(Movimiento.builder().semana(12).tipo("S").volumen(50.0).build())))
			.build();
		return op;
	}
}
