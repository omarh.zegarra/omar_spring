package com.dsb.operaciones.controller;


import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dsb.core.common.http.Response;
import com.dsb.core.domain.cliente.CumplimientoCita;
import com.dsb.core.domain.cliente.Excedente;
import com.dsb.core.domain.cliente.OperacionesWrapper;
import com.dsb.operaciones.service.OperacionesService;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/client/web/")
public class OperacionesController {
	
	@Autowired
	OperacionesService operacionesService;
	
	@GetMapping(value = "/")
	public String ping(){
		return "client/web controller is running..";
	}
	
	@GetMapping(value = "/logistica/{idUsuario}")
	public Mono<Response<OperacionesWrapper>> getIngresosSalidas(@Valid @PathVariable String idUsuario){
		return operacionesService.getIngresosSalidas(idUsuario);
	}
	
	@GetMapping(value = "/reacondicionado/bpm/{idUsuario}")
	public Mono<Response<List<Excedente>>> getExcedente(@Valid @PathVariable String idUsuario){
		return operacionesService.getExcedente(idUsuario);
	}
	
	@GetMapping(value = "/status/entrega/recojo/{idUsuario}")
	public Mono<Response<OperacionesWrapper>> getEntregaRecojo(@Valid @PathVariable String idUsuario){
		return operacionesService.getEntregaRecojo(idUsuario);
	}
	
	@GetMapping(value = "/transportes/{idUsuario}")
	public Mono<Response<CumplimientoCita>> getCumpliminetoCita(@Valid @PathVariable String idUsuario){
		return operacionesService.getCumpliminetoCita(idUsuario);
	}	
}
