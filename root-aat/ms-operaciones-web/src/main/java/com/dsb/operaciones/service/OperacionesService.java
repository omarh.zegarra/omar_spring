package com.dsb.operaciones.service;

import java.util.List;

import javax.validation.Valid;

import com.dsb.core.common.http.Response;
import com.dsb.core.domain.cliente.CumplimientoCita;
import com.dsb.core.domain.cliente.Excedente;
import com.dsb.core.domain.cliente.OperacionesWrapper;

import reactor.core.publisher.Mono;

public interface OperacionesService {

	Mono<Response<OperacionesWrapper>> getIngresosSalidas(@Valid String idUsuario);

	Mono<Response<List<Excedente>>> getExcedente(@Valid String idUsuario);

	Mono<Response<OperacionesWrapper>> getEntregaRecojo(@Valid String idUsuario);

	Mono<Response<CumplimientoCita>> getCumpliminetoCita(@Valid String idUsuario);
	
}
