package com.dsb.security.aspect;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import com.dsb.core.common.base.BaseAspect;

@Aspect
@Component
public class TokenAspect extends BaseAspect{
	private static final Logger logger = LogManager.getLogger(TokenAspect.class);
						
	public void verifyToken(){
		logger.info("init security aspect...");
		//this.verifyToken();
	}
}
