package com.dsb.security.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dsb.core.common.http.Response;
import com.dsb.core.domain.seguridad.Usuario;
import com.dsb.security.service.UsuarioService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/user")
public class UsuarioController {
	@Autowired
	UsuarioService usuarioService;

	@GetMapping(value = "/")
	protected String test(){
		return "security-billsale is running...";
	}
	
	@PostMapping(value = "/testPost",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE,headers="Accept=application/json")
	public Response<Usuario> testPost(@RequestBody Usuario usuario){
		return new Response<>();
	}
	
	@PostMapping(value = "/insert",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE,headers="Accept=application/json")
	protected Mono<Response<Usuario>> insert(@RequestBody Usuario usuario){
		return usuarioService.insert(usuario);
	}
	
	@PostMapping(value = "/update",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE,headers="Accept=application/json")
	protected Mono<Response<Usuario>> update(@RequestBody Usuario usuario) throws Exception{	
		return usuarioService.update(usuario);
	}
	
	@PostMapping(value = "/delete/{correo}",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE,headers="Accept=application/json")
	protected Mono<Response<Boolean>> delete(@Valid @PathVariable String correo){		
		return usuarioService.delete(correo);
	}
	
	@PostMapping(value = "/listx", consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	protected Flux<Response<List<Usuario>>> listx(){	
		return usuarioService.list();
	}
	
	@PostMapping(value = "/list", consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	protected Flux<Usuario> list(){	
		return usuarioService.getUsers();
	}
	
	@PostMapping(value = "/confirm/email_{accessToken}", consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Mono<Response<Void>> confirmEmail(@PathVariable String accessToken){	
		return usuarioService.confirmEmail(accessToken);
	}
	
	@PostMapping(value = "/change/password/{correo}/{clave}/{nuevaClave}",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE,headers="Accept=application/json")
	public Mono<Response<Void>> changePassword(@Valid @PathVariable String correo,@Valid @PathVariable String clave,@Valid @PathVariable String nuevaClave){
		return usuarioService.changePassword(correo,clave,nuevaClave);
	}
	
	@PostMapping(value = "/remember/password/{correo}",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE,headers="Accept=application/json")
	public Mono<Response<String>> rememberPassword(@Valid @PathVariable String correo){
		return usuarioService.rememberPassword(correo);
	}
	
	@PostMapping(value = "/find",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE,headers="Accept=application/json")
	protected Flux<Usuario> find(@RequestBody Usuario usuario){	
		return usuarioService.find(usuario);
	}
	
	@PostMapping(value = "/findbyemailandpass/{correo}/{clave}",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE,headers="Accept=application/json")
	public Usuario findByEmailAndPass(@Valid @PathVariable String correo,@Valid @PathVariable String clave){	
		return usuarioService.findByEmailAndPass(correo,clave);
	}
}
