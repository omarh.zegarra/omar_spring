package com.dsb.security.config;
/*package com.dsb.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.dsb.persistence.model.UsuarioModel;
import com.dsb.persistence.repository.UsuarioRepository;

@Component
@RepositoryEventHandler(UsuarioModel.class)
public class UserEventHandler {

  @Autowired 
  private BCryptPasswordEncoder passwordEncoder;

  @Autowired 
  private UsuarioRepository usuarioRepository;

  @HandleBeforeCreate     
  public void handleUsuarioModelCreate(UsuarioModel UsuarioModel) {
    UsuarioModel.setPassword(passwordEncoder.encode(UsuarioModel.getPassword()));
  }

  @HandleBeforeSave
  public void handleUsuarioModelUpdate(UsuarioModel UsuarioModel) {
    if (UsuarioModel.getPassword() == null || UsuarioModel.getPassword().equals("")) {
        //keeps the last password
        UsuarioModel storedUsuarioModel = UsuarioRepository.getOne(UsuarioModel.getId());
        UsuarioModel.setPassword(storedUsuarioModel.getPassword());
    }
    else {
        //password change request
        UsuarioModel.setPassword(passwordEncoder.encode(UsuarioModel.getPassword()));
    }
  }
}
*/