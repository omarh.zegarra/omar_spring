package com.dsb.security.service;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.dsb.core.common.base.BaseService;
import com.dsb.core.common.http.Response;
import com.dsb.core.domain.seguridad.Token;
import com.dsb.core.domain.seguridad.Usuario;
import com.dsb.core.util.Util;
import com.dsb.persistence.model.TokenModel;
import com.dsb.persistence.model.UsuarioModel;
import com.dsb.persistence.repository.UsuarioRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class UsuarioServiceImpl extends BaseService implements UsuarioService {
	private static final Logger logger = LogManager.getLogger(UsuarioServiceImpl.class);
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired(required=true)
	PasswordEncoder encoder;
	
	private UsuarioModel getUsuarioModel(Usuario usuario) {		
		if (StringUtils.isEmpty(usuario.getToken())) {
			throw new RuntimeException("Error al registrar usuario...no tiene asignado un token");
		}
		UsuarioModel user = Util.objectToObject(UsuarioModel.class, usuario);
		TokenModel token = Util.objectToObject(TokenModel.class, usuario.getToken());
		user.setToken(token);
		user.getToken().setUsuario(user);
		user.setEstado(true);
		return user;
	}
	
	@Override
	public Mono<Response<Usuario>> insert(Usuario usuario) {
		Response<Usuario> res = new Response<>();
		usuario.setClave(encoder.encode(usuario.getClave()));
		usuario.setCodigoRol("ROLE_USER");
		return Mono.just(usuarioRepository.save(getUsuarioModel(usuario)))
			.switchIfEmpty(Mono.error(new RuntimeException("Error al registrar usuario...")))
			.flatMap(x -> {
				//usuario.setId(x.getId());
				res.setObjetoRespuesta(usuario);   
				return Mono.just(res);
			})
			.onErrorResume(e -> {
				logger.info("error delete user"+e);
				throw new RuntimeException("error on insert user...");
			});		
	}
	
	@Override
	public Mono<Response<Boolean>> delete(String correo) {
		logger.info("init delete user...");
		Response<Boolean> res = new Response<>();
		//res.setPath(getPathApi());
		return Mono.just(usuarioRepository.changeUserStatusByEmail(correo, false))
			.switchIfEmpty(Mono.error(new RuntimeException("Error on delete user...")))
			.flatMap(x -> {
				res.setObjetoRespuesta(true);   
				return Mono.just(res);
			})
			.onErrorResume(e -> {
				logger.info("error on delete user"+e);
				throw new RuntimeException("erroron delete user...");
			});		
	}
	
	@Override
	public Mono<Response<Usuario>> update(Usuario usuario) throws Exception {
		Response<Usuario> response = new Response<>();
		UsuarioModel uModel = getModelByEmail(usuario);
		return Mono.just(usuarioRepository.save(uModel))
			.switchIfEmpty(Mono.error(new RuntimeException("user withouth asigned token...")))
			.flatMap(x -> {
				Token token=Util.objectToObject(Token.class, x.getToken());
				usuario.setToken(token);
				response.setObjetoRespuesta(usuario);
				return Mono.just(response);
			})
			.onErrorResume(e -> {
				logger.info("error in remember passwordr"+e);
				throw new RuntimeException("Error in change password...");
			});
	}

	private UsuarioModel getModelByEmail(Usuario usuario) throws Exception {
		UsuarioModel uModel = usuarioRepository.findUserByEmail(usuario.getCorreo());		
		if (StringUtils.isEmpty(uModel)) {
			throw new RuntimeException("usuario no encontrado...");
		}
		if (uModel.getEstado().equals(false)) {
			throw new RuntimeException("usuario no habilitado...");
		}
		UsuarioModel usuarioModel=Util.objectToObject(UsuarioModel.class, usuario);	
		usuarioModel.setToken(uModel.getToken());
		return usuarioModel;
	}
	/*
	private UsuarioModel getModelByEmailPassword(Usuario usuario) throws Exception {
		UsuarioModel uModel = usuarioRepository.findUserByEmailAndPassword(usuario.getCorreo(), usuario.getClave());		
		if (StringUtils.isEmpty(uModel)) {
			throw new RuntimeException("usuario no encontrado...");
		}
		if (uModel.getEstado().equals(false)) {
			throw new RuntimeException("usuario no habilitado...");
		}
		UsuarioModel usuarioModel=Util.objectToObject(UsuarioModel.class, usuario);	
		usuarioModel.setToken(uModel.getToken());
		return usuarioModel;
	}
	*/
	@Override
	public Flux<Response<List<Usuario>>> list() {
		logger.info("service list users...");
		Response<List<Usuario>> response = new Response<>(); 
		response.setObjetoRespuesta(Util.listObjectToListObject(Usuario.class, usuarioRepository.findAll()));		
		return Flux.just(response)
			.onErrorResume(e -> {
				logger.info("error getallusers"+e);
				throw new RuntimeException("error get all users...");
			});
	}

	@Override
	public Flux<Usuario> getUsers() {
	    return Flux.fromIterable(
	    	Util.listObjectToListObject(Usuario.class, usuarioRepository.findAll())
	    	.stream() 
	    	.filter(user -> user.getEstado())
            .collect(Collectors.toList())
	    );
	}
	
	@Override
	public Flux<Usuario> find(Usuario usuario) {
		return Flux.fromIterable(
		    Util.listObjectToListObject(Usuario.class,usuarioRepository.find(
		    	usuario.getNombres(),usuario.getCorreo(),usuario.getApellidoPaterno())
		    ).stream() 
		    .filter(user -> user.getEstado())
	        .collect(Collectors.toList())
		);
	}
	
	@Override
	public Mono<Response<Void>> confirmEmail(String accessToken) {
		Response<Void> response = new Response<>();	
		return Mono.just(usuarioRepository.findUserByAccessToken(accessToken))
			.switchIfEmpty(Mono.error(new RuntimeException("user withouth asigned token...")))
			.flatMap(x -> {
				usuarioRepository.changeUserStatusByEmail(x.getCorreo(),true);  
				return Mono.just(response);
			})
			.onErrorResume(e -> {
				logger.info("error in remember passwordr"+e);
				throw new RuntimeException("Error in change password...");
			});
	}
	
	@Override
	public Mono<Response<Void>> changePassword(String correo,String clave,String nuevaClave) {
		Response<Void> response = new Response<>();	
		return Mono.just(usuarioRepository.findUserByEmailAndPassword(correo,clave))
			.switchIfEmpty(Mono.error(new RuntimeException("Error in change password...")))
			.flatMap(x -> {
				x.setClave(nuevaClave);
				usuarioRepository.save(x);   
				return Mono.just(response);
			})
			.onErrorResume(e -> {
				logger.info("error in remember passwordr"+e);
				throw new RuntimeException("Error in change password...");
			});
	}
	
	@Override
	public Mono<Response<String>> rememberPassword(String correo) {
		Response<String> response = new Response<>();	
		return Mono.just(usuarioRepository.findUserByEmail(correo))
			.switchIfEmpty(Mono.error(new RuntimeException("Error in remember password...")))
			.flatMap(x -> {
				response.setObjetoRespuesta(Util.generateRandomSring());   
				return Mono.just(response);
			})
			.onErrorResume(e -> {
				logger.info("error in remember passwordr"+e);
				throw new RuntimeException("error get all users...");
			});
	}

	@Override
	public Usuario findByEmailAndPass(String correo, String clave) {
		Usuario user=Util.objectToObject(Usuario.class,usuarioRepository.findUserByEmailAndPasswordx(correo,encoder.encode(clave)));
		logger.info("init findByEmailAndPass..."+user);
		return user;
		
		/*
		return Mono.just(Util.objectToObject(Usuario.class,usuarioRepository.findUserByEmailAndPasswordx(
		    	correo,clave)
		    ))
			.switchIfEmpty(Mono.error(new RuntimeException("Error in remember password...")))
			.onErrorResume(e -> {
				logger.info("error in remember passwordr"+e);
				throw new RuntimeException("error get all users...");
			});
		*/
	}		
}
