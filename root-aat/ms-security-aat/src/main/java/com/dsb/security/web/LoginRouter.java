package com.dsb.security.web;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;
import static org.springframework.web.reactive.function.server.RequestPredicates.contentType;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.reactive.config.EnableWebFlux;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

@Configuration
@EnableWebFlux
@CrossOrigin
//@Service
public class LoginRouter{
	private static final String LOGIN_URI = "/usuario/test/login";

	@Bean
	public RouterFunction<ServerResponse> routerFunction(LoginHandler handler) {
		return RouterFunctions
			.route(POST(LOGIN_URI)
			.and(accept(APPLICATION_JSON).and(contentType(APPLICATION_JSON))),handler::login);
				//.andRoute(GET(IMAGES_URI).and(accept(MediaType.APPLICATION_JSON)), handler::getUploadedImages)
				//.andRoute(GET(IMAGES_UPLOAD_STATUS_URI).and(accept(MediaType.APPLICATION_JSON)),handler::getUploadedStatus);
	}
}
