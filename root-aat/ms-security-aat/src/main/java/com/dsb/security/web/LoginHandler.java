package com.dsb.security.web;

import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import reactor.core.publisher.Mono;

public interface LoginHandler {
	Mono<ServerResponse> login(ServerRequest serverRequest);
}
