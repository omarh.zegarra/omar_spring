package com.dsb.security.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dsb.core.common.http.Response;
import com.dsb.core.domain.seguridad.Usuario;
import com.dsb.security.service.TokenService;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/token")
public class TokenController {
	@Autowired
	TokenService tokenService;
	
	@PostMapping(value = "/save/newtoken",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE,headers="Accept=application/json")
	protected Response<Usuario> getTokenByUser(@RequestBody Usuario usuario){
		return tokenService.saveAccessToken(usuario);
	}
	
	@PostMapping(value = "/verifytoken/{bearerToken}",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE,headers="Accept=application/json")
	protected Mono<Boolean> verifyToken(@PathVariable String bearerToken){
		return tokenService.verifyToken(bearerToken);
	}
}
