package com.dsb.security.service;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dsb.core.common.http.Response;
import com.dsb.core.domain.seguridad.Rol;
import com.dsb.core.util.Util;
import com.dsb.persistence.repository.RolRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class RolServiceImpl implements RolService{
	private static final Logger logger = LogManager.getLogger(RolServiceImpl.class);
	
	@Autowired
	RolRepository rolRepository;
	
	@Override
	public Flux<Response<List<Rol>>> getAllRol() {
		Response<List<Rol>> response = new Response<>(); 
		response.setObjetoRespuesta(Util.listObjectToListObject(Rol.class, rolRepository.findAll()));		
		return Flux.just(response)
			.onErrorResume(e -> {
				logger.info("error getallusers"+e);
				throw new RuntimeException("error get all users...");
			});
	}

	@Override
	public Mono<Response<Rol>> getRolById(Long id) {
		Response<Rol> response = new Response<>(); 
		response.setObjetoRespuesta(Util.objectToObject(Rol.class, rolRepository.findById(id)));		
		return Mono.just(response)
			.onErrorResume(e -> {
				logger.info("error getallusers"+e);
				throw new RuntimeException("error get all users...");
			});
	}
}
