package com.dsb.security.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.dsb.persistence.model.UsuarioLoginModel;
import com.dsb.persistence.repository.UsuarioLoginRepository;

import reactor.core.publisher.Mono;

@Component
public class LoginHandlerImpl implements LoginHandler{

	@Autowired
	UsuarioLoginRepository usuarioLoginRepository;
	
	@Override
	public Mono<ServerResponse> login(ServerRequest request) {
		Mono<UsuarioLoginModel> usuario = request.bodyToMono(UsuarioLoginModel.class);
		
        return Mono.just(usuarioLoginRepository.save(usuario.block()))
        	.flatMap(user -> ServerResponse.ok()
        	//.contentType(APPLICATION_JSON)
        	.body(BodyInserters.fromObject(user)))
        	.switchIfEmpty(ServerResponse.notFound().build());
	}	
}
