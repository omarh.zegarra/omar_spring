package com.dsb.security.service;

import java.util.List;

import org.springframework.scheduling.annotation.Async;

import com.dsb.core.common.http.Response;
import com.dsb.core.domain.seguridad.Usuario;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Async("databasePoolExecutor")
public interface UsuarioService {
	Mono<Response<Usuario>> insert(Usuario usuario);
	Mono<Response<Boolean>> delete(String correo);
	Mono<Response<Void>> confirmEmail(String accessToken);
	Mono<Response<Void>> changePassword(String correo, String clave, String nuevaClave);
	Mono<Response<String>> rememberPassword(String correo);
	Flux<Response<List<Usuario>>> list();
	Mono<Response<Usuario>> update(Usuario usuario) throws Exception;
	Flux<Usuario> getUsers();
	Flux<Usuario> find(Usuario usuario);	
	Usuario findByEmailAndPass(String correo, String clave);
}
