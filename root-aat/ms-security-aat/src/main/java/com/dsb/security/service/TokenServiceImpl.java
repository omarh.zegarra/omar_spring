package com.dsb.security.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.dsb.core.common.http.Response;
import com.dsb.core.domain.seguridad.Token;
import com.dsb.core.domain.seguridad.Usuario;
import com.dsb.core.util.Enumeradores;
import com.dsb.core.util.Util;
import com.dsb.persistence.model.TokenModel;
import com.dsb.persistence.model.UsuarioModel;
import com.dsb.persistence.repository.TokenRepository;
import com.dsb.persistence.repository.UsuarioRepository;

import reactor.core.publisher.Mono;

@Service
public class TokenServiceImpl implements TokenService{
	private static final Logger logger = LogManager.getLogger(TokenServiceImpl.class);
	
	@Autowired
	TokenRepository tokenRepository;
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Override
	public Response<Usuario> saveAccessToken(Usuario usuario) {
		Response<Usuario> res = new Response<>();
		Usuario usu=new Usuario();
		Token token=new Token();
		try {
			UsuarioModel usuarioModel =usuarioRepository.findUserByEmail(usuario.getCorreo());
			if(StringUtils.isEmpty(usuarioModel)) {
				throw new RuntimeException("usuario sin token asignado...");
			}
			token=usuario.getToken();
			usu=Util.objectToObject(Usuario.class, usuarioModel);
			usu.setToken(token);
			
			TokenModel tModelAux=Util.objectToObject(TokenModel.class, usu.getToken());
			tModelAux.setId(usuarioModel.getToken().getId());
			UsuarioModel uModelAux=Util.objectToObject(UsuarioModel.class, usuario);
			uModelAux.setId(usuarioModel.getId());			
			tModelAux.setUsuario(uModelAux);
			TokenModel tokenModel = tokenRepository.save(tModelAux);
			
			if(StringUtils.isEmpty(tokenModel)) {
				throw new RuntimeException("error in save new access token...");
			}
			
			res.setObjetoRespuesta(usu);
			res.setStatusText(Enumeradores.StatusText.OK.name());
		} catch (Exception e) {
			res.setStatusText(Enumeradores.StatusText.ERROR.name());
			res.setMessage(e.getMessage());
			logger.info("error al actualizar refreshtoken..", e);
		}
		return res;
	}

	@Override
	public Mono<Boolean> verifyToken(String bearerToken) {
		return Mono.<Boolean> defer(() -> {
		    try { 
		    	Boolean bol = tokenRepository.verifyToken(bearerToken)==null ? Boolean.FALSE : Boolean.TRUE;
		    	return Mono.just(bol);		    	
		    }catch(Exception e) { 
		    	return Mono.just(false);
		    }
		}).doOnError(ex -> logger.error("error access token", ex));
	}
}
