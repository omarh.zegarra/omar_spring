package com.dsb.security.service;

import java.util.List;

import com.dsb.core.common.http.Response;
import com.dsb.core.domain.seguridad.Rol;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface RolService {
	Flux<Response<List<Rol>>> getAllRol();
	Mono<Response<Rol>> getRolById(Long id);
}
