package com.dsb.security.service;

import com.dsb.core.common.http.Response;
import com.dsb.core.domain.seguridad.Usuario;

import reactor.core.publisher.Mono;

public interface TokenService {
	Mono<Boolean> verifyToken(String bearerToken);
	Response<Usuario> saveAccessToken(Usuario usuario);
}
