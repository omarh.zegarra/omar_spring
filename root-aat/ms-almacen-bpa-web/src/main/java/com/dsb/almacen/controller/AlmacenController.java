package com.dsb.almacen.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dsb.almacen.service.AlmacenService;
import com.dsb.core.common.http.Response;
import com.dsb.core.common.wrapper.BPAwrapper;
import com.dsb.core.domain.Requerimiento;
import com.dsb.core.domain.RequerimientoBPA;
import com.dsb.persistence.model.FiltroRequerimiento;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/web/almacen")
public class AlmacenController {
	@Autowired
	AlmacenService almacenService;
	
	@GetMapping(value = "/")
	public String ping(){
		return "/web/almacen controller is running..";
	}
	
	@GetMapping(value = "/obtener/ingresos/{idCliente}")
	public Flux<Response<List<RequerimientoBPA>>> obtenerIngresos(@Valid @PathVariable Long idCliente){
		return almacenService.obtenerDatosBPA(idCliente,"ING");
	}
	
	@GetMapping(value = "/obtener/salidas/{idCliente}")
	public Flux<Response<List<RequerimientoBPA>>> obtenerSalidas(@Valid @PathVariable Long idCliente){
		return almacenService.obtenerDatosBPA(idCliente,"SAL");
	}
	
	@GetMapping(value = "/obtener/transferencias/{idCliente}")
	public Flux<Response<List<RequerimientoBPA>>> obtenerTransferencias(@Valid @PathVariable Long idCliente){
		return almacenService.obtenerDatosBPA(idCliente,"TRA");
	}
	
	@PostMapping(value = "/crear/requerimiento/",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Mono<Response<Boolean>> guardarRequerimiento(@RequestBody Requerimiento requerimiento){
		return almacenService.crearRequerimiento(requerimiento);
	}
	
	@PostMapping(value = "/obtener/requerimiento/{idRequerimiento}",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Mono<Response<Requerimiento>> obtenerRequerimientoXid(@Valid @PathVariable Long idRequerimiento){
		return almacenService.obtenerRequerimientoXid(idRequerimiento);
	}
	
	@PostMapping(value = "/obtener/requerimientos/{idCliente}",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Flux<Response<BPAwrapper>> obtenerRequerimientos(@Valid @PathVariable Long idCliente) throws Exception{
		return almacenService.obtenerRequerimientos(idCliente);
	}
	
	@PostMapping(value = "/fecha/corte",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Mono<Response<Boolean>> validaFechaCorteRequerimiento() throws Exception{
		return almacenService.validacionFechaCorte();
	}
	
	@PostMapping(value = "/crear/requerimiento/masivo",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Mono<Response<Boolean>> crearRequerimientosMasivo(@RequestBody List<Requerimiento> requerimientos){
		return almacenService.crearRequerimientosMasivo(requerimientos);
	}

	@PostMapping(value = "/obtener/bpa/{idCliente}",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Mono<Response<BPAwrapper>> obtenerWrapperBPA(@Valid @PathVariable Long idCliente) throws Exception{
		return almacenService.obtenerWrapperBPA(idCliente);
	}
	
	@PostMapping(value="/filtrar/requerimientos",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Flux<Response<BPAwrapper>> filtrar(@RequestBody FiltroRequerimiento t) throws Exception{
		return almacenService.filtrarRequerimientos(t);
	}
	
}
