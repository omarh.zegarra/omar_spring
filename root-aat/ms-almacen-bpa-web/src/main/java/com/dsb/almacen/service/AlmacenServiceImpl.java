package com.dsb.almacen.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dsb.core.common.base.BaseService;
import com.dsb.core.common.http.Response;
import com.dsb.core.common.wrapper.BPAwrapper;
import com.dsb.core.domain.ClienteFinal;
import com.dsb.core.domain.ProductoXrequerimiento;
import com.dsb.core.domain.Requerimiento;
import com.dsb.core.domain.RequerimientoBPA;
import com.dsb.core.domain.Rotulo;
import com.dsb.core.domain.producto.Producto;
import com.dsb.core.util.Constantes;
import com.dsb.core.util.Util;
import com.dsb.persistence.model.ClienteFinalModel;
import com.dsb.persistence.model.ClienteModel;
import com.dsb.persistence.model.FiltroRequerimiento;
import com.dsb.persistence.model.requerimiento.ProductoXrequerimientoBPAmodel;
import com.dsb.persistence.model.requerimiento.RequerimientoBPAmodel;
import com.dsb.persistence.model.requerimiento.RequerimientoMasterBPAmodel;
import com.dsb.persistence.repository.ClienteRepository;
import com.dsb.persistence.repository.ProductoRepository;
import com.dsb.persistence.repository.RequerimientoBPArepository;
import com.dsb.persistence.repository.RequerimientoMasterBPArepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class AlmacenServiceImpl extends BaseService implements AlmacenService {
	private static final Logger logger = LogManager.getLogger(AlmacenServiceImpl.class);

	@Autowired
	RequerimientoMasterBPArepository requerimientoMasterBPArepository;

	@Autowired
	RequerimientoBPArepository requerimientoBPArepository;

	@Autowired
	ClienteRepository clienteRepository;

	@Autowired
	ProductoRepository productoRepository;
	

	EntityManager em;

	@Override
	public Flux<Response<List<RequerimientoBPA>>> obtenerDatosBPA(@Valid Long idCliente, String tipoEvento) {
		Response<List<RequerimientoBPA>> response = new Response<>();
		List<RequerimientoBPA> listaBPA = new ArrayList<>();

		List<RequerimientoBPAmodel> listReqBPA = requerimientoBPArepository
				.obtenerRequerimientosBPAxTipoEventoYcliente(idCliente, tipoEvento);

		listReqBPA.forEach(x -> {
			Integer totalUnidades = x.getRequerimiento().getTotalUnidades() != null
					? x.getRequerimiento().getTotalUnidades()
					: 0;
			Double volumen = x.getRequerimiento().getVolumen() != null ? x.getRequerimiento().getVolumen() : 0.0;
			String fechaRequerimiento = x.getRequerimiento().getFechaRequerimiento();

			String codigoRequerimiento=x.getRequerimiento().getCodigo();
			
			x.setRequerimiento(null);
			if (x.getDetallesRequerimientos() != null) {
				x.getDetallesRequerimientos().clear();
			}
			RequerimientoBPA req = new RequerimientoBPA();
			req = Util.objectToObject(RequerimientoBPA.class, x);
			req.setTotalUnidades(totalUnidades);
			req.setVolumen(volumen);
			req.setFechaHoraIngreso(fechaRequerimiento);
			req.setCodigoRequerimiento(codigoRequerimiento);
			req.setOrdenServicio("OS00125");
			req.setStatus("CUARENTENA");
			listaBPA.add(req);
		});
		response.setObjetoRespuesta(listaBPA);
		return Flux.just(response);
	}

	@Override
	public Mono<Response<Boolean>> crearRequerimiento(@Valid Requerimiento requerimiento) {
		logger.info("init create requerimientos...");
		Response<Boolean> res = new Response<>();
		return Mono.<Response<Boolean>>defer(() -> transactionTemplate.execute(status -> {
			RequerimientoMasterBPAmodel reqMasterModel = obtenerRequerimientoMasterBPAmodel(requerimiento).block();
			return Mono.just(requerimientoMasterBPArepository.save(reqMasterModel))
					.switchIfEmpty(Mono.error(new RuntimeException("error on save requerimiento..."))).flatMap(x -> {
						res.setObjetoRespuesta(true);
						return Mono.just(res);
					}).onErrorResume(e -> {
						logger.info(e);
						throw new RuntimeException("error on resume save requerimiento...");
					});
		})).subscribeOn(jdbcScheduler);
	}

	private Mono<RequerimientoMasterBPAmodel> obtenerRequerimientoMasterBPAmodel(Requerimiento requerimiento) {
		Integer idEstado = Constantes.idEstadoRequerimiento.get(requerimiento.getAccion());
		requerimiento.setIdEstado(idEstado);
		String estado = Constantes.estadoRequerimiento.get(requerimiento.getAccion());
		requerimiento.setEstado(estado);

		Integer idActividadLogistica = Constantes.idActividadLogistica.get(requerimiento.getActividadLogistica());
		requerimiento.setIdActividadLogistica(idActividadLogistica);
		String actividadLogistica = Constantes.actividadLogistica.get(requerimiento.getActividadLogistica());
		requerimiento.setActividadLogistica(actividadLogistica);

		requerimiento.setCodigo(obtenerCodigoTabla("REQ"));
		RequerimientoMasterBPAmodel reqMasterModel = Util.objectToObject(RequerimientoMasterBPAmodel.class,
				requerimiento);

		RequerimientoBPAmodel reqBPAmodel = reqMasterModel.getRequerimientoBPA();
		reqBPAmodel.getDetallesRequerimientos().clear();

		Set<ProductoXrequerimientoBPAmodel> setDetalleReqBPAmodel = new HashSet<>(0);
		for (ProductoXrequerimiento req : requerimiento.getRequerimientoBPA().getDetallesRequerimientos()) {
			ProductoXrequerimientoBPAmodel det = Util.objectToObject(ProductoXrequerimientoBPAmodel.class, req);
			det.setRequerimientoBPA(reqBPAmodel);
			setDetalleReqBPAmodel.add(det);
		}

		reqBPAmodel.setDetallesRequerimientos(setDetalleReqBPAmodel);
		reqMasterModel.setRequerimientoBPA(reqBPAmodel);
		reqMasterModel.getRequerimientoBPA().setRequerimiento(reqMasterModel);

		return Mono.just(reqMasterModel).onErrorResume(e -> {
			logger.info(e);
			throw new RuntimeException("error on get master bpa model...");
		});
	}

	@Override
	public Mono<Response<Requerimiento>> obtenerRequerimientoXid(Long idRequerimiento) {
		logger.info("init obtener requerimiento...");
		Response<Requerimiento> response = new Response<>();
		return Mono.just(requerimientoMasterBPArepository.findById(idRequerimiento)).switchIfEmpty(Mono.empty())
				.flatMap(x -> {
					if (!x.isPresent())
						return Mono.just(response);
					RequerimientoMasterBPAmodel reqModel = x.get();

					if (reqModel.getRequerimientoBPA() != null) {
						reqModel.getRequerimientoBPA().setRequerimiento(null);
						reqModel.getRequerimientoBPA().getDetallesRequerimientos()
								.forEach(y -> y.setRequerimientoBPA(null));
					}

					Requerimiento req = Util.objectToObject(Requerimiento.class, reqModel);
					response.setObjetoRespuesta(req);
					return Mono.just(response);
				}).onErrorResume(e -> {
					logger.info(e);
					throw new RuntimeException();
				});
	}

	@Override
	public Mono<Response<BPAwrapper>> obtenerWrapperBPA(@Valid Long idCliente) {
		logger.info("init obtener requerimientos...");
		Response<BPAwrapper> response = new Response<>();
		BPAwrapper wrapper = new BPAwrapper();
		return Mono.just(requerimientoMasterBPArepository.obtenerRequerimientoXidCliente(idCliente)).flatMap(y -> {
			y.forEach(x -> x.setRequerimientoBPA(null));
			wrapper.setRequerimientos(Util.listObjectToListObject(Requerimiento.class, y));

			Optional<ClienteModel> optClienteModel = clienteRepository.findById(idCliente);
			if (optClienteModel.isPresent()) {
				List<ClienteFinal> clientesFinales = new ArrayList<>();
				ClienteModel clienteModel = optClienteModel.get();
				Set<ClienteFinalModel> clientesFinalesModel = clienteModel.getClientesFinales();

				clientesFinalesModel.forEach(x -> {
					x.setCliente(null);
					ClienteFinal clienteFinal = Util.objectToObject(ClienteFinal.class, x);
					clientesFinales.add(clienteFinal);
				});
				wrapper.setClientesFinales(clientesFinales);

				List<Rotulo> rotulos = new ArrayList<>();
				clienteModel.getRotulos().forEach(x -> {
					x.setCliente(null);
					Rotulo rotulo = Util.objectToObject(Rotulo.class, x);
					rotulos.add(rotulo);
				});
				wrapper.setRotulos(rotulos);
			}

			List<Producto> productos = new ArrayList<>();
			productoRepository.obtenerProductosXidCliente(idCliente.intValue()).forEach(x -> {
				x.setProductoDatoBPA(null);
				x.setProductoDatoLogistico(null);
				Producto producto = Util.objectToObject(Producto.class, x);
				productos.add(producto);
			});
			wrapper.setProductos(productos);
			wrapper.setParametros(obtenerParametros("BPA").block());
			response.setObjetoRespuesta(wrapper);
			return Mono.just(response).onErrorResume(e -> {
				logger.info(e);
				throw new RuntimeException();
			});
		}).onErrorResume(e -> {
			logger.info(e);
			throw new RuntimeException();
		});
	}

	@Override
	public Flux<Response<BPAwrapper>> obtenerRequerimientos(@Valid Long idCliente) throws Exception {
		logger.info("init obtener requerimientos...");
		Response<BPAwrapper> response = new Response<>();

		BPAwrapper wrapper = new BPAwrapper();
		List<RequerimientoMasterBPAmodel> lista = requerimientoMasterBPArepository
				.obtenerRequerimientoXidCliente(idCliente);
		lista.forEach(x -> x.setRequerimientoBPA(null));

		wrapper.setParametros(obtenerParametros("BPA").block());
		wrapper.setRequerimientos(Util.listObjectToListObject(Requerimiento.class, lista));
		response.setObjetoRespuesta(wrapper);

		return Flux.just(response).onErrorResume(e -> {
			logger.info(e);
			throw new RuntimeException();
		});
	}

	@Override
	public Flux<Response<BPAwrapper>> filtrarRequerimientos(FiltroRequerimiento t) throws Exception {
		logger.info("init obtener requerimientos...");

		Response<BPAwrapper> response = new Response<>();
		BPAwrapper wrapper = new BPAwrapper();
		List<RequerimientoMasterBPAmodel> lista = requerimientoMasterBPArepository.filtroRequerimiento(t.getIdCliente(),
				t.getActividadLogistica());
		lista.forEach(x -> x.setRequerimientoBPA(null));
		wrapper.setRequerimientos(Util.listObjectToListObject(Requerimiento.class, lista));
		response.setObjetoRespuesta(wrapper);

		return Flux.just(response).onErrorResume(e -> {
			logger.info(e);
			throw new RuntimeException();
		});
	}

	@Override
	public Mono<Response<Boolean>> validacionFechaCorte() throws Exception {
		Response<Boolean> response = new Response<>();
		return Mono.just(validaFechaCorteRequerimiento())
				.switchIfEmpty(Mono.error(new RuntimeException("error on save requerimiento..."))).flatMap(x -> {
					response.setObjetoRespuesta(x);
					return Mono.just(response);
				}).onErrorResume(e -> {
					logger.info(e);
					throw new RuntimeException("error on save requerimiento...");
				});
	}

	@Override
	public Mono<Response<Boolean>> crearRequerimientosMasivo(List<Requerimiento> requerimientos) {
		logger.info("init create req masivo...");
		Response<Boolean> res = new Response<>();
		return Mono.<Response<Boolean>>defer(() -> transactionTemplate.execute(status -> {
			requerimientos.forEach(x -> {
				RequerimientoMasterBPAmodel reqMasterModel = obtenerRequerimientoMasterBPAmodel(x).block();
				Mono.just(requerimientoMasterBPArepository.save(reqMasterModel)).onErrorResume(e -> {
					logger.info(e);
					throw new RuntimeException();
				});
			});
			res.setObjetoRespuesta(true);
			return Mono.just(res);
		})).subscribeOn(jdbcScheduler);
	}

}
