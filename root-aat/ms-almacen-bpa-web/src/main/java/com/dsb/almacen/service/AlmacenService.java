package com.dsb.almacen.service;

import java.util.List;

import javax.validation.Valid;

import com.dsb.core.common.http.Response;
import com.dsb.core.common.wrapper.BPAwrapper;
import com.dsb.core.domain.Requerimiento;
import com.dsb.core.domain.RequerimientoBPA;
import com.dsb.persistence.model.FiltroRequerimiento;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface AlmacenService {

	Flux<Response<List<RequerimientoBPA>>> obtenerDatosBPA(@Valid Long idCliente,String opcion);

	Mono<Response<Boolean>> crearRequerimiento(@Valid Requerimiento requerimiento);

	Mono<Response<Requerimiento>> obtenerRequerimientoXid(Long idRequerimiento);

	Flux<Response<BPAwrapper>> obtenerRequerimientos(@Valid Long idcliente) throws Exception;

	Mono<Response<Boolean>> validacionFechaCorte() throws Exception;

	Mono<Response<Boolean>> crearRequerimientosMasivo(List<Requerimiento> requerimientos);

	Mono<Response<BPAwrapper>> obtenerWrapperBPA(@Valid Long idCliente) throws Exception;
	
	Flux<Response<BPAwrapper>> filtrarRequerimientos(FiltroRequerimiento t) throws Exception;


}
