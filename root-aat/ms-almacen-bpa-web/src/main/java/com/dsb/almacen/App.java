package com.dsb.almacen;

import com.dsb.core.common.base.BaseApp;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;

public class App extends BaseApp{
	private static final Logger log= LogManager.getLogger(App.class);
	
    public static void main( String[] args ){
    	SpringApplication.run(App.class, args);
    }
    
    @Override
	public void run(String... arg0) throws Exception {
        log.info("init almacen/bpa/web app...");
	}
}
