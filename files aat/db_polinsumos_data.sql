insert into rol(id_rol,descripcion,default_rol) values('ROLE_ADMIN','usuario admin',true);
insert into rol(id_rol,descripcion,default_rol) values('ROLE_USER','usuario user',false);

INSERT INTO public.usuario(
	id_rol,nombres,apellido_paterno,apellido_materno, correo, clave, estado, numero_celular, clave_celular)
VALUES (
	'ROLE_ADMIN','default user', 'default user', 'default user', 
	'defaultemail@gmail.com','$2a$10$6lqWBGXaW6cgaoKB0DQkV.qd.nlJfGr8/6LIrqu8bjJO72LD5oEZy','true',null,null
);
--clave: $2a$10$6lqWBGXaW6cgaoKB0DQkV.qd.nlJfGr8/6LIrqu8bjJO72LD5oEZy ------> 123

