-- Create tables section -------------------------------------------------

-- Table usuario

CREATE TABLE "usuario"(
 "id_usuario" SERIAL,
 "id_rol" Character varying(10) NOT NULL,
 "nombres" Character varying(180) NOT NULL,
 "apellido_paterno" Character varying(180),
 "apellido_materno" Character varying(180),
 "correo" Character varying(180) NOT NULL,
 "clave" Character varying(180) NOT NULL,
 "estado" Boolean DEFAULT false NOT NULL,
 "numero_celular" Character varying(20),
 "clave_celular" Character varying(50)
);

-- Create indexes for table usuario
CREATE INDEX "index_rol" ON "usuario" ("id_rol")
;

-- Add keys for table usuario

ALTER TABLE "usuario" ADD CONSTRAINT "pk_usuario_id" PRIMARY KEY ("id_usuario")
;

ALTER TABLE "usuario" ADD CONSTRAINT "unique_correo" UNIQUE ("correo")
;

-- Table rol

CREATE TABLE "rol"(
 "id_rol" Character varying(10),
 "descripcion" Character varying(180),
 "default_rol" Boolean DEFAULT false NOT NULL
)
;

-- Add keys for table rol
ALTER TABLE "rol" ADD CONSTRAINT "pk_rol_id" PRIMARY KEY ("id_rol")
;
-- Create relationships section ------------------------------------------------- 

ALTER TABLE "usuario" ADD CONSTRAINT "fk_rol_nombre" FOREIGN KEY ("id_rol") REFERENCES "rol" ("id_rol") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Table usuario

CREATE TABLE "aplicacion"(
 "id_aplicacion" Character varying(10) NOT NULL,
 "descripcion" Character varying(180) NOT NULL,
 "estado" Boolean DEFAULT true NOT NULL
);

-- Add keys for table rol

ALTER TABLE "aplicacion" ADD CONSTRAINT "pk_aplicacion_id" PRIMARY KEY ("id_aplicacion");

/**********************************************************/
CREATE TABLE "token"(
 "id_token" SERIAL,
 "access_token" Character varying(180) NOT NULL,
 "token_type" Character varying(50) NOT NULL,
 "refresh_token" Character varying(180) NOT NULL,
 "expires_in" Integer NOT NULL,
 "scope" Character varying(100),
 "id_usuario" Integer NOT NULL 
);

-- Create indexes for table token
CREATE INDEX "index_id_usuario" ON "token" ("id_usuario");

ALTER TABLE "token" ADD CONSTRAINT "unique_usuario" UNIQUE ("id_usuario")
;

-- Add keys for table token

ALTER TABLE "token" ADD CONSTRAINT "pk_token_id" PRIMARY KEY ("id_token");

ALTER TABLE "token" ADD CONSTRAINT "fk_id_usuario" FOREIGN KEY ("id_usuario") REFERENCES "usuario"("id_usuario") ON DELETE NO ACTION ON UPDATE NO ACTION;
