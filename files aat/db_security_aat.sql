/*
Created: 23/12/2019
Modified: 24/12/2019
Model: security-aat
Database: PostgreSQL 9.4
*/


-- Create tables section -------------------------------------------------

-- Table usuario

CREATE TABLE "usuario"(
 "id_usuario" Serial NOT NULL,
 "id_perfil" Integer,
 "nombres" Character varying(180) NOT NULL,
 "apellido_paterno" Character varying(180),
 "apellido_materno" Character varying(180),
 "correo" Character varying(180) NOT NULL,
 "clave" Character varying(180) NOT NULL,
 "status" Boolean DEFAULT False NOT NULL
)
;

-- Create indexes for table usuario

CREATE INDEX "IX_Relationship2" ON "usuario" ("id_perfil")
;

-- Add keys for table usuario

ALTER TABLE "usuario" ADD CONSTRAINT "Key1" PRIMARY KEY ("id_usuario")
;

ALTER TABLE "usuario" ADD CONSTRAINT "correo" UNIQUE ("correo")
;

-- Table perfil

CREATE TABLE "perfil"(
 "id_perfil" Serial NOT NULL,
 "nombre" Character varying(180) NOT NULL,
 "descripcion" Character varying(180)
)
;

-- Add keys for table perfil

ALTER TABLE "perfil" ADD CONSTRAINT "Key2" PRIMARY KEY ("id_perfil")
;

-- Table rol

CREATE TABLE "rol"(
 "id_rol" Serial NOT NULL,
 "nombre" Character varying(180) NOT NULL,
 "descripcion" Character varying(180)
)
;

-- Add keys for table rol

ALTER TABLE "rol" ADD CONSTRAINT "Key3" PRIMARY KEY ("id_rol")
;

-- Table perfil_rol

CREATE TABLE "perfil_rol"(
 "id_perfil" Integer NOT NULL,
 "id_rol" Integer NOT NULL,
 "descripcion" Character varying(180)
)
;

-- Create indexes for table perfil_rol

CREATE INDEX "IX_Relationship4" ON "perfil_rol" ("id_perfil")
;

CREATE INDEX "IX_Relationship5" ON "perfil_rol" ("id_rol")
;

-- Add keys for table perfil_rol

ALTER TABLE "perfil_rol" ADD CONSTRAINT "Key4" PRIMARY KEY ("id_perfil","id_rol")
;

-- Table menu

CREATE TABLE "menu"(
 "id_menu" Serial NOT NULL,
 "nombre" Character varying(180) NOT NULL,
 "descripcion" Character varying(180)
)
;

-- Add keys for table menu

ALTER TABLE "menu" ADD CONSTRAINT "Key5" PRIMARY KEY ("id_menu")
;

-- Table item

CREATE TABLE "item"(
 "id_item" Serial NOT NULL,
 "id_menu" Integer,
 "nombre" Character varying(20) NOT NULL,
 "descripcion" Character varying(180)
)
;

-- Create indexes for table item

CREATE INDEX "IX_Relationship6" ON "item" ("id_menu")
;

-- Add keys for table item

ALTER TABLE "item" ADD CONSTRAINT "Key6" PRIMARY KEY ("id_item")
;

-- Table opciones

CREATE TABLE "opciones"(
 "id_opciones" Serial NOT NULL,
 "id_perfil" Integer,
 "id_rol" Integer,
 "id_menu" Integer,
 "id_item" Integer,
 "descripcion" Character varying(180)
)
;

-- Create indexes for table opciones

CREATE INDEX "IX_Relationship12" ON "opciones" ("id_menu")
;

CREATE INDEX "IX_Relationship13" ON "opciones" ("id_item")
;

CREATE INDEX "IX_Relationship18" ON "opciones" ("id_perfil","id_rol")
;

-- Add keys for table opciones

ALTER TABLE "opciones" ADD CONSTRAINT "Key7" PRIMARY KEY ("id_opciones")
;

-- Table token

CREATE TABLE "token"(
 "id_token" Serial NOT NULL,
 "id_usuario" Integer,
 "access_token" Character varying(180) NOT NULL,
 "token_type" Character varying(180) NOT NULL,
 "refresh_token" Character varying(180) NOT NULL,
 "expires_in" Integer NOT NULL,
 "scope" Character varying(180) NOT NULL
)
;

-- Create indexes for table token

CREATE INDEX "IX_Relationship14" ON "token" ("id_usuario")
;

-- Add keys for table token

ALTER TABLE "token" ADD CONSTRAINT "Key8" PRIMARY KEY ("id_token")
;

-- Create relationships section ------------------------------------------------- 

ALTER TABLE "usuario" ADD CONSTRAINT "Relationship2" FOREIGN KEY ("id_perfil") REFERENCES "perfil" ("id_perfil") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "perfil_rol" ADD CONSTRAINT "Relationship4" FOREIGN KEY ("id_perfil") REFERENCES "perfil" ("id_perfil") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "perfil_rol" ADD CONSTRAINT "Relationship5" FOREIGN KEY ("id_rol") REFERENCES "rol" ("id_rol") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "item" ADD CONSTRAINT "Relationship6" FOREIGN KEY ("id_menu") REFERENCES "menu" ("id_menu") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "opciones" ADD CONSTRAINT "Relationship12" FOREIGN KEY ("id_menu") REFERENCES "menu" ("id_menu") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "opciones" ADD CONSTRAINT "Relationship13" FOREIGN KEY ("id_item") REFERENCES "item" ("id_item") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "token" ADD CONSTRAINT "Relationship14" FOREIGN KEY ("id_usuario") REFERENCES "usuario" ("id_usuario") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "opciones" ADD CONSTRAINT "Relationship18" FOREIGN KEY ("id_perfil", "id_rol") REFERENCES "perfil_rol" ("id_perfil", "id_rol") ON DELETE NO ACTION ON UPDATE NO ACTION
;







####################
INSERT INTO public.perfil(id_perfil, nombre, descripcion) VALUES (1, 'ADMIN', 'PERFIL ADMIN');
INSERT INTO public.perfil(id_perfil, nombre, descripcion) VALUES (2, 'USER', 'PERFIL USER');

INSERT INTO public.rol(id_rol, nombre, descripcion) VALUES (1,'ROLE_ADMIN','ROLE ADMIN');
INSERT INTO public.rol(id_rol, nombre, descripcion) VALUES (2,'ROLE_USER','ROLE USER');

INSERT INTO public.perfil_rol(id_perfil, id_rol, descripcion) VALUES (1, 1, 'ADMIN ROLE_ADMIN');
INSERT INTO public.perfil_rol(id_perfil, id_rol, descripcion) VALUES (1, 2, 'ADMIN ROLE_USER');

INSERT INTO public.menu(id_menu, nombre, descripcion) VALUES (1, 'Operaciones', 'menú Operaciones');
INSERT INTO public.menu(id_menu, nombre, descripcion) VALUES (2, 'Almacen BPA', 'menú Almacen BPA');
INSERT INTO public.menu(id_menu, nombre, descripcion) VALUES (3, 'Laboratorio BPM', 'menú Laboratorio BPM');
INSERT INTO public.menu(id_menu, nombre, descripcion) VALUES (4, 'BPyD', 'menú BPyD');
INSERT INTO public.menu(id_menu, nombre, descripcion) VALUES (5, 'Inventarios', 'menú Inventarios');
INSERT INTO public.menu(id_menu, nombre, descripcion) VALUES (6, 'Facturación', 'menú Facturación');


INSERT INTO public.item(id_item,id_menu,nombre, descripcion) VALUES (1,1,'Logísticas BPA', 'BPA...');
INSERT INTO public.item(id_item,id_menu,nombre, descripcion) VALUES (2,1,'Reacondicionado BPM', 'BPA...');
INSERT INTO public.item(id_item,id_menu, nombre, descripcion) VALUES (3,1,'Status entreg. /recoj.', 'BPA...');
INSERT INTO public.item(id_item,id_menu, nombre, descripcion) VALUES (4,1,'Tus transportes!', 'BPA...');
INSERT INTO public.item(id_item,id_menu, nombre, descripcion) VALUES (5,2,'Requerimientos (RS)', 'BPA...');
INSERT INTO public.item(id_item,id_menu, nombre, descripcion) VALUES (6,2,'Ingresos', 'BPA...');
INSERT INTO public.item(id_item,id_menu, nombre, descripcion) VALUES (7,2,'Salidas', 'BPA...');
INSERT INTO public.item(id_item,id_menu, nombre, descripcion) VALUES (8,2,'Transferencias', 'BPA...');
INSERT INTO public.item(id_item,id_menu, nombre, descripcion) VALUES (9,2,'Actas de Recepción', 'BPA...');
INSERT INTO public.item(id_item,id_menu, nombre, descripcion) VALUES (10,3,'Requerimientos (RS)', 'BPA...');
INSERT INTO public.item(id_item,id_menu, nombre, descripcion) VALUES (11,3,'Catálogo de Fotopatrón', 'BPA...');
	
	
INSERT INTO public.opciones(
	id_opciones, id_perfil, id_rol, id_menu, id_item, descripcion) VALUES (1, 1, 1, 1, 1, 'xxxxxx');
INSERT INTO public.opciones(
	id_opciones, id_perfil, id_rol, id_menu, id_item, descripcion) VALUES (2, 1, 1, 1, 2, 'xxxxxx');
INSERT INTO public.opciones(
	id_opciones, id_perfil, id_rol, id_menu, id_item, descripcion) VALUES (3, 1, 1, 1, 3, 'xxxxxx');
INSERT INTO public.opciones(
	id_opciones, id_perfil, id_rol, id_menu, id_item, descripcion) VALUES (4, 1, 1, 1, 4, 'xxxxxx');
	
INSERT INTO public.opciones(
	id_opciones, id_perfil, id_rol, id_menu, id_item, descripcion) VALUES (5, 1, 1, 2, 5, 'xxxxxx');
INSERT INTO public.opciones(
	id_opciones, id_perfil, id_rol, id_menu, id_item, descripcion) VALUES (6, 1, 1, 2, 6, 'xxxxxx');
INSERT INTO public.opciones(
	id_opciones, id_perfil, id_rol, id_menu, id_item, descripcion) VALUES (7, 1, 1, 2, 7, 'xxxxxx');
INSERT INTO public.opciones(
	id_opciones, id_perfil, id_rol, id_menu, id_item, descripcion) VALUES (8, 1, 1, 2, 8, 'xxxxxx');
	

